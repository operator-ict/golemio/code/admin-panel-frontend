import axios, { AxiosInstance, AxiosResponse, AxiosRequestConfig } from 'axios';
import { getToken } from "@dataplatform/shared/common-token";

export class HttpService {

  protected service: AxiosInstance;

  constructor() {
    this.service = axios.create();
    this.service.interceptors.response.use(this.handleSuccess, this.handleError);
    this.service.interceptors.request.use((config: AxiosRequestConfig) => {
      config.headers = {
        ...config.headers,
        'x-access-token': getToken()
      }
      return config;
    });
  }

  public handle401(callback: () => void) {
    this.service.interceptors.response.use(response => {
      return response;
    }, error => {
      if (error && error.response && error.response.status === 401) {
        callback();
      }
      return Promise.reject(error);
    });
  }

  private handleSuccess(response: AxiosResponse<any>) {
    return response;
  }

  private handleError = (error) => {
    return Promise.reject(error)
  }

  public get<ServerType, ClientType>(url: string, mapFce?: (rawData: ServerType) => ClientType):
    Promise<ClientType> {
    return this.service.get(url)
      .then((response) => mapFce != null ? mapFce(response.data) : response.data)
      .catch((err) => { console.log(err); throw err; });
  }

  public getRaw<ServerType, ClientType>(url: string, mapFce?: (rawData: AxiosResponse<ServerType>) => ClientType):
    Promise<ClientType> {
    return this.service.get(url)
      .then((response) => mapFce != null ? mapFce(response) : response.data)
      .catch((err) => { console.log(err); throw err; });
  }

  public post<ServerType, ClientType>(url: string, payload: any, mapFce?: (rawData: ServerType) => ClientType):
    Promise<ClientType> {
    return this.service.request({
      method: 'POST',
      url: url,
      responseType: 'json',
      data: payload
    }).then((response) => mapFce != null ? mapFce(response.data) : response.data)
      .catch((err) => {
        console.log(err); throw err;
      });
  }

  public put<ServerType, ClientType>(url: string, payload: any, mapFce?: (rawData: ServerType) => ClientType):
    Promise<ClientType> {
    return this.service.request({
      method: 'PUT',
      url: url,
      responseType: 'json',
      data: payload
    }).then((response) => mapFce != null ? mapFce(response.data) : response.data)
      .catch((err) => { console.log(err); throw err; });
  }

  public patch<ServerType, ClientType>(url: string, payload: any, mapFce?: (rawData: ServerType) => ClientType):
    Promise<ClientType> {
    return this.service.request({
      method: 'PATCH',
      url: url,
      responseType: 'json',
      data: payload
    }).then((response) => mapFce != null ? mapFce(response.data) : response.data)
      .catch((err) => { console.log(err); throw err; });
  }

  public delete<ServerType, ClientType>(url: string, mapFce?: (rawData: ServerType) => ClientType):
    Promise<ClientType> {
    return this.service.request({
      method: 'DELETE',
      url: url,
      responseType: 'json'
    }).then((response) => mapFce != null ? mapFce(response.data) : response.data)
      .catch((err) => { console.log(err); throw err; });
  }
}

export const httpService = new HttpService();