import { EnumItem } from './shared-ui-models';
import { EnhancedTableQuery } from './components';

export class CommonUiUtils {
    static getEnumName = <TKey>(items: EnumItem<TKey>[], value: TKey): string => {
        if (items) {
            const item = items.find(x => x.id === value);
            if (item) {
                return item.name;
            }
        }
        return '';
    }

    static getEnumNames = <TKey>(items: EnumItem<TKey>[], values: TKey[], delimiter: string = ', '): string => {
        if (items) {
            return items
                .filter(item => values.indexOf(item.id) > -1)
                .map(x => x.name)
                .join(delimiter);
        }
        return '';
    };

    static enhancedTableQueryToQueryString = <TData>(query: EnhancedTableQuery<TData>, mapFieldFn: (x: keyof TData) => string): string => {
        let qs = '';
        if (query.page != null && query.pageSize != null) {
            qs += !qs ? '?' : '&';
            qs += 'limit=' + query.pageSize;
            qs += '&page=' + (query.page + 1);
        }
        if (query.orderBy) {
            qs += !qs ? '?' : '&';
            qs += 'orderBy=' + mapFieldFn(query.orderBy);
            if (query.orderDirection) {
                qs += '&orderDir=' + query.orderDirection;
            }
        }
        if (query.search) {
            qs += !qs ? '?' : '&';
            qs += 'search=' + query.search;
        }

        return qs;
    }
}