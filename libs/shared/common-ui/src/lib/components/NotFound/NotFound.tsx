import React from 'react';

import './NotFound.scss';
import { Grid, Paper } from '@material-ui/core';

/* tslint:disable:no-empty-interface */
export interface NotFoundProps { }

export const NotFound = (props: NotFoundProps) => {
  return (
    <div>
      <Grid container justify="center" className="not-found-container">
        <Grid item>
          <Paper className="panel">
            <h1>Page not found.</h1>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};
