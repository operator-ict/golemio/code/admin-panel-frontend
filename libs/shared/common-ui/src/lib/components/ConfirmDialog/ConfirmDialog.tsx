import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import React from 'react';

export interface ConfirmDialogProps {
  open: boolean;
  title: string;
  message: string;
  handleCancel: () => void;
  handleOk: () => void;
}

export const ConfirmDialog = (props: ConfirmDialogProps) => {
  return (
    <Dialog
      disableBackdropClick
      disableEscapeKeyDown
      maxWidth="xs"
      aria-labelledby="confirmation-dialog-title"
      open={props.open}
    >
      <DialogTitle id="confirmation-dialog-title">{props.title}</DialogTitle>
      <DialogContent dividers>
        {props.message}
      </DialogContent>
      <DialogActions className="buttons">
        <Button onClick={props.handleCancel} color="primary">
          Cancel
        </Button>
        <Button onClick={props.handleOk} color="primary" type="submit">
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmDialog;
