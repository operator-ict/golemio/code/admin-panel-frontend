import React, { useState, useEffect } from 'react';
import { MenuItem, TextField, makeStyles, Theme, createStyles, Grid } from '@material-ui/core';
import useDebounce from '../../hooks/useDebounce';

export interface ComboBoxProps {
  id: string;
  value: ComboBoxOptionModel;
  label?: string;
  placeholder?: string;
  fetch: (search: string) => Promise<ComboBoxOptionModel[]>;
  onBlur?: React.EventHandler<any>;
  onChange?: (value: ComboBoxOptionModel) => void;
  error?: boolean;
  helperText?: string;
}

export interface ComboBoxOptionModel {
  value: number;
  label: string;
}

export const ComboBox = (props: ComboBoxProps) => {
  const useStyles = makeStyles((theme: Theme) => {
    const textItemHeight = '56px';
    return createStyles({
      root: {
      },
      autocompleteContainer: {
        position: 'relative',
      },
      autocompleteTextItem: {
        width: '100%',
        height: textItemHeight,
      },
      autocompletePopup: {
        width: '100%',
        background: 'white',
        top: textItemHeight,
        position: 'absolute',
        zIndex: 1,
        boxShadow: '0px 2px 6px rgba(0, 0, 0, 0.08)',
      },
    });
  });


  const classes = useStyles(props);

  const [searchValue, setSearchValue] = useState<string>('');
  const [isSearching, setIsSearching] = useState(false);
  const [selectedItem, setSelectedItem] = useState<ComboBoxOptionModel>(null);
  const [options, setOptions] = useState<ComboBoxOptionModel[]>([]);
  const debouncedSearchTerm = useDebounce(searchValue, 300);

  useEffect(() => {
    setSearchValue(props.value != null ? props.value.label : '');
    setSelectedItem(props.value);
  }, [props.value]);

  useEffect(
    () => {
      if (debouncedSearchTerm) {
        setIsSearching(true);
        props.fetch(debouncedSearchTerm).then(results => {
          setIsSearching(false);
          setOptions(results);
        });
      } else {
        if (props.onChange) {
          props.onChange(null);
        }
        setOptions([]);
      }
    },
    [debouncedSearchTerm]
  );

  const handleClickItem = (item: ComboBoxOptionModel) => {
    setSelectedItem(item);
    setSearchValue(item.label);
    if (props.onChange) {
      props.onChange(item);
    }
    setOptions([]);
  }

  return (
    <div className={classes.root}>
      <Grid container className={classes.autocompleteContainer}>
        <Grid item xs={12}>
          <TextField
            type="text"
            id={props.id}
            label={props.label}
            placeholder={props.placeholder}
            error={props.error}
            helperText={props.helperText}
            margin="normal"
            onChange={e => setSearchValue(e.target.value)}
            onBlur={props.onBlur}
            value={searchValue}
            required
            className={classes.autocompleteTextItem}
            autoComplete="off"
          />
        </Grid>
        <Grid item xs={12} className={classes.autocompletePopup}>
          {isSearching && <div>Searching ...</div>}
          {
            options.map(suggestion => {
              return <MenuItem
                onClick={event => { handleClickItem(suggestion) }}
                key={suggestion.value}
                selected={selectedItem != null && selectedItem.value === suggestion.value}
                component="div"
                style={{
                  fontWeight: selectedItem != null && selectedItem.value === suggestion.value ? 600 : 400,
                }}
              >
                {suggestion.label}
              </MenuItem>;
            })
          }
        </Grid>
      </Grid>
    </div>
  );
};

export default ComboBox;
