import React, { useEffect } from 'react';
import deburr from 'lodash/deburr';
import Autosuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import { MenuItem, TextField, Paper, makeStyles, Theme, createStyles } from '@material-ui/core';
import { BaseTextFieldProps } from '@material-ui/core/TextField';

export interface AutocompleteProps {
  source: string[];
  inputProps: BaseTextFieldProps;
  suggestionType: 'startsWith' | 'contains';

  value: string;
  setFieldValue: (field: string, value: any) => void;
  setFieldTouched: (field: string, isTouched?: boolean) => void;
}

export const Autocomplete = (props: AutocompleteProps) => {
  const [stateSuggestions, setSuggestions] = React.useState<string[]>([]);

  const handleSuggestionsFetchRequested = ({ value }: any) => {
    setSuggestions(getSuggestions(value));
  };

  const handleSuggestionsClearRequested = () => {
    setSuggestions([]);
  };

  const handleKeyDown = (event: KeyboardEvent) => {
    if (event.key === 'Enter') {
      event.preventDefault();
    }
  }

  const renderInputComponent = (inputProps: any) => {
    const { classes, inputRef = () => { }, ref, id, ...other } = inputProps;

    return (
      <TextField
        fullWidth
        InputProps={{
          inputRef: node => {
            ref(node);
            inputRef(node);
          },
          name: id,
          id: id,
          onKeyDown: handleKeyDown,
          classes: {
            input: classes.input,
          },
        }}
        {...other}
      />
    );
  }

  const renderSuggestion = (suggestion: string, { query, isHighlighted }: Autosuggest.RenderSuggestionParams, ) => {
    const matches = props.suggestionType === 'startsWith' ? match(suggestion, query) : matchContains(suggestion, query);
    const parts = parse(suggestion, matches);

    return (
      <MenuItem selected={isHighlighted} component="div">
        <div>
          {parts.map((part, index) => (
            <span key={part.text + '_' + index} style={{ fontWeight: part.highlight ? 600 : 400 }}>
              {part.text}
            </span>
          ))}
        </div>
      </MenuItem>
    );
  }

  const matchContains = (text: string, query: string) => {
    const specialCharsRegex = /[.*+?^${}()|[\]\\]/g;
    // const wordCharacterRegex = /[a-z0-9_]/i;
    const whitespacesRegex = /\s+/;

    function escapeRegexCharacters(str: any) {
      return str.replace(specialCharsRegex, '\\$&');
    }

    return (
      query
        .trim()
        .split(whitespacesRegex)
        .reduce((result: any, word: any) => {
          if (!word.length) return result;
          const wordLen = word.length;
          const regex = new RegExp(escapeRegexCharacters(word), 'i');
          const { index = -1 } = text.match(regex);
          if (index > -1) {
            result.push([index, index + wordLen]);

            // Replace what we just found with spaces so we don't find it again.
            text =
              text.slice(0, index) +
              new Array(wordLen + 1).join(' ') +
              text.slice(index + wordLen);
          }

          return result;
        }, [])
        .sort((match1: any, match2: any) => {
          return match1[0] - match2[0];
        })
    );
  };

  const getSuggestions = (value: string) => {
    const inputValue = deburr(value.trim()).toLowerCase();
    const inputLength = inputValue.length;
    let count = 0;

    return inputLength === 0
      ? props.source
      : props.source.filter(suggestion => {
        const keep = count < 5 && (props.suggestionType === 'startsWith' ?
          suggestion.slice(0, inputLength).toLowerCase() === inputValue :
          suggestion.indexOf(inputValue) > -1);

        if (keep) {
          count += 1;
        }

        return keep;
      });
  }

  const getSuggestionValue = (suggestion: string) => {
    return suggestion;
  }

  const handleChange = (event: React.ChangeEvent<{}>, { newValue }: Autosuggest.ChangeEvent) => {
    props.setFieldValue(props.inputProps.id, newValue);
  };

  const handleBlur = (event: React.FormEvent<any>, params?: Autosuggest.BlurEvent<string>) => {
    props.setFieldTouched(props.inputProps.id, true);
  };

  const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      root: {
        flexGrow: 1,
      },
      container: {
        position: 'relative',
      },
      suggestionsContainerOpen: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing(1),
        left: 0,
        right: 0,
      },
      suggestion: {
        display: 'block',
      },
      suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
      },
    }),
  );

  const classes = useStyles(props);

  return (
    <div className={classes.root}>
      <Autosuggest
        renderInputComponent={renderInputComponent}
        suggestions={stateSuggestions}
        onSuggestionsFetchRequested={handleSuggestionsFetchRequested}
        onSuggestionsClearRequested={handleSuggestionsClearRequested}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={renderSuggestion}
        shouldRenderSuggestions={() => { return true; }}
        inputProps={{
          classes,
          value: props.value,
          onChange: handleChange,
          onBlur: handleBlur,
          ...(props.inputProps as any),
        }}
        theme={{
          container: classes.container,
          suggestionsContainerOpen: classes.suggestionsContainerOpen,
          suggestionsList: classes.suggestionsList,
          suggestion: classes.suggestion,
        }}
        renderSuggestionsContainer={options => (
          <Paper {...options.containerProps} square>
            {options.children}
          </Paper>
        )}
      />
    </div>
  );
};

export default Autocomplete;
