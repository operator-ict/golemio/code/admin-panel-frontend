import React from 'react';

import { Drawer } from '@material-ui/core';
import './Aside.scss';

/* tslint:disable:no-empty-interface */
export interface AsideProps {
  children: JSX.Element[] | JSX.Element;
  visibility?: boolean;
  handleClose: () => void;
}

export const Aside = (props: AsideProps) => {

  return (
    <Drawer anchor="right" open={props.visibility} onClose={props.handleClose}>
      <div className="aside-container">
        {props.children}
      </div>
    </Drawer>
  );
};
