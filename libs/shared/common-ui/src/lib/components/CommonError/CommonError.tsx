import React from 'react';

import './CommonError.scss';

export interface CommonErrorProps {
  message: string;
  display?: string;
  subMessage?: string;
}

export const CommonError = (props: CommonErrorProps) => {
  return (
    <div className="common-error">
      <h1 style={{ color: 'red', display: props.display ? props.display : 'block' }}>{props.message}</h1>
      {props.subMessage && <h2 style={{ color: 'red', display: props.display ? props.display : 'block' }}>{props.subMessage}</h2>}
    </div>
  );
};
