import {
  IconButton,
  Toolbar,
  Tooltip,
  Typography,
  TextField,
  InputAdornment,
  Box
} from '@material-ui/core';
import {
  createStyles,
  lighten,
  makeStyles,
  Theme
} from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import clsx from 'clsx';
import React, { Fragment } from 'react';
import SearchIcon from '@material-ui/icons/Search';
import ClearIcon from '@material-ui/icons/Clear';
import { EnhancedTableActionDefinition } from './models';
import EnhancedTableActions from './EnhancedTableActions';

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
      minHeight: '36px',
      position: 'relative',
      bottom: '65px'
    },
    highlight:
      theme.palette.type === 'light'
        ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85)
        }
        : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark
        },
    spacer: {
      flex: '1 1 100%'
    },
    actions: {
      color: theme.palette.text.secondary,
      display: 'flex',
      flexFlow: 'row-reverse',
      width: '100%',
    },
    title: {
      flex: '0 0 auto'
    },
    searchField: {
      paddingLeft: theme.spacing(2)
    },
    searchIcon: {
      color: theme.palette.grey.A200
    }
  })
);

interface EnhancedTableToolbarProps<TData> {
  title?: string;
  numSelected: number;

  search: boolean;
  searchText?: string;
  searchFieldAlignment?: 'left' | 'right';
  onSearchChanged?: (value: string) => void;
  searchFieldStyle?: React.CSSProperties;

  actions: EnhancedTableActionDefinition<TData>[];
}

const EnhancedTableToolbar = <TData extends object>(
  props: EnhancedTableToolbarProps<TData>
) => {
  const classes = useToolbarStyles(props);
  const { numSelected } = props;

  const renderSearch = () => {
    if (props.search) {
      return (
        <TextField
          className={classes.searchField}
          value={props.searchText}
          onChange={event => props.onSearchChanged(event.target.value)}
          placeholder="Search..."
          InputProps={{
            className: classes.searchIcon,
            startAdornment: (
              <InputAdornment position="start">
                <Tooltip title="Search">
                  <SearchIcon className={classes.searchIcon} color="inherit" fontSize="small" />
                </Tooltip>
              </InputAdornment>
            ),
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  disabled={!props.searchText}
                  onClick={() => props.onSearchChanged('')}
                >
                  <ClearIcon color="inherit" fontSize="small" />
                </IconButton>
              </InputAdornment>
            ),
            style: props.searchFieldStyle
          }}
        />
      );
    } else {
      return null;
    }
  };

  const renderFreeActions = () => {
    return (
      <Fragment>
        <EnhancedTableActions actions={props.actions} size={'medium'} />
      </Fragment>
    );
  };

  return (
    <Toolbar
      className={clsx(classes.root, {
        [classes.highlight]: numSelected > 0
      })}
    >
      <div className={classes.title}>
        {numSelected > 0 ? (
          <Typography color="inherit" variant="subtitle1">
            {numSelected} selected
          </Typography>
        ) : (
            <Typography variant="h6" id="tableTitle">
              {props.title}
            </Typography>
          )}
      </div>
      <div className={classes.spacer} />
      <div className={classes.actions}>
        {numSelected > 0 ? (
          <Tooltip title="Delete">
            <IconButton aria-label="Delete">
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        ) : (
            <Fragment>
              <Box display="flex" alignItems="center" className="toolbar">
                {renderFreeActions()}
                {renderSearch()}
              </Box>
            </Fragment>
          )}
      </div>
    </Toolbar>
  );
};

export default EnhancedTableToolbar;
