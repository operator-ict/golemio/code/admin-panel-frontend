import { EnhancedTableActionDefinition } from './models';
import React from 'react';
import EnhancedTableAction from './EnhancedTableAction';

interface EnhancedTableActionsProps<TData> {
  actions: EnhancedTableActionDefinition<TData>[];
  data?: TData;
  size: 'small' | 'medium';
  inRow?: boolean;
}

export const EnhancedTableActions = <TData extends object>(
  props: EnhancedTableActionsProps<TData>
) => {
  if (props.actions) {
    return (
      <div style={{ display: 'flex' }}>
        {props.actions.map((action, index) => (
          <EnhancedTableAction
            action={action}
            key={index}
            data={props.data}
            size={props.size}
            inRow={props.inRow}
          />
        ))}
      </div>
    );
  }

  return null;
};

export default EnhancedTableActions;
