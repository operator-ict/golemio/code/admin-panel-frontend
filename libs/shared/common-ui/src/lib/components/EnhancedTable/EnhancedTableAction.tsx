import { EnhancedTableActionDefinition } from './models';
import React from 'react';
import { IconButton, Icon, Tooltip, Button } from '@material-ui/core';
import { Theme, makeStyles, createStyles } from '@material-ui/core/styles';

const useTableActionStyles = makeStyles((theme: Theme) => {
  return createStyles({
    icon: {
      background: theme.palette.grey.A100,
      marginLeft: '20px',
      marginRight: '20px'
    }
  });
});

interface EnhancedTableActionProps<TData> {
  action: EnhancedTableActionDefinition<TData>;
  data?: TData;
  size: 'small' | 'medium';
  inRow?: boolean;
}

export const EnhancedTableAction = <TData extends object>(
  props: EnhancedTableActionProps<TData>
) => {
  const classes = useTableActionStyles(props);
  let isHidden: boolean = false;
  if (typeof props.action.hidden === 'function') {
    isHidden = props.action.hidden(props.data)
  } else {
    isHidden = props.action.hidden;
  }
  if (isHidden) {
    return null;
  }

  let isDisabled: boolean = false;
  if (typeof props.action.disabled === 'function') {
    isDisabled = props.action.disabled(props.data)
  } else {
    isDisabled = props.action.disabled;
  }

  const handleOnClick = event => {
    if (props.action.onClick) {
      props.action.onClick(event, props.data);
      event.stopPropagation();
    }
  };

  const button = (
    <span>
      {props.action.icon != null ?
        <IconButton
          className={(props.inRow ? classes.icon : null) + ' ' + props.action.icon}
          size={props.size}
          color="inherit"
          disabled={isDisabled}
          onClick={event => handleOnClick(event)}
        >
          <Icon {...props.action.iconProps} fontSize="small">
            {props.action.icon}
          </Icon>
        </IconButton> :
        <Button
          className={props.inRow ? classes.icon : null}
          size={props.size}
          color="primary"
          variant="outlined"
          disabled={isDisabled}
          onClick={event => handleOnClick(event)}
        >
          {props.action.text}
        </Button>
      }
    </span >
  );

  if (props.action.tooltip) {
    return <Tooltip title={props.action.tooltip}>{button}</Tooltip>;
  } else {
    return button;
  }
};

export default EnhancedTableAction;
