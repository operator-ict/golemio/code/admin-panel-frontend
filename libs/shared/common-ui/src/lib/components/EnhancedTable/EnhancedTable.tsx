import {
  Checkbox,
  FormControlLabel,
  Paper,
  Switch,
  Table,
  TableBody,
  TableCell,
  TablePagination,
  TableRow,
  CircularProgress,
  Box
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import React, { useEffect } from 'react';
import Moment from 'react-moment';
import EnhancedTableHead from './EnhancedTableHead';
import EnhancedTableToolbar from './EnhancedTableToolbar';
import CheckIcon from '@material-ui/icons/Check';
import RemoveIcon from '@material-ui/icons/Remove';
import {
  EnhancedTableOrder,
  EnhancedTableQueryResult,
  EnhancedTableColumn,
  EnhancedTableQuery,
  EnhancedTableActionDefinition
} from './models';
import EnhancedTableActions from './EnhancedTableActions';
import { CommonError } from '../CommonError/CommonError';
import useDebounce from '../../hooks/useDebounce';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      marginTop: theme.spacing(3)
    },
    paper: {
      width: '100%',
      marginBottom: theme.spacing(2),
      position: 'relative'
    },
    table: {
      minWidth: '750px'
    },
    tr: {
      height: '62px'
    },
    th: {
      borderBottomColor: '#F3F3F3',
      color: theme.palette.text.secondary,
    },
    tableWrapper: {
      overflowX: 'auto'
    },
  })
);

export interface EnhancedTableProps<TData, TKey> {
  title?: string;

  loading: boolean;
  loadError: boolean;
  data: EnhancedTableQueryResult<TData>;

  columns: EnhancedTableColumn<TData>[];
  idAccessor: (row: TData) => TKey;
  dataStateChange: (query: EnhancedTableQuery<TData>) => void;

  selectable: boolean;

  sortable: boolean;
  order?: EnhancedTableOrder | '';
  orderBy?: keyof TData | '';

  pageable: boolean;
  page?: number;
  pageSize?: number;

  searchable: boolean;
  searchText?: string;

  actions: EnhancedTableActionDefinition<TData>[];

}

export const EnhancedTable = <TData extends object, TKey>(
  props: EnhancedTableProps<TData, TKey>
) => {
  // set defaults for columns
  const columns = props.columns
    ? props.columns.map(x => {
      return {
        ...x,
        sortable: x.sortable != null ? x.sortable : true
      };
    })
    : [];

  const classes = useStyles(props);
  const [selected, setSelected] = React.useState<TKey[]>([]);
  const [searchTerm, setSearchTerm] = React.useState<string>((props.searchable && props.searchText) || '');
  const debouncedSearchTerm = useDebounce(searchTerm, 300);

  const [query, setQuery] = React.useState<EnhancedTableQuery<TData>>({
    filters: [],
    page: props.pageable ? props.page : null,
    pageSize: props.pageable ? props.pageSize : null,
    search: (props.searchable && props.searchText) || '',
    orderBy: props.sortable ? props.orderBy : null,
    orderDirection: props.sortable ? props.order : null
  });

  useEffect(() => {
    props.dataStateChange(query);
  }, []);

  useEffect(
    () => {
      handleNewQuery({
        ...query,
        page: 0,
        search: debouncedSearchTerm
      });
    },
    [debouncedSearchTerm]
  );

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof TData
  ) => {
    const isDesc =
      query.orderBy === property && query.orderDirection === 'desc';

    handleNewQuery({
      ...query,
      orderDirection: isDesc ? 'asc' : 'desc',
      orderBy: property
    });
  };

  const handleNewQuery = (newQuery: EnhancedTableQuery<TData>) => {
    setQuery(newQuery);
    props.dataStateChange(newQuery);
  }

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelecteds = props.data.items.map(n => props.idAccessor(n));
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event: React.MouseEvent<unknown>, id: TKey) => {
    if (props.selectable) {
      const selectedIndex = selected.indexOf(id);
      let newSelected: TKey[] = [];

      if (selectedIndex === -1) {
        newSelected = newSelected.concat(selected, id);
      } else if (selectedIndex === 0) {
        newSelected = newSelected.concat(selected.slice(1));
      } else if (selectedIndex === selected.length - 1) {
        newSelected = newSelected.concat(selected.slice(0, -1));
      } else if (selectedIndex > 0) {
        newSelected = newSelected.concat(
          selected.slice(0, selectedIndex),
          selected.slice(selectedIndex + 1)
        );
      }

      setSelected(newSelected);
    }
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    handleNewQuery({
      ...query,
      page: newPage
    });
  };

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    handleNewQuery({
      ...query,
      pageSize: +event.target.value,
      page: 0
    });
  };

  const handleSearch = (searchText: string) => {
    setSearchTerm(searchText);
  };

  const isSelected = (id: TKey) => selected.indexOf(id) !== -1;

  const emptyRows: number =
    query.pageSize - Math.min(query.pageSize, props.data && props.data.items ? props.data.items.length : 0);

  const cellContent = (
    col: EnhancedTableColumn<TData>,
    row: TData
  ): React.ReactNode => {
    if (col.render) {
      return col.render(row);
    }

    const value = row[col.field.toString()];
    switch (col.dataType) {
      case 'number':
        return value;
      case 'boolean':
        return value === true ? (
          <CheckIcon />
        ) : value === false ? (
          <RemoveIcon />
        ) : null;
      case 'datetime':
        return (
          value && (
            <>
              <Moment format="L">{value}</Moment>&nbsp;
              <Moment format="LT">{value}</Moment>
            </>
          )
        );
      case 'date':
        return value && <Moment format="LT">{value}</Moment>;
      case 'time':
        return value && <Moment format="L">{value}</Moment>;
    }
    return value ? value.toString() : '';
  };

  const getColSpan = (): number => {
    let count = props.selectable ? columns.length + 1 : columns.length;
    if (props.actions.filter(a => !a.isFreeAction).length > 0) {
      count++;
    }
    return count;
  };

  const renderRowActions = (rowData: TData) => {
    const actions = props.actions.filter(a => !a.isFreeAction);
    return (
      <TableCell
        className={classes.th}
        size={'medium'}
        padding="none"
        key="key-actions-column"
        style={{ width: 44 * actions.length + 10, padding: '0px 5px' }}
      >
        <div>
          <EnhancedTableActions
            data={rowData}
            actions={actions}
            size={'medium'}
            inRow={true}
          />
        </div>
      </TableCell>
    );
  };

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <div className={props.loading ? "overlay visible" : "overlay"}>
          <div className="center-vertical">
            <CircularProgress color="primary" />
          </div>
        </div>

        <EnhancedTableToolbar
          title={props.title}
          numSelected={selected.length}
          search={props.searchable}
          searchText={searchTerm}
          onSearchChanged={handleSearch}
          actions={props.actions.filter(a => a.isFreeAction)}
        />
        <div className={classes.tableWrapper}>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size="medium"
          >
            <EnhancedTableHead
              columns={columns}
              rowsCount={props.data && props.data.items ? props.data.items.length : 0}
              selectable={props.selectable}
              numSelected={selected.length}
              onSelectAllClick={handleSelectAllClick}
              sortable={props.sortable}
              order={query.orderDirection}
              orderBy={query.orderBy}
              onRequestSort={handleRequestSort}
              actions={props.actions.filter(a => !a.isFreeAction)}
            />
            <TableBody>
              {!props.loadError && props.data && props.data.items &&
                props.data.items.map((row, index) => {
                  const isItemSelected = isSelected(props.idAccessor(row));
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      className={classes.tr}
                      hover
                      onClick={event =>
                        handleClick(event, props.idAccessor(row))
                      }
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={'page_' + query.page + 'row_' + index}
                      selected={isItemSelected}
                    >
                      {props.selectable ? (
                        <TableCell
                          className={classes.th}
                          padding="checkbox">
                          <Checkbox
                            checked={isItemSelected}
                            inputProps={{
                              'aria-labelledby': labelId
                            }}
                          />
                        </TableCell>
                      ) : null}

                      {columns.map(col => (
                        <TableCell
                          className={classes.th}
                          key={col.field.toString()}
                          align="left">
                          {cellContent(col, row)}
                        </TableCell>
                      ))}
                      {renderRowActions(row)}
                    </TableRow>
                  );
                })}
              {!props.loadError && props.data && props.data.items && props.data.items.length > 0 && emptyRows > 0 && (
                <TableRow className={classes.tr}>
                  <TableCell
                    className={classes.th}
                    colSpan={getColSpan()} />
                </TableRow>
              )}
              {!props.loadError && (!props.data || !props.data.items || props.data.items.length === 0) && (
                <TableRow>
                  {!props.loading && (
                    <TableCell
                      className={classes.th}
                      align="center"
                      colSpan={getColSpan()}>
                      No items to display
                    </TableCell>
                  )}
                </TableRow>
              )}
              {props.loadError && (
                <TableRow className={classes.tr}>
                  <TableCell
                    className={classes.th}
                    align="center"
                    colSpan={getColSpan()}>
                    <CommonError message="Error while loading data" />
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </div>
        {props.pageable ? (
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={props.data ? props.data.total : 0}
            rowsPerPage={query.pageSize}
            page={query.page}
            backIconButtonProps={{
              'aria-label': 'Previous Page'
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page'
            }}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        ) : (
            <Table>
              <TableRow>
                <TableCell
                  className={classes.th}
                  align="right">Total: {props.data ? props.data.total : 0} items</TableCell>
              </TableRow>
            </Table>
          )}
      </Paper>
    </div>
  );
};

export default EnhancedTable;
