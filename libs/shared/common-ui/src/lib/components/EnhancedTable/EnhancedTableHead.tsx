import {
  TableHead,
  TableRow,
  TableCell,
  Checkbox,
  TableSortLabel,
} from '@material-ui/core';
import {
  createStyles,
  makeStyles,
  Theme
} from '@material-ui/core/styles';
import {
  EnhancedTableColumn,
  EnhancedTableOrder,
  EnhancedTableActionDefinition
} from './models';
import React from 'react';


const useTableHeadStyles = makeStyles((theme: Theme) => {
  return createStyles({
    th: {
      color: '#8C8C8C !important',
      fontWeight: 'bold',
      borderBottomColor: '#DADADA',
      paddingTop: '0px'
    }
  });
});

export interface EnhancedTableHeadProps<TData> {
  columns: EnhancedTableColumn<TData>[];
  rowsCount: number;

  selectable: boolean;
  numSelected?: number;
  onSelectAllClick?: (
    event: React.ChangeEvent<HTMLInputElement>,
    checked: boolean
  ) => void;

  sortable: boolean;
  onRequestSort?: (
    event: React.MouseEvent<unknown>,
    property: keyof TData
  ) => void;
  order?: EnhancedTableOrder | '';
  orderBy?: keyof TData | '';

  actions: EnhancedTableActionDefinition<TData>[];
}

export const EnhancedTableHead = <Data extends object>(
  props: EnhancedTableHeadProps<Data>
) => {
  const classes = useTableHeadStyles(props);
  const createSortHandler = (property: keyof Data) => (
    event: React.MouseEvent<unknown>
  ) => {
    props.onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {props.selectable ? (
          <TableCell
            className={classes.th}
            padding="checkbox">
            <Checkbox
              indeterminate={
                props.numSelected > 0 && props.numSelected < props.rowsCount
              }
              checked={props.numSelected === props.rowsCount}
              onChange={props.onSelectAllClick}
              inputProps={{ 'aria-label': 'Select all' }}
            />
          </TableCell>
        ) : null}

        {props.columns.map(col => (
          <TableCell
            className={classes.th}
            key={col.field.toString()}
            align={col.dataType === 'number' ? 'right' : 'left'}
            padding={col.disablePadding ? 'none' : 'default'}
            sortDirection={
              props.sortable && col.sortable
                ? props.orderBy === col.field
                  ? props.order || null
                  : false
                : null
            }
          >
            {props.sortable && col.sortable ? (
              <TableSortLabel
                className={classes.th}
                active={props.orderBy === col.field}
                direction={props.order || null}
                onClick={createSortHandler(col.field)}
              >
                {col.label}
              </TableSortLabel>
            ) : (
                <>{col.label}</>
              )}
          </TableCell>
        ))}
        {props.actions.length > 0 && <TableCell />}
      </TableRow>
    </TableHead>
  );
};

export default EnhancedTableHead;
