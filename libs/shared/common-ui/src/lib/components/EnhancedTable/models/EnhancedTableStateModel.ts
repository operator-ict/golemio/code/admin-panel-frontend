import { AsyncData } from '../../../shared-ui-models';
import { EnhancedTableQueryResult } from './EnhancedTableQueryResultModel';
import { EnhancedTableQuery } from './EnhancedTableQueryModel';

export interface EnhancedTableState<TData> extends AsyncData<EnhancedTableQueryResult<TData>> {
    query?: EnhancedTableQuery<TData>;
}