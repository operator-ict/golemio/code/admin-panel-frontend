export interface EnhancedTableQueryResult<TData> {
    items: TData[];
    total: number;
}