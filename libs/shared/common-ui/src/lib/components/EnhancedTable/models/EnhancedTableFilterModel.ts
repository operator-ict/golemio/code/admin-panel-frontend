export interface EnhancedTableFilter<TData> {
    field: keyof TData;
    operator: string;
    value: any;
}