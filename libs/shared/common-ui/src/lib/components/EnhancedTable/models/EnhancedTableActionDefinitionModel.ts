import { IconProps } from '@material-ui/core/Icon';

export interface EnhancedTableActionDefinition<TData> {
    disabled?: boolean | ((data: TData) => boolean);
    icon: string;
    text?: string;
    isFreeAction?: boolean;
    tooltip?: string;
    onClick: (event: any, data: TData) => void;
    iconProps?: IconProps;
    hidden?: boolean | ((data: TData) => boolean);
}