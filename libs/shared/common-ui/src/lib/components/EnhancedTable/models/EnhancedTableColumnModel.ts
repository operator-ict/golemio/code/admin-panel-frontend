export interface EnhancedTableColumn<TData> {
    disablePadding?: boolean;
    field: keyof TData;
    label: string;
    dataType?: 'number' | 'date' | 'datetime' | 'time' | 'boolean';
    render?: (rowData: TData) => any;
    sortable?: boolean;
}
