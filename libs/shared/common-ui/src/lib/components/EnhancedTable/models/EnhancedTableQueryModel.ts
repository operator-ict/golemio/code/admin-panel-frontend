import { EnhancedTableFilter } from './EnhancedTableFilterModel';
import { EnhancedTableOrder } from './EnhancedTableOrderType';

export interface EnhancedTableQuery<TData> {
    filters: EnhancedTableFilter<TData>[];
    page: number;
    pageSize: number;
    search: string;
    orderBy: keyof TData | '';
    orderDirection: EnhancedTableOrder | '';
}