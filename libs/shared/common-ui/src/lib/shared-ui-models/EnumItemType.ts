export type EnumItem<TKey = number, TOthers = {}> = {
  id: TKey;
  name: string;
} & { [P in keyof TOthers]: TOthers[P] };
