export interface AsyncData<TData, TSaveResultData = any, TSaveErrorData = any> {
    loading?: boolean;
    loadError?: boolean;
    loadErrorMessage?: string;
    data?: TData;

    saving?: boolean;
    saveError?: boolean;
    saveResultData?: TSaveResultData;
    saveErrorData?: TSaveErrorData;
}