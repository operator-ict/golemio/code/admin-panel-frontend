export class CommonRegexUtils {
    // https://regexr.com/3grae
    // static routeTargetUrl = /^((https?:\/\/)((([A-Za-z\d_-]+\.)+)([A-Za-z\d_-]+)|(localhost))((:\d{2,4})?))$/;
    // muze tam byt i napr. https://input-gateway-v1:3000
    static routeTargetUrl = /^((https?:\/\/)([\.A-Z\d_-]+)((:\d{2,4})?))$/i;

    // https://regex101.com/r/kIowvx/2
    // static routeTargetUrl = /^((https?:\/\/)(?:localhost|[\w-]+(?:\.[\w-]+)+)(:\d+)?(\/\S*)?)$/;


    static startsWithSlash = /^\//;
    static notEndsWithSlash = /([^\/])$/;
}