const util = require('util');

const {
  parseFiles,
  getAffectedApps
} = require('@nrwl/workspace/src/command-line/shared');

function main() {
  let branch = null;
  let project = null;
  process.argv.forEach((val, index) => {
    if (val.includes('--branch=')) {
      const split = val.split('=');
      branch = split[1];
    }
    if (val.includes('--project=')) {
      const split = val.split('=');
      project = split[1];
    }
  });

  if (branch == null) {
    console.log('Specify branch using --branch=someBranch');
    return;
  }

  if (project == null) {
    console.log('Specify project using --project=someProject');
    return;
  }

  const affectedApps = getAffectedApps(
    parseFiles({
      base: 'origin/' + branch + '~1',
      head: 'origin/' + branch
    }).files
  );

  if (affectedApps.includes(project)) {
    console.log(1);
  } else {
    console.log(0);
  }
}
main();
