# Client Permission Proxy for Golemio

Permission proxy admin panel for the Golemio Data Platform System.

Developed by http://operatorict.cz

## Local installation

### Prerequisites

- node.js
- npm
- yarn

### Installation

1. Install all prerequisites
2. Install all dependencies using command: `yarn install` from the monorepo's root directory.
3. Create edit `src/environments/environment.ts`

### Compilation of typescript code

## Usage

## NPM Scripts

from the monorepo's root directory.

Local start

`yarn start`

Local build

`yarn build-local`

Dev start

`yarn start-dev`

Dev build

`yarn build-dev`

Prod start

`yarn start-prod`

Prod build

`yarn build-prod`

Local tests

`e2e-local`
`e2e-local-watch`

Dev tests

`e2e-dev`

Prod tests

`e2e-prod`

## Contribution guidelines

Please read [CONTRIBUTING.md](CONTRIBUTING.md).

## Problems?

Contact benak@operatorict.cz or vycpalek@operatorict.cz
