# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.11] - 2024-10-29

-   No changelog

## [1.1.10] - 2024-09-24

-   No changelog

## [1.1.9] - 2024-09-18

### Changed

-   Note for Golemio BI admins ([golemio-bi#44](https://gitlab.com/operator-ict/golemio/code/golemio-bi/gbi-general/-/issues/44))

## [1.1.8] - 2024-08-19

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.1.7] - 2024-06-05

### Changed

-   add new limitation is-empty ([p0255#63](https://gitlab.com/operator-ict/golemio/projekty/oict/p0255-datova-integrace-parkovist/-/issues/63))

## [1.1.6] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.1.5] - 2024-02-20

### Fixed

-   Fix honey pot ([golemio-bi#61]https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/61)

## [1.1.4] - 2024-02-07

### Removed

-   Remove Recaptcha ([golemio-bi#61]https://gitlab.com/operator-ict/golemio/code/golemio-bi/frontend/-/issues/61)

## [1.1.3] - 2023-05-10

### Added

-   Rate limit on role ([permission-proxy#150](https://gitlab.com/operator-ict/golemio/code/permission-proxy/-/issues/150))

## [1.1.2] - 2023-03-30

### Added

-   Min-max scope accepts date and datetime value ([admin-panel-frontend#19](https://gitlab.com/operator-ict/golemio/code/admin-panel-frontend/-/issues/3))

## [1.1.1] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.0] - 2023-01-23

### Changed

-   Docker image optimization

