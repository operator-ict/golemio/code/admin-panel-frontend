export const getLoginHeader = () => cy.get('.login-h');
export const getLoginSubmitBtn = () => cy.get('.submit-btn');
export const getLoginVersions = () => cy.get('.versions');
export const getLoginError = () => cy.get('.Mui-error');
export const getLoginEmailInput = () => cy.get('#email');
export const getLoginPasswordInput = () => cy.get('#password');
export const getForgottenPasswordLink = () => cy.get('.forgotten-password-link');
export const getForgottenPasswordHeader = () => cy.get('.forgotten-password-header');