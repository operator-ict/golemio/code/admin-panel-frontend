import { getLoginHeader, getLoginVersions, getLoginSubmitBtn, getLoginError, getLoginEmailInput, getLoginPasswordInput } from "./login.po";
import { getHomeHeader } from './common.po';
import { getLogoutBtn } from './logout.po';

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('login', (email: string, password: string) => {
    cy.server();

    getLoginHeader().should('be.visible');
    getLoginVersions().should('be.visible');
    getLoginSubmitBtn().should('be.visible');
    getLoginSubmitBtn().click();
    getLoginError().should('be.visible');
    getLoginEmailInput().type(email);
    getLoginPasswordInput().type(password);

    cy.route('POST', '**/login?**').as('login');
    getLoginSubmitBtn().click();

    cy.wait('@login', { requestTimeout: 20000 }).then(() => {
        getHomeHeader().should('be.visible');
    });
});

Cypress.Commands.add('logout', () => {
    getLogoutBtn().should('be.visible');
    getLogoutBtn().click();
    getLoginHeader().should('be.visible');
});