/// <reference path="../support/index.d.ts"/>

import { getHomeHeader } from "../support/common.po";
import { getForgottenPasswordLink, getForgottenPasswordHeader } from '../support/login.po';

describe('client-permission-proxy login logout tests', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('should log in and log out', () => {
    getHomeHeader().should('not.be.visible');
    getForgottenPasswordLink().click();
    getForgottenPasswordHeader().click();
    cy.get('#email').type('something123456789@something.cz');
    cy.get('.submit-btn').click();
    cy.get('.error-message').should('be.visible');
    cy.get('#email').clear().type(Cypress.env('email'));
    cy.get('.submit-btn').click();
    //  cy.get('.error-message').should('be.visible');
  });
});
