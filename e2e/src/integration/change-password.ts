/// <reference path="../support/index.d.ts"/>

import { getHomeHeader, getSettings, getSaveButton } from "../support/common.po";

describe('client-permission-proxy login logout tests', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.login(Cypress.env('email'), Cypress.env('password'));
  });

  it('should change password, log in and log out and change back', () => {
    getHomeHeader().should('visible');
    changePassword(Cypress.env('password'), Cypress.env('passwordTempNew'));
    cy.logout();
    cy.login(Cypress.env('email'), Cypress.env('passwordTempNew'));
    changePassword(Cypress.env('passwordTempNew'), Cypress.env('password'));
    cy.logout();
    cy.login(Cypress.env('email'), Cypress.env('password'));
  });

  const changePassword = (oldPassword: string, newPassword: string) => {
    getSettings().click();
    cy.get('[id="oldPassword"]').type(oldPassword);
    cy.get('[id="newPassword"]').type(newPassword);
    cy.get('[id="newPasswordConfirmation"]').type(newPassword);
    getSaveButton().click();
  };
});
