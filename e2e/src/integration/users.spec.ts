/// <reference path="../support/index.d.ts"/>

import { getTable, getSearchInput, getNotSelectedLinks, getAddButton, getDialogContent, getFirstListItem, getSaveButton, getError, getBody, getHomeHeader, getDeleteButtonByIndex } from "../support/common.po";


describe('client-permission-proxy users tests', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.login(Cypress.env('email'), Cypress.env('password'));
  });

  it('users table settings should be saved', () => {
    cy.route('GET', '**/users?**').as('search');

    const searchValue = 'admin';
    getTable().should('be.visible');
    cy.get('table tr').its('length').should('be.greaterThan', 0);
    getSearchInput().type(searchValue);

    cy.wait('@search').then(() => {
      getNotSelectedLinks().eq(0).click();
      getNotSelectedLinks().eq(0).click();
      getSearchInput().should('have.value', searchValue);
      cy.get('table tr').should('contain.have.text', searchValue);
    });
  });

  it('user should be successfully added, edited and deleted', () => {
    addUser();
    editUser();
    deleteUser();
  });

  const fillForm = (
    firstName: string,
    lastName: string,
    count: number,
    timeWindow: number,
    email: string = null
  ) => {
    cy.get('input[id="firstName"]').clear().type(firstName);
    cy.get('input[id="lastName"]').clear().type(lastName);
    if (email != null) {
      cy.get('input[id="email"]').clear().type(email);
      cy.get('[id="select-referer"]').click();
      getFirstListItem().click();
    } else {
      cy.get('input[id="email"]').should('be.disabled');
    }
    cy.get('[id="select-roles"]').click();
    getFirstListItem().click();
    getBody().click(10, 10);
    cy.get('input[id="rateLimit"]').then(x => {
      if (!x.is(':checked')) {
        cy.get('input[id="rateLimit"]').click();
      }
      cy.get('input[id="count"]').should('be.enabled');
      cy.get('input[id="timeWindow"]').should('be.enabled');
      cy.get('input[id="count"]').clear().type(count.toString());
      cy.get('input[id="timeWindow"]').clear().type(timeWindow.toString());
    });
  }

  const addUser = () => {
    getAddButton().click();
    getDialogContent().should('be.visible');

    getSaveButton().click();
    getError().should('be.visible');

    const firstName = Cypress.env('testUserName');
    const lastName = Cypress.env('testUserName');
    const email = Cypress.env('testUserEmail');
    const count = 5;
    const timeWindow = 5;
    fillForm(firstName, lastName, count, timeWindow, email);

    cy.route('POST', '**/users?**').as('add');
    getSaveButton().click();

    cy.wait('@add', { requestTimeout: 20000 }).then(() => {
      checkExistingRow(firstName, lastName, email);
    });
  };

  const editUser = () => {
    cy.get('table tr:nth-child(1) button').first().click();
    getDialogContent().should('be.visible');

    const firstName = 'firstNameTest1Edit';
    const lastName = 'lastNameTest1Edit';
    const count = 5;
    const timeWindow = 5;
    fillForm(firstName, lastName, count, timeWindow);

    cy.route('PATCH', '**/users/**').as('edit');
    getSaveButton().click();

    cy.wait('@edit', { requestTimeout: 20000 }).then(() => {
      checkExistingRow(firstName, lastName);
    });
  };

  const checkExistingRow = (
    firstName: string,
    lastName: string,
    email: string = null
  ) => {
    getHomeHeader().should('be.visible');

    cy.route('GET', '**/users?**').as('search');

    getSearchInput().clear().type(email != null ? email : firstName);

    cy.wait('@search').then(() => {
      cy.get('table tr').should('contain.have.text', firstName);
      cy.get('table tr').should('contain.have.text', lastName);
      if (email != null) {
        cy.get('table tr').should('contain.have.text', email);
      }
    });
  }

  const deleteUser = () => {
    getSearchInput().clear().type(Cypress.env('testUserEmail'));
    getDeleteButtonByIndex(0).click();
    getSaveButton().click();
  }
});
