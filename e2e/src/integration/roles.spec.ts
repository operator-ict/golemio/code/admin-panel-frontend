/// <reference path="../support/index.d.ts"/>

import { getTab, getTable, getSearchInput, getFirstListItem, getHomeHeader, getAddButton, getDialogContent, getSaveButton, getError, getDeleteButtonByIndex } from "../support/common.po";

describe('client-permission-proxy roles tests', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.login(Cypress.env('email'), Cypress.env('password'));
    getTab(1).click();
  });


  it('roles table settings should be saved', () => {
    cy.route('GET', '**/roles?**').as('search');

    const searchValue = 'Opendatar';
    getTable().should('be.visible');
    cy.get('table tr').its('length').should('be.greaterThan', 0);
    getSearchInput().type(searchValue);


    cy.wait('@search').then(() => {
      getSearchInput().should('have.value', searchValue);
      cy.get('table tr').should('contain.have.text', searchValue);
      getTab(2).click();
      getTab(1).click();
      getSearchInput().should('have.value', searchValue);
      cy.get('table tr').should('contain.have.text', searchValue);
    });
  });

  it('roles should be successfully added, edited and deleted', () => {
    addRow();
    editRow();
    deleteRow(0);
  });

  const addRow = () => {
    getAddButton().click();
    getDialogContent().should('be.visible');

    getSaveButton().click();
    getError().should('be.visible');

    const name = 'nameTest1';
    const description = 'descriptionTest1';
    fillForm(name, description);

    cy.route('POST', '**/role?**').as('add');
    getSaveButton().click();

    cy.wait('@add', { requestTimeout: 20000 }).then(() => {
      checkExistingRow(name, description);
    });
  }

  const editRow = () => {
    cy.get('table tr:nth-child(1) button.edit:not(.edit-scope)').first().click();
    getDialogContent().should('be.visible');

    const name = 'nameTestEdit1';
    const description = 'descriptionTestEdit1';
    fillForm(name, description);

    cy.route('PATCH', '**/roles/** ').as('edit');
    getSaveButton().click();

    cy.wait('@edit', { requestTimeout: 20000 }).then(() => {
      checkExistingRow(name, description);
    });
  }

  const fillForm = (name: string, description: string) => {
    cy.get('input[id="name"]').clear().type(name);
    cy.get('input[id="description"]').clear().type(description);
    cy.get('[id=select-parentId]').click();
    getFirstListItem().click();
  }

  const checkExistingRow = (name: string, description: string) => {
    getHomeHeader().should('be.visible');
    getSearchInput().clear().type(name);
    cy.get('table tr').should('contain.have.text', name);
    cy.get('table tr').should('contain.have.text', description);
  };

  const deleteRow = (index: number) => {
    getDeleteButtonByIndex(index).click();
    getSaveButton().click();
  }
});
