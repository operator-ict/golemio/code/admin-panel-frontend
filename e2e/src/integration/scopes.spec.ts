/// <reference path="../support/index.d.ts"/>

import { getTab, getAddButton, getDialogContent, getSaveButton, getError, getFirstListItem, getHomeHeader, getDeleteButtonByIndex, getSearchInput, getSearchInputLast } from "../support/common.po";


describe('client-permission-proxy scopes tests', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.login(Cypress.env('email'), Cypress.env('password'));
    goToScopes();
  });

  it('scopes tables settings should be saved', () => {
    cy.route('GET', '**/scopes?**').as('search');

    const search = 'something';
    getSearchInput().type(search);
    getSearchInputLast().type(search);

    cy.wait('@search').then(() => { // debounce both searches
      cy.wait(2000).then(() => {
        goToScopes();
        getSearchInput().should('have.value', search);
        getSearchInputLast().should('have.value', search);
      });
    });
  });

  const goToScopes = () => {
    getTab(1).click();
    cy.get('table tr:nth-child(1) button').first().click();
    cy.get('.scopes-routes').should('be.visible');
  }

  it('scope should be successfully added, edited and deleted', () => {
    addRow();
    editRow();
    deleteRow(0);
  });

  const addRow = () => {
    getAddButton().click();
    getDialogContent().should('be.visible');

    getSaveButton().click();
    getError().should('be.visible');

    const parameter = 'test';
    const values = 'test';
    fillForm(parameter, values);

    cy.route('POST', '**//*roles/**').as('add');
    getSaveButton().click();

    cy.wait('@add', { requestTimeout: 20000 }).then(() => {
      checkExistingRow(parameter);
    });
  }

  const editRow = () => {
    cy.get('table tr button').first().click();
    getDialogContent().should('be.visible');

    cy.get('.limitation-form .delete').click();

    const parameter = 'test';
    const values = 'test';
    fillForm(parameter, values);

    cy.route('PATCH', '**//*roles/**').as('edit');
    getSaveButton().click();

    cy.wait('@edit', { requestTimeout: 20000 }).then(() => {
      checkExistingRow(parameter);
    });
  }

  const fillForm = (parameter: string, values: string) => {
    cy.get('[id=routeId]').clear().type('a');
    cy.get('.scope-form .MuiListItem-button').then(x => x.first().click());
    cy.get('.add-new-item').click();
    fillItemForm(parameter, values);
  }

  const fillItemForm = (parameter: string, values: string) => {
    cy.get('[id=select-type]').click();
    getFirstListItem().click();
    cy.get('[id=parameter]').type(parameter);
    cy.get('[id=valuesList]').type(values);
    cy.get('.limitation-form button[type="submit"]').click();
  };

  const checkExistingRow = (parameter: string) => {
    getHomeHeader().should('be.visible');
    cy.get('table tr').should('contain.have.text', parameter);
  };

  const deleteRow = (index: number) => {
    getDeleteButtonByIndex(index).click();
    getSaveButton().click();
  }
});
