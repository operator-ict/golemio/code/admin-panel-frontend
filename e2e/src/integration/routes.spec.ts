/// <reference path="../support/index.d.ts"/>

import { getTable, getSearchInput, getTab, getFirstListItem, getSaveButton, getAddButton, getDialogContent, getError, getHomeHeader, getDialogTitle, getDeleteButtonByIndex } from "../support/common.po";


describe('client-permission-proxy routes tests', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.login(Cypress.env('email'), Cypress.env('password'));
    getTab(2).click();
  });

  it('routes table settings should be saved', () => {
    cy.route('GET', '**/routes?**').as('search');

    const searchValue = 'GET';
    getTable().should('be.visible');
    cy.get('table tr').its('length').should('be.greaterThan', 0);
    getSearchInput().type(searchValue);

    cy.wait('@search').then(() => {
      getSearchInput().should('have.value', searchValue);
      cy.get('table tr').should('contain.have.text', searchValue);
      getTab(0).click();
      getTab(2).click();
      getSearchInput().should('have.value', searchValue);
      cy.get('table tr').should('contain.have.text', searchValue);
    });
  });

  it('route should be successfully added, edited and deleted', () => {
    addRow();
    editRow();
    deleteRow(0);
  });

  const addRow = () => {
    getAddButton().click();
    getDialogContent().should('be.visible');

    getSaveButton().click();
    getError().should('be.visible');

    const uniqueNumber = new Date().valueOf();
    const endpoint = '/' + uniqueNumber;
    const target = 'http://localhost:4321';
    const targetPath = '/targetpath';
    fillForm(endpoint, target, targetPath);

    cy.route('POST', '**/route?**').as('add');
    getSaveButton().click();

    cy.wait('@add', { requestTimeout: 20000 }).then(() => {
      checkExistingRow(endpoint, target, targetPath);
    });
  }

  const editRow = () => {
    cy.get('table tr:nth-child(1) button').first().click();
    getDialogContent().should('be.visible');

    const uniqueNumber = new Date().valueOf();
    const endpoint = '/' + uniqueNumber;
    const target = 'http://localhost:4322';
    const targetPath = '/targetpathedited';
    fillForm(endpoint, target, targetPath);

    cy.route('PATCH', '**/routes/**').as('edit');
    getSaveButton().click();

    cy.wait('@edit', { requestTimeout: 20000 }).then(() => {
      checkExistingRow(endpoint, target, targetPath);
    });
  }

  const fillForm = (endpoint: string, target: string, targetPath: string) => {
    cy.get('[id=select-method]').click();
    getFirstListItem().click();
    cy.get('input[id="endpoint"]').clear().type(endpoint);
    cy.get('input[id="target"]').clear().type(target);
    getDialogTitle().click();
    cy.get('input[id="targetPath"]').clear().type(targetPath);
  }

  const checkExistingRow = (endpoint: string, target: string, targetPath: string) => {
    getHomeHeader().should('be.visible');
    getSearchInput().clear().type(endpoint);
    cy.get('table tr').should('contain.have.text', target);
    cy.get('table tr').should('contain.have.text', targetPath);
  };

  const deleteRow = (index: number) => {
    getDeleteButtonByIndex(index).click();
    getSaveButton().click();
  }
});
