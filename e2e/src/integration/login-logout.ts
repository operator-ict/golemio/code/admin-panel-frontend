/// <reference path="../support/index.d.ts"/>

import { getHomeHeader } from "../support/common.po";
import { getLoginHeader } from '../support/login.po';

describe('client-permission-proxy login logout tests', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('should log in and log out', () => {
    cy.login(Cypress.env('email'), Cypress.env('password'));
    cy.logout();
    cy.visit('/admin');
    getHomeHeader().should('not.be.visible');
    getLoginHeader().should('be.visible');
  });
});
