import { observer } from 'mobx-react';
import React, { useContext, useEffect } from 'react';
import Moment from 'react-moment';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import 'react-virtualized/styles.css';
import { environment } from '../../environments/environment';
import { authStore } from '../store';
import './App.scss';
import { ErrorPage } from './error';
import { HomePage } from './home';
import { LoginPage, ForgottenPassword, PasswordReset } from './login';

// Set the locale for every react-moment instance to Czech.
Moment.globalLocale = 'cs';


export const App = observer(() => {

  useEffect(() => {
    authStore.checkToken();
  }, []);

  return (
    <BrowserRouter basename={environment.prefix}>
      <Switch>
        <Route path="/error" component={ErrorPage} />
        <Route path="/login" component={LoginPage} />
        <Route path="/forgotten-password" component={ForgottenPassword} />
        <Route path="/password-reset/:code" component={PasswordReset} />
        {authStore.tokenChecked && <>
          {authStore.authenticated &&
            <Route path="/" component={HomePage} />
          }
          {!authStore.authenticated &&
            <Redirect to="/login" />
          }
        </>}
      </Switch>
    </BrowserRouter>
  );
});

export default App;
