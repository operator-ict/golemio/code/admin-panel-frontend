import { Chip, Tooltip } from '@material-ui/core';
import React from 'react';
import { LightTooltip } from "@dataplatform/shared/common-ui"
import { LimitationModel, LimitationFormModel } from '../../../models';
import './LimitationChip.scss';
import { LimitationDetail } from '../LimitationDetail/LimitationDetail';


export interface LimitationChipProps {
  data: LimitationModel | LimitationFormModel;
  handleClick?: () => void;
  handleDelete?: () => void;
}

export const LimitationChip = (props: LimitationChipProps) => {

  const renderTooltip = () => {
    return (props.data ? <LimitationDetail data={props.data} /> : '');
  }

  return (
    <LightTooltip
      title={renderTooltip()}
    >
      <Chip
        label={props.data ? '?' + props.data.parameter : '+ add'}
        size="small"
        clickable
        onClick={props.handleClick}
        onDelete={props.handleDelete}
        className="limitation-chip"
        color={props.data ? "default" : "primary"}
      />
    </LightTooltip>
  );
};

export default LimitationChip;
