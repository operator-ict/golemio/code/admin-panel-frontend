import { EnhancedTable, EnhancedTableColumn, EnhancedTableQuery, EnhancedTableState, ConfirmDialog } from '@dataplatform/shared/common-ui';
import { observer } from 'mobx-react';
import React, { useContext, useState } from 'react';
import { ScopeListItemModel, ScopeSaveDataModel } from '../../../models';
import { limitationsStore, scopesStore } from '../../../store';
import LimitationChip from '../LimitationChip/LimitationChip';
import ScopeDialog from '../ScopeDialog/ScopeDialog';
import './ScopesRoutes.scss';
import RouteMethod from '../../home/RouteMethod/RouteMethod';


export interface ScopesRoutesProps {
  roleName: string;

  list: EnhancedTableState<ScopeListItemModel>;
  dataStateChange: (query: EnhancedTableQuery<ScopeListItemModel>) => void;
  handleOpen: (rowData: ScopeListItemModel) => void;
  handleClose: () => void;
  handleSave: (data: ScopeSaveDataModel) => void;
  handleDelete: (scopeId: number) => void;
}

export const ScopesRoutes = observer((props: ScopesRoutesProps) => {

  const [showConfirm, setShowConfirm] = useState<boolean>(false);
  const [removeId, setRemoveId] = useState<number>(null);

  const columns: EnhancedTableColumn<ScopeListItemModel>[] = [
    {
      field: 'method',
      label: 'METHOD',
      render: (x: ScopeListItemModel) => <RouteMethod method={x.method}></RouteMethod>
    },
    {
      field: 'routeName',
      label: 'ROUTE',
      render: (x: ScopeListItemModel) => <span className="fw-bold">{x.routeName}</span>
    },
    {
      field: 'limitations',
      label: 'LIMITATIONS',
      sortable: false,
      render: rowData => {
        return <>
          {rowData.limitations.map((limitation, index) =>
            <LimitationChip
              key={index}
              data={limitation} />
          )}
        </>
      }
    },
    {
      field: 'updated',
      label: 'UPDATED',
      dataType: 'datetime'
    }
  ];

  const idAccessor = (row: ScopeListItemModel): number => {
    return row.id;
  };

  const dataStateChange = (query: EnhancedTableQuery<ScopeListItemModel>) => {
    props.dataStateChange(query);
  };

  const handleRemove = (data: ScopeListItemModel) => {
    setRemoveId(data.id);
    setShowConfirm(true);
  };

  const handleRemoveConfirm = () => {
    setRemoveId(null);
    setShowConfirm(false);
    props.handleDelete(removeId)
  };


  return (
    <div className="scopes-routes">
      <ScopeDialog
        isOpened={scopesStore.dialogState === 'open'}
        roleName={props.roleName}
        limitationTypes={limitationsStore.enums}
        loading={scopesStore.detail.loading}
        loadError={scopesStore.detail.loadError}
        data={scopesStore.detail.data}
        handleSave={props.handleSave}
        saving={scopesStore.detail.saving}
        saveError={scopesStore.detail.saveError}
        saveErrorData={scopesStore.detail.saveErrorData}
        handleClose={props.handleClose}
      />
      <ConfirmDialog
        title="Remove Scope"
        message="Do you really want to remove this scope?"
        open={showConfirm}
        handleCancel={() => setShowConfirm(false)}
        handleOk={() => handleRemoveConfirm()}
      />
      <EnhancedTable
        loading={props.list.loading}
        loadError={props.list.loadError}
        data={props.list.data}
        columns={columns}
        dataStateChange={dataStateChange}
        idAccessor={idAccessor}
        selectable={false}
        sortable={true}
        order={props.list.query.orderDirection}
        orderBy={props.list.query.orderBy}
        pageable={true}
        page={props.list.query.page}
        pageSize={props.list.query.pageSize}
        searchable={true}
        searchText={props.list.query.search}
        actions={[
          {
            icon: null,
            text: '+ Add new',
            isFreeAction: true,
            onClick: () => props.handleOpen(null)
          },
          {
            icon: 'edit',
            tooltip: 'Edit Scope',
            isFreeAction: false,
            onClick: (event, rowData) => props.handleOpen(rowData)
          },
          {
            icon: 'delete',
            tooltip: 'Remove',
            isFreeAction: false,
            onClick: (event, rowData) => handleRemove(rowData)
          },
        ]}
      />
    </div>
  );
});

export default ScopesRoutes;
