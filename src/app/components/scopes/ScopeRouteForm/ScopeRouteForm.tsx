import { Button, FormControl, FormHelperText, IconButton, InputAdornment, InputLabel, ListItem, ListItemText, TextField, Tooltip, CircularProgress } from '@material-ui/core';
import ClearIcon from '@material-ui/icons/Clear';
import SearchIcon from '@material-ui/icons/Search';
import { Formik, FormikActions } from 'formik';
import React, { useState, useEffect } from 'react';
import { List, ListRowProps } from "react-virtualized";
import * as Yup from 'yup';
import { RouteListItemModel } from '../../../models';
import { routesService } from '../../../services';
import './ScopeRouteForm.scss';


export interface ScopeRouteFormModel {
  search: string;
  selectedId: number | '';
}

export interface ScopeRouteFormProps {
  roleName: string;
  isEdit?: boolean;
  submitted: (item: RouteListItemModel) => void;
  handleClose: () => void;
}

export const ScopeRouteForm = (props: ScopeRouteFormProps) => {

  const [items, setItems] = useState<RouteListItemModel[]>([]);
  const [loading, setLoading] = useState<boolean>(false);

  const searchRoutes = async (search: string, setFieldValue: (field: string, value: any) => void, formikValues: ScopeRouteFormModel) => {
    setLoading(true);
    try {
      const result = await routesService.getAll({
        search: search,
        filters: [],
        orderBy: null,
        orderDirection: 'asc',
        page: 0,
        pageSize: 100
      });

      if (setFieldValue && formikValues) {
        const found = result.items.find(x => x.id === formikValues.selectedId);
        if (!found) {
          setFieldValue('selectedId', '');
        }
      }

      setItems(result.items);
      setLoading(false);
    } catch{
      setLoading(false);
    }
  };

  useEffect(() => {
    searchRoutes('', null, null);
  }, []);

  const rowRenderer = (listRowProps: ListRowProps & {
    setFieldValue: (field: string, value: any) => void;
    setFieldTouched: (field: string, isTouched?: boolean) => void;
    formikValues: ScopeRouteFormModel
  }): React.ReactNode => {
    const value = items[listRowProps.index];
    return (
      <ListItem
        key={listRowProps.key}
        style={listRowProps.style}
        onClick={() => {
          listRowProps.setFieldValue('selectedId', value.id);
          listRowProps.setFieldTouched("selectedId", true);
        }}
        className={listRowProps.formikValues.selectedId === value.id ? "selected" : ""}
      >
        <ListItemText>
          <span className="item-method">{value.method}</span>
          <span className="item-endpoint">{value.endpoint}</span>
        </ListItemText>
      </ListItem>
    );
  };

  return (
    <div className="scope-route-form">
      <h1>{props.roleName}</h1>
      <h2>Add New Route</h2>
      <Formik
        initialValues={{ search: '', selectedId: '' }}
        onSubmit={(values: ScopeRouteFormModel, actions: FormikActions<ScopeRouteFormModel>) => {
          const selected = items.find(x => x.id === values.selectedId);
          props.submitted(selected);
        }}
        validationSchema={Yup.object().shape({
          selectedId: Yup.string()
            .required('Please select route')
        })}
      >
        {formikProps => {
          const {
            values,
            touched,
            errors,
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue,
            setFieldTouched,
          } = formikProps;
          return (
            <div>
              <form onSubmit={handleSubmit} noValidate>

                <TextField
                  error={errors.search && touched.search}
                  type="name"
                  id="search"
                  label="Search Route"
                  placeholder="Start typing to filter routes..."
                  margin="normal"
                  onChange={(e) => {
                    handleChange(e);
                    searchRoutes(e.target.value, setFieldValue, values);
                  }}
                  onBlur={handleBlur}
                  value={values.search}
                  className="text-item"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <Tooltip title="Search">
                          <SearchIcon color="inherit" fontSize="small" />
                        </Tooltip>
                      </InputAdornment>
                    ),
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          disabled={!values.search}
                          onClick={() => {
                            setFieldValue('search', '');
                            searchRoutes('', setFieldValue, values);
                          }}
                        >
                          <ClearIcon color="inherit" fontSize="small" />
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />

                <InputLabel shrink style={{ marginTop: '16px' }}>Available routes</InputLabel>
                <div className="list-wrapper">
                  <div className={loading ? "overlay visible" : "overlay"}>
                    <div className="center-vertical">
                      <CircularProgress color="primary" />
                    </div>
                  </div>

                  <FormControl
                    fullWidth={true}
                    margin="normal"
                    // required
                    error={errors.selectedId && touched.selectedId ? true : false}
                    className={errors.selectedId && touched.selectedId ? 'error' : ''}
                  >
                    <List
                      className="routes-list"
                      id="selectedId"
                      name="selectedId"
                      width={350}
                      style={{ width: '100%' }}
                      height={200} rowHeight={30}
                      rowCount={items.length}
                      rowRenderer={(listRowProps: ListRowProps) => {
                        return rowRenderer({
                          ...listRowProps,
                          setFieldValue: setFieldValue,
                          setFieldTouched: setFieldTouched,
                          formikValues: values
                        });
                      }} />
                    {(errors.selectedId && touched.selectedId) && <FormHelperText>
                      {errors.selectedId}
                    </FormHelperText>}
                  </FormControl>
                </div>
                <Button fullWidth={true} color="primary" variant="contained" type="submit">{props.isEdit ? "Edit route" : "Add route"}</Button>
                <Button fullWidth={true} color="primary" variant="contained" type="button" onClick={props.handleClose}>Cancel</Button>
              </form>

            </div>
          );
        }}
      </Formik>
    </div>
  );
};

export default ScopeRouteForm;
