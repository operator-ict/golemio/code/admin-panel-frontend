import { EnhancedTable, EnhancedTableColumn, EnhancedTableQuery, EnhancedTableState } from '@dataplatform/shared/common-ui';
import React from 'react';
import { observer } from 'mobx-react';
import { ScopeListItemModel } from '../../../models';
import { LimitationChip } from '../LimitationChip/LimitationChip';
import './ScopesInheritedRoutes.scss';
import RouteMethod from '../../home/RouteMethod/RouteMethod';


export interface ScopesInheritedRoutesProps {
  list: EnhancedTableState<ScopeListItemModel>;
  dataStateChange: (query: EnhancedTableQuery<ScopeListItemModel>) => void;
}

export const ScopesInheritedRoutes = observer((props: ScopesInheritedRoutesProps) => {

  const columns: EnhancedTableColumn<ScopeListItemModel>[] = [
    {
      field: 'method',
      label: 'METHOD',
      render: (x: ScopeListItemModel) => <RouteMethod method={x.method}></RouteMethod>
    },
    {
      field: 'routeName',
      label: 'ROUTE',
      render: (x: ScopeListItemModel) => <span className="fw-bold">{x.routeName}</span>
    },
    {
      field: 'limitations',
      label: 'LIMITATIONS',
      sortable: false,
      render: rowData => {
        return <>
          {rowData.limitations.map((limitation, index) =>
            <LimitationChip
              key={index}
              data={limitation} />
          )}
        </>
      }
    },
    {
      field: 'updated',
      label: 'UPDATED',
      dataType: 'datetime'
    }
  ];

  const idAccessor = (row: ScopeListItemModel): number => {
    return row.id;
  };

  const dataStateChange = (query: EnhancedTableQuery<ScopeListItemModel>) => {
    props.dataStateChange(query);
  };


  return (
    <div>
      <EnhancedTable
        loading={props.list.loading}
        loadError={props.list.loadError}
        data={props.list.data}
        columns={columns}
        dataStateChange={dataStateChange}
        idAccessor={idAccessor}
        selectable={false}
        sortable={true}
        order={props.list.query.orderDirection}
        orderBy={props.list.query.orderBy}
        pageable={true}
        page={props.list.query.page}
        pageSize={props.list.query.pageSize}
        searchable={true}
        searchText={props.list.query.search}
        actions={[]}
      />
    </div>
  );
});

export default ScopesInheritedRoutes;
