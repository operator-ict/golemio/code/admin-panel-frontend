import React, { useEffect, useContext } from 'react';

import './ScopesPage.scss';
import ScopesRoutes from '../ScopesRoutes/ScopesRoutes';
import ScopesInheritedRoutes from '../ScopesInheritedRoutes/ScopesInheritedRoutes';
import { scopesStore } from '../../../store';
import { observer } from 'mobx-react';
import { RouteComponentProps, NavLink } from 'react-router-dom';
import {
  CommonError,
  EnhancedTableQuery
} from '@dataplatform/shared/common-ui';
import { ScopeListItemModel, ScopeSaveDataModel } from '../../../models';
import { environment } from 'src/environments/environment';

export const ScopesPage = observer((props: RouteComponentProps) => {
  const roleId = parseInt(props.match.params['id'], 10);

  useEffect(() => {
    scopesStore.loadRole(roleId);
  }, [props.match.params['id']]);

  const dataStateChange = (
    query: EnhancedTableQuery<ScopeListItemModel>
  ): void => {
    scopesStore.loadList(roleId, query);
  };

  const handleOpen = (rowData: ScopeListItemModel) => {
    scopesStore.openDetail(roleId, rowData ? rowData.id : null);
  };

  const handleClose = () => {
    scopesStore.closeDetail();
  };

  const handleSave = (data: ScopeSaveDataModel) => {
    scopesStore.saveDetail(data, () => {
      scopesStore.refreshList(roleId);
    });
  };

  const handleDelete = (scopeId: number) => {
    scopesStore.delete(roleId, scopeId, () => {
      scopesStore.refreshList(roleId);
    });
  };

  const inheritedRoutesDataStateChange = (
    query: EnhancedTableQuery<ScopeListItemModel>
  ): void => {
    scopesStore.loadInheritedList(query);
  };

  return (
    <div style={{ position: 'relative' }}>
      <div className="headers">
        <h2>Role Scopes</h2>
        <div className="subheader">
          <NavLink className="link" to="/roles">
            Roles
          </NavLink>{' '}
          / Scopes of role{' '}
          {scopesStore.role.data != null ? (
            <strong>{scopesStore.role.data.name}</strong>
          ) : (
            ''
          )}
        </div>
      </div>
      {scopesStore.role.loadError && (
        <CommonError
          message={
            scopesStore.role.loadErrorMessage || 'Error while loading data'
          }
        />
      )}

      {!scopesStore.role.loadError && scopesStore.role.data && (
        <div>
          <ScopesRoutes
            roleName={scopesStore.role.data.name}
            list={scopesStore.list}
            dataStateChange={dataStateChange}
            handleOpen={handleOpen}
            handleClose={handleClose}
            handleSave={handleSave}
            handleDelete={handleDelete}
          />

          {!scopesStore.role.data.isParent && (
            <>
              <div className="headers">
                <h2>Parent Role Scopes</h2>
                <div className="subheader">
                  <span>
                    Inherited scopes of role{' '}
                    {scopesStore.role.data != null ? (
                      <strong>{scopesStore.role.data.name}</strong>
                    ) : (
                      ''
                    )}
                  </span>
                  {scopesStore.role.data != null &&
                    scopesStore.role.data.parentId != null && (
                      <span>
                        {' '}
                        from role{' '}
                        <NavLink
                          className="link"
                          to={`/roles/${scopesStore.role.data.parentId}/scopes`}
                        >
                          {scopesStore.role.data.parentName}
                        </NavLink>
                      </span>
                    )}
                </div>
              </div>

              <ScopesInheritedRoutes
                list={scopesStore.inheritedList}
                dataStateChange={inheritedRoutesDataStateChange}
              />
            </>
          )}
        </div>
      )}
    </div>
  );
});

export default ScopesPage;
