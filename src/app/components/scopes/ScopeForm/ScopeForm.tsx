import { CommonError, ComboBox, ComboBoxOptionModel } from '@dataplatform/shared/common-ui';
import { Button, Grid } from '@material-ui/core';
import { FormikProps } from 'formik';
import React from 'react';
import { ScopeFormModel } from '../../../models';
import { routesService } from '../../../services';
import './ScopeForm.scss';

export type ScopeFormProps = FormikProps<ScopeFormModel> & {
  roleName: string;
  isNew: boolean;
  handleClose: () => void;
  handleLimitationDelete: (index: number) => void;
  loadError: boolean;
  saving: boolean;
}

export const ScopeForm = (props: ScopeFormProps) => {
  const {
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    handleClose,
    loadError,
    setFieldValue,
    saving
  } = props;

  const fetch = async (inputValue: string): Promise<ComboBoxOptionModel[]> => {
    const result = await routesService.getAll({
      search: inputValue,
      filters: [],
      orderBy: null,
      orderDirection: 'asc',
      page: 0,
      pageSize: 100
    });
    const options = await result.items.map(x => {
      const option: ComboBoxOptionModel = {
        value: x.id,
        label: `${x.method} ${x.endpoint}`
      };
      return option;
    });
    return options;
  };

  return (
    <form onSubmit={handleSubmit} noValidate className="scope-form">
      <ComboBox
        id="routeId"
        value={values.route}
        label="Route"
        placeholder="Search route name..."
        fetch={(search: string) => {
          return fetch(search);
        }}
        onBlur={handleBlur}
        onChange={(item: ComboBoxOptionModel) => {
          setFieldValue('routeId', item ? item.value : null);
        }}
        error={errors.routeId && touched.routeId}
        helperText={(errors.routeId && touched.routeId) && 'Select route'} />
      {props.loadError &&
        <CommonError message="Error while loading data" />
      }
      <Grid item xs={12} className="buttons absolute-dialog-btns">
        <Button onClick={handleClose} color="primary">
          Cancel
        </Button>
        {!loadError ? (
          <Button
            type="submit"
            disabled={isSubmitting && saving}
            color="primary"
            autoFocus
          >
            {props.isNew ? 'Save' : 'Save'}
          </Button>
        ) : null}
      </Grid>
    </form>
  );
};

export default ScopeForm;
