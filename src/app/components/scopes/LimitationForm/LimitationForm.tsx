import {
  Box,
  Button,
  FormControl,
  FormHelperText,
  FormLabel,
  Grid,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  Switch,
  TextField,
  Tooltip
} from '@material-ui/core';
import { Formik, FormikActions } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import moment from "moment";
import { LimitationFormModel, LimitationTypeEnum, MinMaxTypeEnum } from '../../../models';
import './LimitationForm.scss';
import ChipInput from 'material-ui-chip-input';
import DeleteIcon from '@material-ui/icons/Delete';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};

const dateRegExp = /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/;
const dateTimeRegexp = /^[0-9]{4}-((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01])|(0[469]|11)-(0[1-9]|[12][0-9]|30)|(02)-(0[1-9]|[12][0-9]))T(0[0-9]|1[0-9]|2[0-3]):(0[0-9]|[1-5][0-9]):(0[0-9]|[1-5][0-9])\.[0-9]{3}Z$/;

export interface LimitationFormProps {
  limitation?: LimitationFormModel;
  submitted: (item: LimitationFormModel) => void;
  handleDelete: () => void;
}

export const LimitationForm = (props: LimitationFormProps) => {
  const typeItems = Object.keys(LimitationTypeEnum);
  const minMaxTypeItems = Object.keys(MinMaxTypeEnum);

  const getInitialData = (): LimitationFormModel => {
    return {
      type: (props.limitation && props.limitation.type) || '',
      parameter: (props.limitation && props.limitation.parameter) || '',
      block: (props.limitation && props.limitation.block) || false,
      minMaxType: props.limitation && props.limitation.minMaxType != undefined
        ? props.limitation.minMaxType
        : props.limitation && props.limitation.type === "min-max"
            ? MinMaxTypeEnum.Number
            : '',
      min:
        props.limitation && props.limitation.min != null
          ? props.limitation.min
          : '',
      max:
        props.limitation && props.limitation.max != null
          ? props.limitation.max
          : '',
      default:
        props.limitation && props.limitation.default != null
          ? props.limitation.default
          : '',
      valuesList: props.limitation
        ? props.limitation.valuesList.map(x => x)
        : []
    };
  };

  return (
    <div className="limitation-form">
      <Formik<LimitationFormModel>
        initialValues={getInitialData()}
        onSubmit={(
          values: LimitationFormModel,
          actions: FormikActions<LimitationFormModel>
        ) => {
          props.submitted(values);
          actions.resetForm(values);
        }}
        validationSchema={Yup.object().shape({
          type: Yup.string().required('Required'),
          parameter: Yup.string().required('Required'),
          block: Yup.boolean(),
          minMaxType: Yup.string().when('type', {
            is: LimitationTypeEnum.MinMax,
            then: Yup.string().required('Required'),
            otherwise: Yup.string().notRequired()
          }),
          min: Yup.mixed()
            .when('type', {
              is: LimitationTypeEnum.MinMax,
              then: Yup.mixed().required('Required'),
              otherwise: Yup.mixed().notRequired()
            })
            .when(['type', 'minMaxType'], {
              is: (type, minMaxType) => type === LimitationTypeEnum.MinMax && minMaxType === MinMaxTypeEnum.Number,
              then: Yup.number()
            })
            .when(['type', 'minMaxType'], {
              is: (type, minMaxType) => type === LimitationTypeEnum.MinMax && minMaxType === MinMaxTypeEnum.DateTime,
              then: Yup.string().matches(dateTimeRegexp, 'Invalid ISO date format. Must be YYYY-MM-DDTHH:MM:SS.sssZ')
            })
            .when(['type', 'minMaxType'], {
              is: (type, minMaxType) => type === LimitationTypeEnum.MinMax && minMaxType === MinMaxTypeEnum.Date,
              then: Yup.string().matches(dateRegExp, 'Invalid date format. Must be YYYY-MM-DD'),
            }),
          max: Yup.mixed()
            .when('type', {
              is: LimitationTypeEnum.MinMax,
              then: Yup.mixed().required('Required'),
              otherwise: Yup.mixed().notRequired()
            })
            .when(['type', 'minMaxType'], {
              is: (type, minMaxType) => type === LimitationTypeEnum.MinMax && minMaxType === MinMaxTypeEnum.Number,
              then: Yup.number()
            })
            .when(['type', 'minMaxType'], {
              is: (type, minMaxType) => type === LimitationTypeEnum.MinMax && minMaxType === MinMaxTypeEnum.DateTime,
              then: Yup.string().matches(dateTimeRegexp, 'Invalid ISO date format. Must be YYYY-MM-DDTHH:MM:SS.sssZ')
            })
            .when(['type', 'minMaxType'], {
              is: (type, minMaxType) => type === LimitationTypeEnum.MinMax && minMaxType === MinMaxTypeEnum.Date,
              then: Yup.string().matches(dateRegExp, 'Invalid date format. Must be YYYY-MM-DD'),
            })
            .test(
              'max-greater-than-min',
              'Max must be greater than min.',
              function (value) {
                const type = this.resolve(Yup.ref('type'));
                if (type === LimitationTypeEnum.MinMax) {
                  const min = this.resolve(Yup.ref('min'));
                  const minMaxType = this.resolve(Yup.ref('minMaxType'));
                  if (typeof value === 'number' && typeof min === 'number') {
                    return value > min;
                  } else if (minMaxType === MinMaxTypeEnum.Date) {
                    return moment(min, 'YYYY-MM-DD').isBefore(moment(value, 'YYYY-MM-DD'))
                  }  else if (minMaxType === MinMaxTypeEnum.DateTime) {
                    return moment(min).isBefore(moment(value))
                  } else {
                    return false;
                  }
                } else {
                  return true;
                }
              }
            ),
          default: Yup.mixed()
            .when('type', {
              is: LimitationTypeEnum.MinMax,
              then: Yup.mixed().required('Required'),
              otherwise: Yup.mixed().notRequired()
            })
            .when(['type', 'minMaxType'], {
              is: (type, minMaxType) => type === LimitationTypeEnum.MinMax && minMaxType === MinMaxTypeEnum.Number,
              then: Yup.number()
            })
            .when(['type', 'minMaxType'], {
              is: (type, minMaxType) => type === LimitationTypeEnum.MinMax && minMaxType === MinMaxTypeEnum.DateTime,
              then: Yup.string().matches(dateTimeRegexp, 'Invalid ISO date format. Must be YYYY-MM-DDTHH:MM:SS.sssZ')
            })
            .when(['type', 'minMaxType'], {
              is: (type, minMaxType) => type === LimitationTypeEnum.MinMax && minMaxType === MinMaxTypeEnum.Date,
              then: Yup.string().matches(dateRegExp, 'Invalid date format. Must be YYYY-MM-DD'),
            })
            .test(
              'default-between-min-max',
              'Default must be between min and max (included).',
              function (value) {
                const type = this.resolve(Yup.ref('type'));
                if (type === LimitationTypeEnum.MinMax) {
                  const min = this.resolve(Yup.ref('min'));
                  const max = this.resolve(Yup.ref('max'));
                  const minMaxType = this.resolve(Yup.ref('minMaxType'));
                  if (typeof value === 'number' && typeof min === 'number') {
                    return value >= min && value <= max;
                  } else if (minMaxType === MinMaxTypeEnum.Date) {
                    return moment(value, 'YYYY-MM-DD').isBetween(moment(min, 'YYYY-MM-DD'), moment(max, 'YYYY-MM-DD'), undefined, "[]")
                  }  else if (minMaxType === MinMaxTypeEnum.DateTime) {
                    return moment(value).isBetween(moment(min), moment(max), undefined, "[]")
                  } else {
                    return false;
                  }
                } else {
                  return true;
                }
              }
            ),
          valuesList: Yup.array().when('type', {
            is: LimitationTypeEnum.InArray,
            then: Yup.array().min(1, 'At least 1 value must be entered.')
          })
        })}
      >
        {formikProps => {
          const {
            values,
            dirty,
            touched,
            errors,
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue
          } = formikProps;
          return (
            <form onSubmit={handleSubmit} noValidate>
              <Grid container spacing={1}>
                <Grid item xs={6}>
                  <FormControl
                    fullWidth={true}
                    error={errors.type && touched.type}
                    margin="normal"
                    required
                  >
                    <InputLabel htmlFor="type">Type</InputLabel>
                    <Select
                      value={values.type}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      displayEmpty
                      inputProps={{
                        id: 'type',
                        name: 'type'
                      }}
                      MenuProps={MenuProps}
                    >
                      {typeItems.map(item => (
                        <MenuItem
                          key={LimitationTypeEnum[item]}
                          value={LimitationTypeEnum[item]}
                        >
                          {LimitationTypeEnum[item]}
                        </MenuItem>
                      ))}
                    </Select>
                    {errors.type && touched.type && (
                      <FormHelperText>{errors.type}</FormHelperText>
                    )}
                  </FormControl>
                </Grid>
                <Grid item xs={6}>
                  <Box display="block" textAlign="right">
                    <Tooltip title="Delete">
                      <IconButton
                        className="delete"
                        aria-label="Delete"
                        onClick={props.handleDelete}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </Tooltip>
                  </Box>
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    error={errors.parameter && touched.parameter}
                    helperText={
                      errors.parameter && touched.parameter && errors.parameter
                    }
                    type="name"
                    id="parameter"
                    label="Parameter"
                    margin="normal"
                    required
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.parameter}
                    className="text-item"
                  />
                </Grid>
                {values.type === LimitationTypeEnum.InArray && (
                  <Grid item xs={12}>
                    <ChipInput
                      fullWidth={true}
                      label="Values"
                      margin="normal"
                      required
                      error={!!(errors.valuesList && touched.valuesList)}
                      helperText={
                        errors.valuesList &&
                        touched.valuesList &&
                        errors.valuesList
                      }
                      value={values.valuesList}
                      blurBehavior="add"
                      onAdd={chip =>
                        setFieldValue('valuesList', [
                          ...values.valuesList,
                          chip
                        ])
                      }
                      onDelete={(chip, index) => {
                        values.valuesList.splice(index, 1);
                        setFieldValue('valuesList', values.valuesList);
                      }}
                      InputProps={{
                        id: 'valuesList',
                        name: 'valuesList'
                      }}
                      newChipKeyCodes={[13, 32, 188]}
                    />
                  </Grid>
                )}
                {values.type === LimitationTypeEnum.MinMax && (
                  <>
                    <Grid item xs={12}>
                      <FormControl
                        fullWidth={true}
                        error={errors.minMaxType && touched.minMaxType}
                        margin="normal"
                        required
                      >
                        <InputLabel htmlFor="minMaxType">Value type</InputLabel>
                        <Select
                          value={values.minMaxType}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          displayEmpty
                          inputProps={{
                            id: 'minMaxType',
                            name: 'minMaxType'
                          }}
                        >
                          {minMaxTypeItems.map(item => (
                            <MenuItem
                              key={MinMaxTypeEnum[item]}
                              value={MinMaxTypeEnum[item]}
                            >
                              {MinMaxTypeEnum[item]}
                            </MenuItem>
                          ))}
                        </Select>
                        {errors.minMaxType && touched.minMaxType && (
                          <FormHelperText>{errors.minMaxType}</FormHelperText>
                        )}
                      </FormControl>
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        error={errors.min && touched.min}
                        helperText={errors.min && touched.min && errors.min}
                        type="string"
                        id="min"
                        label="Min"
                        required
                        margin="normal"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.min}
                        className="text-item"
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        error={errors.max && touched.max}
                        helperText={errors.max && touched.max && errors.max}
                        type="string"
                        id="max"
                        label="Max"
                        required
                        margin="normal"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.max}
                        className="text-item"
                      />
                    </Grid>
                    <Grid item xs={6}>
                      <TextField
                        error={errors.default && touched.default}
                        helperText={
                          errors.default && touched.default && errors.default
                        }
                        type="string"
                        id="default"
                        label="Default"
                        margin="normal"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.default}
                        className="text-item"
                      />
                    </Grid>
                  </>
                )}
                {values.type === LimitationTypeEnum.MinMax && (
                  <Grid item xs={12}>
                    <FormControl
                      fullWidth={true}
                      margin="normal"
                      style={{ marginTop: '16px' }}
                    >
                      <FormLabel>Block</FormLabel>
                      <Switch
                        checked={values.block}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        inputProps={{
                          name: 'block',
                          id: 'block'
                        }}
                      />
                    </FormControl>
                  </Grid>
                )}

                <Grid item xs={12} style={{ marginTop: '32px' }}>
                  <div className="text-center">
                    <Button
                      color="primary"
                      variant="outlined"
                      type="submit"
                      disabled={!dirty}
                    >
                      {props.limitation ? 'Save limitation' : 'Add limitation'}
                    </Button>
                  </div>
                </Grid>
              </Grid>
            </form>
          );
        }}
      </Formik>
    </div>
  );
};

export default LimitationForm;
