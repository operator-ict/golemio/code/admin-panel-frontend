import React from 'react';

import './LimitationDetail.scss';
import { Grid } from '@material-ui/core';
import {
  LimitationModel,
  LimitationFormModel,
  LimitationTypeEnum
} from '../../../models';

/* tslint:disable:no-empty-interface */
export interface LimitationDetailProps {
  data: LimitationModel | LimitationFormModel;
}

export const LimitationDetail = (props: LimitationDetailProps) => {
  return props.data ? (
    <div className="limitation-detail">
      <Grid container>
        <Grid item xs={12}>
          <div>
            <h2>Type of limitation</h2>
            <div>{props.data.type}</div>
          </div>
        </Grid>
        <Grid item xs={12}>
          <div>
            <h2>Parameter</h2>
            <div>{props.data.parameter}</div>
          </div>
        </Grid>
        {props.data.type === LimitationTypeEnum.InArray &&
          props.data.valuesList != null &&
          props.data.valuesList.length > 0 && (
            <Grid item xs={12}>
              <div>
                <h2>Values</h2>
                <div>{props.data.valuesList.join(', ')}</div>
              </div>
            </Grid>
          )}
        {props.data.type === LimitationTypeEnum.MinMax && (
          <>
            {props.data.min != null && (
              <Grid item xs={6}>
                <div>
                  <h2>Min</h2>
                  <div>{props.data.min}</div>
                </div>
              </Grid>
            )}
            {props.data.max != null && (
              <Grid item xs={6}>
                <div>
                  <h2>Max</h2>
                  <div>{props.data.max}</div>
                </div>
              </Grid>
            )}
            {props.data.default != null && (
              <Grid item xs={6}>
                <div>
                  <h2>Default</h2>
                  <div>{props.data.default}</div>
                </div>
              </Grid>
            )}
          </>
        )}
        {(props.data.type === LimitationTypeEnum.InArray ||
          props.data.type === LimitationTypeEnum.MinMax) && (
          <Grid item xs={6}>
            <div>
              <h2>Block</h2>
              <div>{props.data.block ? 'Yes' : 'No'}</div>
            </div>
          </Grid>
        )}
      </Grid>
    </div>
  ) : null;
};

export default LimitationDetail;
