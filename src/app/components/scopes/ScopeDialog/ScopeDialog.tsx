import {
  CommonError,
  EnumItem,
  ComboBoxOptionModel
} from '@dataplatform/shared/common-ui';
import {
  CircularProgress,
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  Button
} from '@material-ui/core';
import { Formik, FormikProps } from 'formik';
import React, { useState, useEffect } from 'react';
import * as Yup from 'yup';
import {
  LimitationFormModel,
  LimitationTypeEnum,
  ScopeDetailModel,
  ScopeFormModel,
  ScopeSaveDataModel
} from '../../../models';
import { ScopeForm } from '../ScopeForm/ScopeForm';
import LimitationForm from '../LimitationForm/LimitationForm';
import './ScopeDialog.scss';

export interface ScopeDialogProps {
  isOpened: boolean;
  roleName: string;
  limitationTypes: EnumItem<LimitationTypeEnum>[];

  loading: boolean;
  loadError: boolean;
  data: ScopeDetailModel;
  handleSave: (data: ScopeSaveDataModel) => void;
  saving: boolean;
  saveError: boolean;
  saveErrorData: number;
  handleClose: () => void;
}

const ScopeDialog = (props: ScopeDialogProps) => {
  const [isAddNewVisible, setAddNewVisible] = useState<boolean>(false);
  const [limitations, setLimitations] = useState<LimitationFormModel[]>([]);

  useEffect(() => {
    setLimitations(
      props.data && props.data.limitations ? props.data.limitations : []
    );
  }, [props.data]);

  const submit = (values: ScopeFormModel) => {
    props.handleSave(getSaveData(values));
  };

  const handleClose = (): void => {
    props.handleClose();
  };

  const getInitialData = (): ScopeFormModel => {
    return {
      routeId: props.data ? props.data.routeId : '',
      route: props.data
        ? {
            value: props.data.routeId,
            label: `${props.data.method} ${props.data.endpoint}`
          }
        : null
    };
  };

  const getSaveData = (values: ScopeFormModel): ScopeSaveDataModel => {
    return {
      routeId: values.routeId !== '' ? values.routeId : null,
      limitations: limitations.map(x => {
        return {
          type: x.type !== '' ? x.type : null,
          parameter: x.parameter,
          block:
            x.type === LimitationTypeEnum.InArray ||
            x.type === LimitationTypeEnum.MinMax
              ? x.block
              : null,
          minMaxType:
            x.type === LimitationTypeEnum.MinMax && x.minMaxType !== '' ? x.minMaxType : null,
          min:
            x.type === LimitationTypeEnum.MinMax && x.min !== '' ? x.min : null,
          max:
            x.type === LimitationTypeEnum.MinMax && x.max !== '' ? x.max : null,
          default:
            x.type === LimitationTypeEnum.MinMax && x.default !== ''
              ? x.default
              : null,
          valuesList: x.type === LimitationTypeEnum.InArray ? x.valuesList : []
        };
      })
    };
  };

  const handleLimitationSave = (
    item: LimitationFormModel,
    currentLimitationIndex?: number
  ) => {
    if (currentLimitationIndex == null) {
      setLimitations([...limitations, item]);
      setAddNewVisible(false);
    } else {
      if (
        currentLimitationIndex > -1 &&
        currentLimitationIndex < limitations.length
      ) {
        const newValues = [
          ...limitations.slice(0, currentLimitationIndex),
          item,
          ...limitations.slice(currentLimitationIndex + 1, limitations.length)
        ];
        setLimitations(newValues);
      }
    }
  };

  const handleLimitationDelete = (index: number) => {
    if (index > -1 && index < limitations.length) {
      const newValues = [
        ...limitations.slice(0, index),
        ...limitations.slice(index + 1, limitations.length)
      ];
      setLimitations(newValues);
    }
  };

  const getErrorMessage = (): string => {
    if (props.saveError && props.saveErrorData != null) {
      if (props.saveErrorData === 409) {
        return 'This route was already added to the role.';
      }
    }
    return '';
  };

  return (
    <Dialog
      className="scope-dialog"
      open={props.isOpened}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
    >
      {!props.loading && props.isOpened && (
        <DialogTitle id="form-dialog-title">
          {props.data == null ? 'Create' : 'Edit'} Scope
        </DialogTitle>
      )}
      {props.isOpened && (
        <DialogContent>
          <div>
            <div
              className={
                props.loading || props.saving ? 'overlay visible' : 'overlay'
              }
            >
              <div className="center-vertical">
                <CircularProgress color="primary" />
              </div>
            </div>
          </div>
          <div style={{ visibility: props.loading ? 'hidden' : 'visible' }}>
            <Formik<ScopeFormModel>
              enableReinitialize={true}
              initialValues={getInitialData()}
              onSubmit={values => {
                submit(values);
              }}
              validationSchema={Yup.object().shape({
                routeId: Yup.string().required('Please select route')
              })}
            >
              {(formikProps: FormikProps<ScopeFormModel>) => {
                return (
                  <ScopeForm
                    {...formikProps}
                    roleName={props.roleName}
                    isNew={props.data == null}
                    handleClose={handleClose}
                    handleLimitationDelete={handleLimitationDelete}
                    loadError={props.loadError}
                    saving={props.saving}
                  />
                );
              }}
            </Formik>
            <Grid item xs={12}>
              <div className="header-limitations">Limitations</div>
              {limitations && limitations.length === 0 && (
                <div className="no-limitations text-center">
                  There are no limitations...
                </div>
              )}
              {limitations &&
                limitations.length > 0 &&
                limitations.map((limitation, index) => (
                  <LimitationForm
                    key={index.toString()}
                    limitation={limitation}
                    submitted={item => {
                      handleLimitationSave(item, index);
                    }}
                    handleDelete={() => handleLimitationDelete(index)}
                  />
                ))}
              {isAddNewVisible && (
                <LimitationForm
                  submitted={item => {
                    handleLimitationSave(item);
                  }}
                  handleDelete={() => setAddNewVisible(false)}
                />
              )}
              <div className="add-new text-center">
                <Button
                  variant="outlined"
                  color="primary"
                  onClick={() => setAddNewVisible(true)}
                  className="add-new-item"
                  disabled={isAddNewVisible}
                >
                  + Add new
                </Button>
              </div>
            </Grid>
          </div>

          {props.saveError && (
            <CommonError
              message="Data was not saved."
              subMessage={getErrorMessage()}
            />
          )}
        </DialogContent>
      )}
    </Dialog>
  );
};

export default ScopeDialog;
