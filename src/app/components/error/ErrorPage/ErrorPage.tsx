import React from 'react';
import { Paper, Grid, Button, Box } from '@material-ui/core';
import './ErrorPage.scss';
import { NavLink } from 'react-router-dom';
import { environment } from 'src/environments/environment';

/* tslint:disable:no-empty-interface */
export interface ErrorPageProps {}

export const ErrorPage = (props: ErrorPageProps) => {
  return (
    <div className="error-page">
      <Grid container justify="center" className="error-container">
        <Grid item>
          <Paper className="panel">
            <h3>We are sorry, mistakes were made. :-(</h3>
            <Box display="flex" justifyContent="center">
              <NavLink to="/login">
                <Button variant="contained" color="primary">
                  Back to login.
                </Button>
              </NavLink>
            </Box>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default ErrorPage;
