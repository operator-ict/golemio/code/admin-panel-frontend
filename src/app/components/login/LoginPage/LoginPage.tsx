import { Button, Grid, Link, TextField } from "@material-ui/core";
import { Formik } from "formik";
import { observer } from "mobx-react";
import React from "react";
import { Redirect } from "react-router";
import { Link as RouterLink } from "react-router-dom";
import * as Yup from "yup";
import { LoginModel } from "../../../models";
import { authStore } from "../../../store";
import LoginPanel from "../LoginPanel/LoginPanel";
import "./LoginPage.scss";

export const LoginPage = observer(() => {
    const formikRef = React.createRef<Formik<LoginModel>>();

    const signIn = async (values: LoginModel) => {
        await authStore.signIn(values);
    };

    return (
        <LoginPanel>
            <Grid container>
                <Grid item xs={12}>
                    <h2>Please, log in...</h2>
                </Grid>
                <Grid item xs={12}>
                    <span className={authStore.status === "error" ? "error-message" : "error-message visibility-hidden"}>
                        The combination of email and password is incorrect.
                    </span>
                </Grid>
            </Grid>
            <Formik<LoginModel>
                ref={formikRef}
                initialValues={{ email: "", password: "" }}
                onSubmit={(values: LoginModel) => {
                    signIn({
                        email: values.email,
                        password: values.password,
                        confirmation_password: values.confirmation_password,
                    });
                }}
                validationSchema={Yup.object().shape({
                    email: Yup.string()
                        .email("Email must be a valid email.")
                        .required("Required"),
                    password: Yup.string().required("Required"),
                    confirmation_password: Yup.string()
                        .trim()
                        .matches(/^$/, {
                            message: "Validation failed.",
                        }),
                })}
            >
                {props => {
                    const { values, touched, errors, handleChange, handleBlur, handleSubmit } = props;
                    return (
                        <form
                            onSubmit={event => {
                                handleSubmit(event);
                            }}
                            noValidate
                        >
                            <Grid container>
                                <Grid item xs={12}>
                                    <TextField
                                        error={errors.email && touched.email}
                                        type="email"
                                        id="email"
                                        label="Email"
                                        margin="normal"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.email}
                                        className="text-item helper-input"
                                        helperText={errors.email && touched.email && errors.email}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        error={errors.password && touched.password}
                                        type="password"
                                        id="password"
                                        label="Password"
                                        margin="normal"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.password}
                                        className="text-item helper-input"
                                        helperText={errors.password && touched.password && errors.password}
                                    />
                                    <TextField
                                        type="text"
                                        id="confirmation_password"
                                        margin="normal"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.confirmation_password}
                                        className="hfield"
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <Button type="submit" variant="contained" color="primary" className="submit-btn">
                                        Log in
                                    </Button>
                                </Grid>
                                <Grid item xs={12} className="links-container">
                                    <Link to="/forgotten-password" component={RouterLink} className="forgotten-password-link">
                                        Forgotten Password
                                    </Link>
                                </Grid>
                            </Grid>
                        </form>
                    );
                }}
            </Formik>
            {authStore.status === "success" && <Redirect to="/" />}
        </LoginPanel>
    );
});

export default LoginPage;
