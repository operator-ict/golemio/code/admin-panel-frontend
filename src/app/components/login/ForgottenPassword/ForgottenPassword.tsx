import { Button, Grid, Link, TextField } from "@material-ui/core";
import { Formik, FormikProps } from "formik";
import { observer } from "mobx-react";
import React, { useEffect } from "react";
import { Redirect } from "react-router";
import { Link as RouterLink } from "react-router-dom";
import * as Yup from "yup";
import { ForgottenPasswordModel } from "../../../models";
import { authStore } from "../../../store";
import LoginPanel from "../LoginPanel/LoginPanel";
import "./ForgottenPassword.scss";

export const ForgottenPassword = observer(() => {
    const formikRef = React.createRef<Formik<ForgottenPasswordModel>>();

    useEffect(() => {
        authStore.forgottenPasswordInit();
    }, []);

    const forgottenPasswordRequest = (data: ForgottenPasswordModel) => {
        authStore.forgottenPasswordRequest(data.email);
    };

    return (
        <LoginPanel>
            <Grid container>
                <Grid item xs={12}>
                    <h2 className="forgotten-password-header">Forgotten Password</h2>
                </Grid>
                <Grid item xs={12}>
                    <span
                        className={
                            !authStore.forgottenPasswordSaved && authStore.forgottenPasswordError
                                ? "error-message"
                                : "error-message visibility-hidden"
                        }
                    >
                        {authStore.forgottenPasswordErrorType === "not_found" && "This email address is not registered."}
                        {authStore.forgottenPasswordErrorType !== "not_found" && "Password could not be recovered."}
                    </span>
                </Grid>
            </Grid>
            {authStore.forgottenPasswordSaved ? (
                <>
                    <span className="success-message">Email with further instructions was successfully sent to you.</span>
                </>
            ) : (
                <Formik<ForgottenPasswordModel>
                    ref={formikRef}
                    enableReinitialize={true}
                    initialValues={{
                        email: "",
                        confirmation_password: "",
                    }}
                    onSubmit={forgottenPasswordRequest}
                    validationSchema={Yup.object().shape({
                        confirmation_password: Yup.string()
                            .trim()
                            .matches(/^$/, {
                                message: "Validation failed.",
                            }),
                        email: Yup.string()
                            .email("Email must be a valid email.")
                            .required("Required"),
                    })}
                >
                    {(formikProps: FormikProps<ForgottenPasswordModel>) => {
                        const { values, touched, errors, isSubmitting, handleChange, handleBlur, handleSubmit } = formikProps;

                        return (
                            <form
                                onSubmit={event => {
                                    handleSubmit(event);
                                }}
                                noValidate
                            >
                                <Grid container>
                                    <Grid item xs={12}>
                                        <TextField
                                            error={errors.email && touched.email}
                                            type="email"
                                            id="email"
                                            label="Email"
                                            margin="normal"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.email}
                                            className="text-item helper-input"
                                            helperText={errors.email && touched.email && errors.email}
                                        />
                                        <TextField
                                            type="name"
                                            id="confirmation_password"
                                            margin="normal"
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            value={values.confirmation_password}
                                            className="hfield"
                                        />
                                    </Grid>

                                    <Grid item xs={12}>
                                        <Button
                                            type="submit"
                                            disabled={isSubmitting && authStore.forgottenPasswordSaving}
                                            variant="contained"
                                            color="primary"
                                            className="submit-btn"
                                        >
                                            Send Request
                                        </Button>
                                    </Grid>
                                </Grid>
                            </form>
                        );
                    }}
                </Formik>
            )}
            <Grid item xs={12} className="links-container">
                <Link to="/login" component={RouterLink}>
                    Back to Login
                </Link>
            </Grid>
            {authStore.status === "success" && <Redirect to="/" />}
        </LoginPanel>
    );
});

export default ForgottenPassword;
