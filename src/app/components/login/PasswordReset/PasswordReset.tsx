import { Button, Grid, Link, TextField, CircularProgress } from '@material-ui/core';
import { Formik, FormikProps } from 'formik';
import { observer } from 'mobx-react';
import React, { useEffect } from 'react';
import { Redirect, RouteComponentProps } from 'react-router';
import { Link as RouterLink } from 'react-router-dom';
import * as Yup from 'yup';
import { PasswordResetFormModel, PasswordResetModel } from '../../../models';
import { authStore } from '../../../store';
import LoginPanel from '../LoginPanel/LoginPanel';
import './PasswordReset.scss';

export const PasswordReset = observer((props: RouteComponentProps) => {
  const formikRef = React.createRef<Formik<PasswordResetFormModel>>();

  const code = props.match.params['code'];

  useEffect(() => {
    authStore.passwordResetInit();
    authStore.passwordResetCodeCheck(code);
  }, []);


  const passwordReset = (data: PasswordResetFormModel) => {
    const model: PasswordResetModel = {
      newPassword: data.newPassword
    }
    authStore.passwordReset(code, model);
  }

  return (
    <LoginPanel>
      <Grid container>
        <Grid item xs={12}>
          <h2 className="forgotten-password-header">Password Reset</h2>
        </Grid>
        <Grid item xs={12}>
          {!authStore.passwordResetCodeCheckLoading && <>
            <span className={authStore.passwordResetCodeCheckError || (!authStore.passwordResetSaved && authStore.passwordResetError) ? "error-message" : "error-message visibility-hidden"}>
              {((authStore.passwordResetCodeCheckErrorType === "not_found") || (authStore.passwordResetCodeCheckErrorType === "other") || (authStore.passwordResetErrorType === "not_found")) && <>
                The link is not valid. Please perform request for <Link to="/forgotten-password" component={RouterLink}>forgotten password</Link>.
                </>}
              {((authStore.passwordResetCodeCheckErrorType === "expired") || (authStore.passwordResetErrorType === "expired")) && <>
                The link is not valid anymore. Please repeat request for <Link to="/forgotten-password" component={RouterLink}>forgotten password</Link>.
                </>}
              {!authStore.passwordResetSaved && authStore.passwordResetError && <>
                {authStore.passwordResetErrorType === "other" && "Password could not be reset."}
              </>}
            </span>
          </>}
        </Grid>
      </Grid>

      {authStore.passwordResetSaved ? <>
        <span className="success-message">
          Password was succesfully reset.
        </span>
      </> :
        <Formik<PasswordResetFormModel>
          ref={formikRef}
          enableReinitialize={true}
          initialValues={{
            newPassword: '', newPasswordConfirmation: ''
          }}
          onSubmit={passwordReset}
          validationSchema={Yup.object().shape({
            newPassword: Yup.string().required('Required').min(6, 'Minimum length for password is 6 characters'),
            newPasswordConfirmation: Yup.string()
              .oneOf([Yup.ref('newPassword')], 'Passwords must match')
              .required('Required'),
          })}
        >
          {(formikProps: FormikProps<PasswordResetFormModel>) => {
            const {
              values,
              touched,
              errors,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
            } = formikProps;

            return (
              <form onSubmit={(event) => {
                handleSubmit(event);
              }} noValidate>
                <Grid container style={{ position: 'relative' }}>
                  {authStore.passwordResetCodeCheckLoading && <>
                    <div className="overlay visible">
                      <div className="center-vertical">
                        <CircularProgress color="primary" />
                        {/* Checking the link... */}
                      </div>
                    </div>
                  </>}
                  <Grid item xs={12}>
                    <TextField
                      error={errors.newPassword && touched.newPassword}
                      type="password"
                      id="newPassword"
                      label="New Password"
                      disabled={authStore.passwordResetCodeCheckLoading || authStore.passwordResetCodeCheckError}
                      margin="normal"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.newPassword}
                      className="text-item helper-input"
                      helperText={(errors.newPassword && touched.newPassword) && errors.newPassword}
                    />
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      error={errors.newPasswordConfirmation && touched.newPasswordConfirmation}
                      type="password"
                      id="newPasswordConfirmation"
                      label="Confirm New Password"
                      disabled={authStore.passwordResetCodeCheckLoading || authStore.passwordResetCodeCheckError}
                      margin="normal"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.newPasswordConfirmation}
                      className="text-item helper-input"
                      helperText={(errors.newPasswordConfirmation && touched.newPasswordConfirmation) && errors.newPasswordConfirmation}
                    />
                  </Grid>

                  <Grid item xs={12}>
                    <Button type="submit" disabled={authStore.passwordResetCodeCheckLoading || authStore.passwordResetCodeCheckError || (isSubmitting && authStore.passwordResetSaving)} variant="contained" color="primary" className="submit-btn">Send Request</Button>
                  </Grid>
                </Grid>
              </form>
            );
          }}
        </Formik>}

      <Grid item xs={12} className="links-container">
        <Link to="/login" component={RouterLink}>To Login Page</Link>
      </Grid>

    </LoginPanel>
  );
});

export default PasswordReset;
