import { observer } from 'mobx-react';
import React, { useContext, useEffect } from 'react';
import Moment from 'react-moment';
import { BrowserRouter, Redirect, Route, Switch, RouteProps } from 'react-router-dom';
import 'react-virtualized/styles.css';
import './LoginPanel.scss';
import { Grid, Paper } from '@material-ui/core';
import { authStore } from '../../../store';
import { environment } from '../../../../environments/environment';

// Set the locale for every react-moment instance to Czech.
Moment.globalLocale = 'cs';


export const LoginPanel = observer((props: RouteProps) => {

  useEffect(() => {
    authStore.fetchApiVersion();
  }, []);

  return (
    <div>
      <Grid container justify="center" className="login-container">
        <Grid item>
          <div className="login-h">
            <div>Golemio API</div>
            <h1>Admin Panel</h1>
          </div>
          <Paper className="panel">
            {props.children}
          </Paper>
          <Grid item className="versions">
            {environment.version} / {authStore.apiVersion}
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
});

export default LoginPanel;
