import { EnhancedTable, EnhancedTableColumn, EnhancedTableQuery, ConfirmDialog } from '@dataplatform/shared/common-ui';
import { observer } from 'mobx-react';
import React, { useContext, useState } from 'react';
import { RouteListItemModel, RouteSaveDataModel } from '../../../models';
import { routesStore } from '../../../store';
import RouteDialog from '../RouteDialog/RouteDialog';
import './RoutesPage.scss';
import RouteMethod from '../RouteMethod/RouteMethod';

const RoutesPage = observer(() => {

  const [showConfirm, setShowConfirm] = useState<boolean>(false);
  const [removeId, setRemoveId] = useState<number>(null);

  const columns: EnhancedTableColumn<RouteListItemModel>[] = [
    {
      field: 'method',
      label: 'METHOD',
      render: (x: RouteListItemModel) => <RouteMethod method={x.method}></RouteMethod>
    },
    {
      field: 'endpoint',
      label: 'ENDPOINT',
      render: (x: RouteListItemModel) => <span className="fw-bold">{x.endpoint}</span>
    },
    {
      field: 'targetUrl',
      label: 'TARGET'
    },
    {
      field: 'targetPath',
      label: 'TARGET PATH'
    },
    {
      field: 'isPublic',
      label: 'PUBLIC',
      dataType: 'boolean'
    },
    {
      field: 'isRedirect',
      label: 'REDIRECT',
      dataType: 'boolean'
    }
  ];

  const idAccessor = (row: RouteListItemModel): number => {
    return row.id;
  };

  const dataStateChange = (query: EnhancedTableQuery<RouteListItemModel>): void => {
    routesStore.loadList(query);
  };

  const handleOpen = (rowData: RouteListItemModel) => {
    routesStore.openDetail(rowData ? rowData.id : null);
  };

  const handleClose = () => {
    routesStore.closeDetail();
  };

  const handleSave = (data: RouteSaveDataModel): void => {
    routesStore.saveDetail(data, () => {
      routesStore.refreshList();
    });
  };

  const handleRemove = (data: RouteListItemModel) => {
    setRemoveId(data.id);
    setShowConfirm(true);
  };

  const handleRemoveConfirm = () => {
    setRemoveId(null);
    setShowConfirm(false);

    routesStore.delete(removeId, () => {
      routesStore.refreshList();
    });
  };

  return (
    <div>
      <div className="headers"><h2>Routes</h2><div className="subheader">List of all routes</div></div>
      <RouteDialog
        isOpened={routesStore.dialogState === 'open'}
        targets={routesStore.targets}
        loading={routesStore.detail.loading}
        loadError={routesStore.detail.loadError}
        data={routesStore.detail.data}
        handleSave={handleSave}
        saving={routesStore.detail.saving}
        saveError={routesStore.detail.saveError}
        handleClose={handleClose}
      />
      <ConfirmDialog
        title="Remove Route"
        message="Do you really want to remove this route?"
        open={showConfirm}
        handleCancel={() => setShowConfirm(false)}
        handleOk={() => handleRemoveConfirm()}
      />
      <EnhancedTable
        loading={routesStore.list.loading}
        loadError={routesStore.list.loadError}
        data={routesStore.list.data}
        columns={columns}
        dataStateChange={dataStateChange}
        idAccessor={idAccessor}
        selectable={false}
        sortable={true}
        order={routesStore.list.query.orderDirection}
        orderBy={routesStore.list.query.orderBy}
        pageable={true}
        page={routesStore.list.query.page}
        pageSize={routesStore.list.query.pageSize}
        searchable={true}
        searchText={routesStore.list.query.search}
        actions={[
          {
            icon: null,
            text: '+ Add new',
            isFreeAction: true,
            onClick: () => handleOpen(null)
          },
          {
            icon: 'edit',
            tooltip: 'Edit Route',
            isFreeAction: false,
            onClick: (event, rowData) => handleOpen(rowData)
          },
          {
            icon: 'delete',
            tooltip: 'Remove',
            isFreeAction: false,
            onClick: (event, rowData) => handleRemove(rowData)
          },
        ]}
      />
    </div>
  );
});

export default RoutesPage;
