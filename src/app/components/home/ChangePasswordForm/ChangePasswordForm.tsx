import { Button, Grid, TextField } from '@material-ui/core';
import { FormikProps } from 'formik';
import React from 'react';
import './ChangePasswordForm.scss';
import { ChangePasswordFormModel } from '../../../models';

const ChangePasswordForm = (
  props: FormikProps<ChangePasswordFormModel> & {
    handleClose: () => void;
    saving: boolean;
  }
) => {

  const {
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    handleClose,
    saving
  } = props;

  return (
    <form onSubmit={handleSubmit} noValidate>
      <Grid container>
        <Grid item xs={12}>
          <TextField
            error={errors.oldPassword && touched.oldPassword}
            type="password"
            id="oldPassword"
            label="Current Password"
            margin="normal"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.oldPassword}
            className="text-item helper-input"
            helperText={(errors.oldPassword && touched.oldPassword) && errors.oldPassword}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            error={errors.newPassword && touched.newPassword}
            type="password"
            id="newPassword"
            label="New Password"
            margin="normal"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.newPassword}
            className="text-item helper-input"
            helperText={(errors.newPassword && touched.newPassword) && errors.newPassword}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            error={errors.newPasswordConfirmation && touched.newPasswordConfirmation}
            type="password"
            id="newPasswordConfirmation"
            label="Confirm New Password"
            margin="normal"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.newPasswordConfirmation}
            className="text-item helper-input"
            helperText={(errors.newPasswordConfirmation && touched.newPasswordConfirmation) && errors.newPasswordConfirmation}
          />
        </Grid>

        <Grid item xs={12} className="buttons absolute-dialog-btns">
          <Button onClick={handleClose} color="primary">
            Cancel
        </Button>
          <Button
            type="submit"
            disabled={isSubmitting && saving}
            color="primary"
            autoFocus
          >
            Save
            </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default ChangePasswordForm;
