import { Container, Grid, Tabs, Tab, Paper, Button, Box, IconButton, Icon, Tooltip } from '@material-ui/core';
import React, { useEffect, useContext } from 'react';
import {
  BrowserRouter,
  Route,
  RouteComponentProps,
  Switch,
  Link
} from 'react-router-dom';
import './HomePage.scss';
import { ScopesPage } from '../../scopes';
import UsersPage from '../UsersPage/UsersPage';
import RoutesPage from '../RoutesPage/RoutesPage';
import RolesPage from '../RolesPage/RolesPage';
import { RouteEnum, ChangePasswordModel } from '../../../models';
import { authService } from '../../../services';
import { homeStore } from '../../../store';
import { observer } from 'mobx-react';
import ChangePasswordDialog from '../ChangePasswordDialog/ChangePasswordDialog';
import { NotFound } from '@dataplatform/shared/common-ui';


export const HomePage = observer((props: RouteComponentProps) => {

  const usersUrl = '/';
  const rolesUrl = '/roles';
  const routesUrl = '/routes';
  const scopesUrl = '/scopes';
  const scopesDetailUrl = '/roles/:id/scopes';

  useEffect(() => {
    homeStore.setTab(currentRoute());
  }, []);

  const currentRoute = (): RouteEnum => {
    const pathName = props.location.pathname;
    if (pathName === usersUrl) {
      return RouteEnum.USERS;
    }
    if (pathName === rolesUrl || pathName.indexOf('scopes') > -1) {
      return RouteEnum.ROLES;
    }
    if (pathName === routesUrl) {
      return RouteEnum.ROUTES;
    }
    return RouteEnum.USERS;
  };

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    homeStore.setTab(newValue);
  }

  const openChangePasswordDialog = () => {
    homeStore.toggleChangePasswordDialog(true);
  }

  const closeChangePasswordDialog = () => {
    homeStore.toggleChangePasswordDialog(false);
  }

  const changePassword = (data: ChangePasswordModel) => {
    homeStore.changePassword(data);
  }

  return (
    <div>
      <ChangePasswordDialog
        isOpened={homeStore.changePasswordDialogState === 'open'}
        handleSave={changePassword}
        saving={homeStore.changePasswordSaving}
        saveError={homeStore.changePasswordError}
        saveErrorType={homeStore.changePasswordErrorType}
        handleClose={closeChangePasswordDialog}
      />
      <Container maxWidth="lg" className="page-container">
        <div className="header">
          <Grid container spacing={1}>
            <Grid item xs={12}>
              <Paper>
                <Grid container>
                  <Grid item xs={6}>
                    <h1 className="home-h">Golemio API - Admin Panel</h1>
                    <div className="home-h"><a href="https://gbi-pm.golem.oictinternal.cz/" color="red">Uživatelé Golemio BI se spravují na gbi-pm.golem.oictinternal.cz!</a></div>
                  </Grid>
                  <Grid item xs={6}>
                    <Tooltip title="Change Password">
                      <IconButton
                        className="settings"
                        size="medium"
                        color="inherit"
                        onClick={() => openChangePasswordDialog()}
                      >
                        <Icon fontSize="small">
                          settings
                      </Icon>
                      </IconButton>
                    </Tooltip>
                    <Button variant="text" className="logout" onClick={() => authService.signOut()}>Logout</Button>
                  </Grid>
                  <Grid item xs={12}>
                    <Tabs
                      value={homeStore.tab}
                      onChange={handleChange}
                      indicatorColor="primary"
                      textColor="primary"
                      centered
                    >
                      <Tab label="Users" component={Link} to={usersUrl} />
                      <Tab label="Roles" component={Link} to={rolesUrl} />
                      <Tab label="Routes" component={Link} to={routesUrl} />
                    </Tabs>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          </Grid>
        </div>
        <div className="page-content">
          <Switch>
            <Route exact path={usersUrl} component={UsersPage} />
            <Route path={scopesDetailUrl} component={ScopesPage} />
            <Route path={rolesUrl} component={RolesPage} />
            <Route path={routesUrl} component={RoutesPage} />
            <Route path="*" component={NotFound} />
          </Switch>
        </div>
      </Container>
    </div>
  );
});

export default HomePage;
