import { CommonError, EnumItem } from '@dataplatform/shared/common-ui';
import {
  CircularProgress,
  Dialog,
  DialogContent,
  DialogTitle
} from '@material-ui/core';
import { styled } from '@material-ui/styles';
import { Formik, FormikProps } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import {
  UserDetailModel,
  UserFormModel,
  UserSaveDataModel
} from '../../../models';
import UserForm from '../UserForm/UserForm';
import './UserDialog.scss';

export interface UserDialogProps {
  isOpened: boolean;
  roleItems: EnumItem[];
  refererItems: EnumItem[];

  loading: boolean;
  loadError: boolean;
  data: UserDetailModel;
  handleSave: (data: UserSaveDataModel) => void;
  saving: boolean;
  saveError: boolean;
  handleClose: () => void;
}

const UserDialog = (props: UserDialogProps) => {
  const submit = (values: UserFormModel) => {
    props.handleSave(getSaveData(values));
  };

  const getInitialData = (): UserFormModel => {
    return {
      firstName: (props.data && props.data.firstName) || '',
      lastName: (props.data && props.data.lastName) || '',
      email: (props.data && props.data.email) || '',
      referer:
        props.data && props.data.referer != null ? props.data.referer : '',
      roles: (props.data && props.data.roles) || [],
      rateLimit:
        props.data && props.data.rateLimit != null
          ? props.data.rateLimit
          : false,
      count: props.data && props.data.count != null ? props.data.count : '',
      timeWindow:
        props.data && props.data.timeWindow != null ? props.data.timeWindow : ''
    };
  };

  const getSaveData = (values: UserFormModel): UserSaveDataModel => {
    return {
      firstName: values.firstName,
      lastName: values.lastName,
      email: values.email,
      referer: values.referer !== '' ? values.referer : null,
      roles: values.roles,
      rateLimit: values.rateLimit,
      count: values.count !== '' ? values.count : null,
      timeWindow: values.timeWindow !== '' ? values.timeWindow : null
    };
  };

  return (
    <Dialog
      className="user-dialog"
      open={props.isOpened}
      onClose={props.handleClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle style={{ visibility: !props.loading && props.isOpened ? 'visible' : 'hidden' }}
        id="form-dialog-title">{props.data == null ? "Create" : "Edit"} User</DialogTitle>
      <DialogContent>
        <div>
          <div className={props.loading || props.saving ? "overlay visible" : "overlay"}>
            <div className="center-vertical">
              <CircularProgress color="primary" />
            </div>
          </div>
        </div>
        <div style={{ visibility: props.loading && props.isOpened ? 'hidden' : 'visible' }}>
          {props.saveError ? (
            <CommonError message="Data was not saved." />
          ) : null}

          <Formik<UserFormModel>
            enableReinitialize={true}
            initialValues={getInitialData()}
            onSubmit={values => {
              submit(values);
            }}
            validationSchema={Yup.object().shape({
              firstName: Yup.string().required('Required'),
              lastName: Yup.string().required('Required'),
              email: Yup.string()
                .email('Email must be a valid email.')
                .required('Required'),
              referer: props.data == null ? Yup.number()
                .required('Required') : undefined,
              count: Yup.number().when('rateLimit', {
                is: true,
                then: Yup.number().required('Required').min(0, 'Min. value is 0'),
                otherwise: Yup.number().notRequired()
              }),
              timeWindow: Yup.number().when('rateLimit', {
                is: true,
                then: Yup.number().required('Required').min(0, 'Min. value is 0'),
                otherwise: Yup.number().notRequired()
              }),
            })}
          >
            {(formikProps: FormikProps<UserFormModel>) => {
              return (
                <UserForm
                  {...formikProps}
                  isNew={props.data == null}
                  roleItems={props.roleItems}
                  refererItems={props.refererItems}
                  handleClose={props.handleClose}
                  loadError={props.loadError}
                  saving={props.saving}
                />
              );
            }}
          </Formik>
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default UserDialog;
