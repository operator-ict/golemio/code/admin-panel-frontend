import { CommonError } from '@dataplatform/shared/common-ui';
import {
  CircularProgress,
  Dialog,
  DialogContent,
  DialogTitle
} from '@material-ui/core';
import { Formik, FormikProps } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import {
  ChangePasswordModel,
  ChangePasswordFormModel
} from '../../../models';
import './ChangePasswordDialog.scss';
import ChangePasswordForm from '../ChangePasswordForm/ChangePasswordForm';

export interface ChangePasswordDialogProps {
  isOpened: boolean;

  handleSave: (data: ChangePasswordModel) => void;
  saving: boolean;
  saveError: boolean;
  saveErrorType: 'invalid_password' | 'other';
  handleClose: () => void;
}

const ChangePasswordDialog = (props: ChangePasswordDialogProps) => {
  const submit = (values: ChangePasswordFormModel) => {
    props.handleSave(getSaveData(values));
  };

  const getSaveData = (values: ChangePasswordFormModel): ChangePasswordModel => {
    return {
      oldPassword: values.oldPassword,
      newPassword: values.newPassword
    };
  };

  return (
    <Dialog
      className="user-dialog"
      open={props.isOpened}
      onClose={props.handleClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Change Password</DialogTitle>
      <DialogContent>
        <div>
          <div className={props.saving ? "overlay visible" : "overlay"}>
            <div className="center-vertical">
              <CircularProgress color="primary" />
            </div>
          </div>
        </div>
        <div>


          <Formik<ChangePasswordFormModel>
            enableReinitialize={true}
            initialValues={{
              oldPassword: '', newPassword: '', newPasswordConfirmation: ''
            }}
            onSubmit={values => {
              submit(values);
            }}
            validationSchema={Yup.object().shape({
              oldPassword: Yup.string().required('Required'),
              newPassword: Yup.string().required('Required').min(6, 'Minimum length for password is 6 characters'),
              newPasswordConfirmation: Yup.string()
                .oneOf([Yup.ref('newPassword')], 'Passwords must match')
                .required('Required'),
            })}
          >
            {(formikProps: FormikProps<ChangePasswordFormModel>) => {
              return (
                <ChangePasswordForm
                  {...formikProps}
                  handleClose={props.handleClose}
                  saving={props.saving}
                />
              );
            }}
          </Formik>

          {props.saveError ? (
            <CommonError message={props.saveErrorType === 'invalid_password' ? "Current password is invalid." : "Password could not be saved."} />
          ) : null}
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default ChangePasswordDialog;
