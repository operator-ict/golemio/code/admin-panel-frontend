import {
  CommonUiUtils,
  EnhancedTable,
  EnhancedTableColumn,
  EnhancedTableQuery,
  ConfirmDialog
} from '@dataplatform/shared/common-ui';
import { Tooltip } from '@material-ui/core';
import { observer } from 'mobx-react';
import React, { useContext, useState } from 'react';
import { UserListItemModel, UserSaveDataModel } from '../../../models';
import { referersService, rolesService, usersService } from '../../../services';
import { usersStore } from '../../../store/UsersStore';
import UserDialog from '../UserDialog/UserDialog';
import { rolesStore, referersStore } from '../../../store';

const UsersPage = observer(() => {
  const [showConfirm, setShowConfirm] = useState<boolean>(false);
  const [removeId, setRemoveId] = useState<number>(null);

  const columns: EnhancedTableColumn<UserListItemModel>[] = [
    {
      field: 'firstName',
      label: 'NAME'
    },
    {
      field: 'lastName',
      label: 'SURNAME'
    },
    {
      field: 'email',
      label: 'EMAIL'
    },
    {
      field: 'roles',
      label: 'ROLE',
      sortable: false,
      render: rowData => {
        return rowData.roles.length > 0 ? (
          <Tooltip
            title={
              rowData.roles.length >= 1
                ? CommonUiUtils.getEnumNames(rolesStore.enums, rowData.roles)
                : ''
            }
          >
            <div>
              {rowData.roles.length >= 1 ? (
                <span>{rowData.roles.length}</span>
              ) : (
                ''
              )}
            </div>
          </Tooltip>
        ) : null;
      }
    },
    {
      field: 'created',
      label: 'CREATED',
      dataType: 'datetime'
    },
    {
      field: 'activated',
      label: 'ACTIVATED',
      dataType: 'datetime'
    },
    {
      field: 'last_activity',
      label: 'LAST ACTIVITY',
      dataType: 'datetime'
    }
  ];
  const idAccessor = (row: UserListItemModel): number => {
    return row.id;
  };

  const dataStateChange = (query: EnhancedTableQuery<UserListItemModel>) => {
    usersStore.loadList(query);
  };

  const handleOpen = (rowData: UserListItemModel) => {
    usersStore.openDetail(rowData ? rowData.id : null);
  };

  const handleClose = () => {
    usersStore.closeDetail();
  };

  const handleSave = (data: UserSaveDataModel) => {
    usersStore.saveDetail(data, () => {
      usersStore.refreshList();
    });
  };

  const handleRemove = (data: UserListItemModel) => {
    setRemoveId(data.id);
    setShowConfirm(true);
  };

  const handleRemoveConfirm = () => {
    setRemoveId(null);
    setShowConfirm(false);

    usersStore.delete(removeId, () => {
      usersStore.refreshList();
    });
  };

  return (
    <div>
      <div className="headers">
        <h2>Users</h2>
        <div className="subheader">List of all users</div>
      </div>
      <UserDialog
        isOpened={usersStore.dialogState === 'open'}
        roleItems={rolesStore.enums}
        refererItems={referersStore.enums}
        loading={usersStore.detail.loading}
        loadError={usersStore.detail.loadError}
        data={usersStore.detail.data}
        handleSave={handleSave}
        saving={usersStore.detail.saving}
        saveError={usersStore.detail.saveError}
        handleClose={handleClose}
      />
      <ConfirmDialog
        title="Remove User"
        message="Do you really want to remove this user?"
        open={showConfirm}
        handleCancel={() => setShowConfirm(false)}
        handleOk={() => handleRemoveConfirm()}
      />
      <EnhancedTable
        loading={usersStore.list.loading}
        loadError={usersStore.list.loadError}
        data={usersStore.list.data}
        columns={columns}
        dataStateChange={dataStateChange}
        idAccessor={idAccessor}
        selectable={false}
        sortable={true}
        order={usersStore.list.query.orderDirection}
        orderBy={usersStore.list.query.orderBy}
        pageable={true}
        page={usersStore.list.query.page}
        pageSize={usersStore.list.query.pageSize}
        searchable={true}
        searchText={usersStore.list.query.search}
        actions={[
          {
            icon: null,
            text: '+ Add new',
            isFreeAction: true,
            onClick: () => handleOpen(null)
          },
          {
            icon: 'edit',
            tooltip: 'Edit User',
            isFreeAction: false,
            onClick: (event, rowData) => handleOpen(rowData)
          },
          {
            icon: 'delete',
            tooltip: 'Remove',
            isFreeAction: false,
            onClick: (event, rowData) => handleRemove(rowData)
          }
        ]}
      />
    </div>
  );
});

export default UsersPage;
