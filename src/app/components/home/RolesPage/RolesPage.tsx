import {
  EnhancedTable,
  EnhancedTableColumn,
  EnhancedTableQuery,
  ConfirmDialog
} from '@dataplatform/shared/common-ui';
import { observer } from 'mobx-react';
import React, { useContext, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { RoleListItemModel, RoleSaveDataModel } from '../../../models';
import { rolesStore, scopesStore } from '../../../store';
import RoleDialog from '../RoleDialog/RoleDialog';
import './RolesPage.scss';
import { environment } from 'src/environments/environment.prod';
import { Tooltip, Button } from '@material-ui/core';

const RolesPage = observer((props: RouteComponentProps) => {
  const [showConfirm, setShowConfirm] = useState<boolean>(false);
  const [removeId, setRemoveId] = useState<number>(null);

  const columns: EnhancedTableColumn<RoleListItemModel>[] = [
    {
      field: 'name',
      label: 'NAME'
    },
    {
      field: 'parentName',
      label: 'PARENT',
      render: rowData => {
        return rowData.parentName ? rowData.parentName : '-';
      }
    },
    {
      field: 'description',
      label: 'DESCRIPTION'
    },
    {
      field: 'updated',
      label: 'UPDATED',
      dataType: 'datetime'
    },
    {
      field: 'settings',
      label: 'SCOPES',
      render: rowData => {
        return (
          <Tooltip title="Edit scopes">
            <Button
              className="edit-scope"
              variant="contained"
              onClick={() => handleScopes(rowData)}
            >
              EDIT
            </Button>
          </Tooltip>
        );
      }
    }
  ];

  const idAccessor = (row: RoleListItemModel): number => {
    return row.id;
  };

  const dataStateChange = (
    query: EnhancedTableQuery<RoleListItemModel>
  ): void => {
    rolesStore.loadList(query);
  };

  const handleOpen = (rowData: RoleListItemModel) => {
    rolesStore.openDetail(rowData ? rowData.id : null);
  };

  const handleClose = () => {
    rolesStore.closeDetail();
  };

  const handleSave = (data: RoleSaveDataModel): void => {
    rolesStore.saveDetail(data, () => {
      rolesStore.refreshList();
    });
  };

  const handleScopes = (rowData: RoleListItemModel) => {
    scopesStore.reset();
    props.history.push(`/roles/${rowData.id}/scopes`);
  };

  const handleRemove = (data: RoleListItemModel) => {
    setRemoveId(data.id);
    setShowConfirm(true);
  };

  const handleRemoveConfirm = () => {
    setRemoveId(null);
    setShowConfirm(false);

    rolesStore.delete(removeId, () => {
      rolesStore.refreshList();
    });
  };

  return (
    <div className="roles-page">
      <div className="headers">
        <h2>Roles</h2>
        <div className="subheader">List of all roles</div>
      </div>
      <RoleDialog
        isOpened={rolesStore.dialogState === 'open'}
        parentItems={rolesStore.enums.filter(
          x => x.id !== rolesStore.detailId && !x.parentId
        )}
        loading={rolesStore.detail.loading}
        loadError={rolesStore.detail.loadError}
        data={rolesStore.detail.data}
        handleSave={handleSave}
        saving={rolesStore.detail.saving}
        saveError={rolesStore.detail.saveError}
        saveErrorData={rolesStore.detail.saveErrorData}
        handleClose={handleClose}
      />
      <ConfirmDialog
        title="Remove Role"
        message="Do you really want to remove this role?"
        open={showConfirm}
        handleCancel={() => setShowConfirm(false)}
        handleOk={() => handleRemoveConfirm()}
      />
      <EnhancedTable
        loading={rolesStore.list.loading}
        loadError={rolesStore.list.loadError}
        data={rolesStore.list.data}
        columns={columns}
        dataStateChange={dataStateChange}
        idAccessor={idAccessor}
        selectable={false}
        sortable={true}
        order={rolesStore.list.query.orderDirection}
        orderBy={rolesStore.list.query.orderBy}
        pageable={true}
        page={rolesStore.list.query.page}
        pageSize={rolesStore.list.query.pageSize}
        searchable={true}
        searchText={rolesStore.list.query.search}
        actions={[
          {
            icon: null,
            text: '+ Add new',
            isFreeAction: true,
            onClick: () => handleOpen(null)
          },
          {
            icon: 'edit',
            tooltip: 'Edit Role',
            isFreeAction: false,
            onClick: (event, rowData) => handleOpen(rowData)
          },
          {
            icon: 'delete',
            tooltip: 'Remove',
            isFreeAction: false,
            onClick: (event, rowData) => handleRemove(rowData)
          }
        ]}
      />
    </div>
  );
});

export default RolesPage;
