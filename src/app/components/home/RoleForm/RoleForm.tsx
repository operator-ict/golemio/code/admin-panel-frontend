import { CommonError, EnumItem } from '@dataplatform/shared/common-ui';
import {
  Button,
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  FormHelperText,
  FormLabel,
  FormControlLabel,
  Switch
} from '@material-ui/core';
import { FormikProps } from 'formik';
import React from 'react';
import { RoleFormModel } from '../../../models';
import './RoleForm.scss';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};

const RoleForm = (
  props: FormikProps<RoleFormModel> & {
    isNew: boolean;
    parentItems: EnumItem[];
    isParent: boolean;
    handleClose: () => void;
    loadError: boolean;
    saving: boolean;
  }
) => {
  const {
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    parentItems,
    handleClose,
    loadError,
    saving
  } = props;

  return (
    <form onSubmit={handleSubmit} noValidate>
      {props.loadError ? (
        <CommonError message="Error while loading data" />
      ) : (
          <Grid container>
            <Grid item xs={12}>
              <TextField
                error={errors.name && touched.name}
                helperText={(errors.name && touched.name) && errors.name}
                type="text"
                id="name"
                label="Name"
                margin="normal"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.name}
                required
                className="text-item"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                error={errors.description && touched.description}
                helperText={(errors.description && touched.description) && errors.description}
                type="text"
                id="description"
                label="Description"
                margin="normal"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.description}
                required
                className="text-item"
              />
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth={true}
                margin="normal"
                error={errors.parentId && touched.parentId}
                disabled={props.isParent}>
                <InputLabel shrink htmlFor="parentId">
                  Parent
                </InputLabel>
                <Select
                  value={values.parentId}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  displayEmpty
                  inputProps={{
                    name: 'parentId',
                    id: 'parentId'
                  }}
                  MenuProps={MenuProps}
                >
                  <MenuItem value="">
                    <em>(none)</em>
                  </MenuItem>
                  {parentItems.map(item => (
                    <MenuItem key={item.id} value={item.id}>
                      {item.name}
                    </MenuItem>
                  ))}
                </Select>
                {(props.isParent || (errors.parentId && touched.parentId)) ?
                  <FormHelperText>
                    {props.isParent ? <>Parent role can not have parent.</> : errors.parentId}
                  </FormHelperText> :
                  <FormHelperText>You can only select role with no further parents.</FormHelperText>
                }
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth={true} margin="normal" style={{ marginTop: '16px' }}>
                <FormLabel>Rate Limit</FormLabel>
                <FormControlLabel
                  control={
                    <Switch
                      checked={values.rateLimit}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      inputProps={{
                        name: 'rateLimit',
                        id: 'rateLimit',
                      }}
                    />
                  }
                  label=""
                />
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <TextField
                error={errors.count && touched.count}
                helperText={(errors.count && touched.count) && errors.count}
                type="number"
                id="count"
                disabled={!values.rateLimit}
                label="Count"
                margin="normal"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.count}
                className="text-item"
                inputProps={{ min: 0 }}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                error={errors.timeWindow && touched.timeWindow}
                helperText={(errors.timeWindow && touched.timeWindow) && errors.timeWindow}
                type="number"
                id="timeWindow"
                disabled={!values.rateLimit}
                label="Time Window (ms)"
                margin="normal"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.timeWindow}
                className="text-item"
                inputProps={{ min: 0 }}
              />
            </Grid>
          </Grid>
        )}

      <Grid item xs={12} className="buttons  absolute-dialog-btns">
        <Button onClick={handleClose} color="primary">
          Cancel
        </Button>
        {!loadError ? (
          <Button
            type="submit"
            disabled={isSubmitting && saving}
            color="primary"
            autoFocus
          >
            {props.isNew ? 'Save' : 'Save'}
          </Button>
        ) : null}
      </Grid>
    </form>
  );
};

export default RoleForm;
