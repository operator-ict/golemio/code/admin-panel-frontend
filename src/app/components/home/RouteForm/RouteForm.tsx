import { Autocomplete, CommonError } from '@dataplatform/shared/common-ui';
import { Button, FormControl, FormControlLabel, FormGroup, FormHelperText, FormLabel, Grid, InputLabel, MenuItem, Select, Switch, TextField } from '@material-ui/core';
import { FormikProps } from 'formik';
import React from 'react';
import { RouteFormModel, RouteMethodEnum } from '../../../models';
import './RouteForm.scss';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};

const RouteForm = (
  props: FormikProps<RouteFormModel> & {
    isNew: boolean;
    targets: string[];
    handleClose: () => void;
    loadError: boolean;
    saving: boolean;
  }
) => {
  const methods = Object.keys(RouteMethodEnum);

  const {
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    setFieldValue,
    setFieldTouched,
    handleBlur,
    handleSubmit,
    handleClose,
    loadError,
    saving
  } = props;

  return (
    <form onSubmit={handleSubmit} noValidate>
      {props.loadError ? (
        <CommonError message="Error while loading data" />
      ) : (
          <Grid container>
            <Grid item xs={12}>
              <FormControl
                fullWidth={true}
                margin="normal"
                required
                error={errors.method && touched.method ? true : false}>
                <InputLabel htmlFor="method">
                  Method
              </InputLabel>
                <Select
                  value={values.method}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  displayEmpty
                  inputProps={{
                    name: 'method',
                    id: 'method'
                  }}
                  MenuProps={MenuProps}
                >
                  {methods.map(item => (
                    <MenuItem key={RouteMethodEnum[item]} value={RouteMethodEnum[item]}>
                      {RouteMethodEnum[item]}
                    </MenuItem>
                  ))}
                </Select>
                {(errors.method && touched.method) ?
                  <FormHelperText>{errors.method}</FormHelperText> :
                  <FormHelperText>Select scope request method</FormHelperText>
                }
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <TextField
                error={errors.endpoint && touched.endpoint}
                helperText={
                  errors.endpoint && touched.endpoint ? errors.endpoint : 'Enter scope request endpoint'
                }
                type="text"
                id="endpoint"
                label="Endpoint"
                margin="normal"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.endpoint}
                required
                className="text-item"
              />
            </Grid>
            <Grid item xs={12}>
              <Autocomplete
                source={props.targets}
                suggestionType="contains"
                setFieldValue={setFieldValue}
                setFieldTouched={setFieldTouched}
                value={values.target}
                inputProps={{

                  error: errors.target && touched.target,
                  helperText: (errors.target && touched.target) ? errors.target : 'Must start with http:// or https:// and cannot end with /.',
                  type: "text",
                  id: "target",
                  label: "Target",
                  margin: "normal",
                  required: true,
                  className: "text-item"
                }}

              ></Autocomplete>
            </Grid>
            <Grid item xs={12}>
              <TextField
                error={errors.targetPath && touched.targetPath}
                helperText={
                  errors.targetPath && touched.targetPath ? errors.targetPath : 'Must start with character / and cannot contain question mark.'
                }
                type="text"
                id="targetPath"
                label="Target Path"
                margin="normal"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.targetPath}
                className="text-item"
              />
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth={true} margin="normal" style={{ marginTop: '16px' }}>
                <FormLabel>Public</FormLabel>
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={values.isPublic}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        inputProps={{
                          name: 'isPublic',
                          id: 'isPublic'
                        }}
                      />
                    }
                    label=""
                  />
                </FormGroup>
                <FormHelperText>Enable only if no authorization is required!</FormHelperText>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth={true} margin="normal" style={{ marginTop: '16px' }}>
                <FormLabel>Redirect</FormLabel>
                <FormGroup>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={values.isRedirect}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        inputProps={{
                          name: 'isRedirect',
                          id: 'isRedirect'
                        }}
                      />
                    }
                    label=""
                  />
                </FormGroup>
              </FormControl>
            </Grid>
          </Grid>
        )}

      <Grid item xs={12} className="buttons absolute-dialog-btns">
        <Button onClick={handleClose} color="primary">
          Cancel
        </Button>
        {!loadError ? (
          <Button
            type="submit"
            disabled={isSubmitting && saving}
            color="primary"
            autoFocus
          >
            {props.isNew ? 'Save' : 'Save'}
          </Button>
        ) : null}
      </Grid>
    </form>
  );
};

export default RouteForm;
