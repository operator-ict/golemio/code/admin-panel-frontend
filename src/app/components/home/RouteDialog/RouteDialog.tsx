import { CommonError, CommonRegexUtils } from '@dataplatform/shared/common-ui';
import { CircularProgress, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import { Formik, FormikProps } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import { RouteDetailModel, RouteFormModel, RouteSaveDataModel, RouteMethodEnum } from '../../../models';
import RouteForm from '../RouteForm/RouteForm';
import './RouteDialog.scss';

export interface RouteDialogProps {
  isOpened: boolean;
  targets: string[];

  loading: boolean;
  loadError: boolean;
  data: RouteDetailModel;
  handleSave: (data: RouteSaveDataModel) => void;
  saving: boolean;
  saveError: boolean;
  handleClose: () => void;
}

const RouteDialog = (props: RouteDialogProps) => {
  const submit = (values: RouteFormModel) => {
    props.handleSave(getSaveData(values));
  };

  const getInitialData = (): RouteFormModel => {
    return {
      method: (props.data && props.data.method) || '',
      endpoint: (props.data && props.data.endpoint) || '',
      target: (props.data && props.data.targetUrl) || '',
      targetPath: (props.data && props.data.targetPath) || '',
      isPublic: (props.data && props.data.isPublic) || false,
      isRedirect: (props.data && props.data.isRedirect) || false,
    };
  };

  const getSaveData = (values: RouteFormModel): RouteSaveDataModel => {
    return {
      method: values.method !== '' ? values.method : null,
      endpoint: values.endpoint.trim(),
      targetUrl: values.target.trim(),
      targetPath: values.targetPath.trim(),
      isPublic: values.isPublic,
      isRedirect: values.isRedirect
    };
  };

  return (
    <Dialog
      open={props.isOpened}
      onClose={props.handleClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle style={{ visibility: !props.loading && props.isOpened ? 'visible' : 'hidden' }} id="form-dialog-title">{props.data == null ? "Create" : "Edit"} Route</DialogTitle>
      <DialogContent>
        <div>
          <div className={props.loading || props.saving ? "overlay visible" : "overlay"}>
            <div className="center-vertical">
              <CircularProgress color="primary" />
            </div>
          </div>
        </div>
        <div style={{ visibility: props.loading && props.isOpened ? 'hidden' : 'visible' }}>
          {props.saveError ? (
            <CommonError message="Data was not saved." />
          ) : null}

          <Formik<RouteFormModel>
            enableReinitialize={true}
            initialValues={getInitialData()}
            onSubmit={values => {
              submit(values);
            }}
            validationSchema={Yup.object().shape({
              method: Yup.string().required('Required'),
              endpoint: Yup.string().required('Required').matches(CommonRegexUtils.startsWithSlash, 'Must start with /'),
              target: Yup.string().required('Required')
                .matches(CommonRegexUtils.routeTargetUrl, 'Must consist of protocol (http:// or https://), host name and optionally port only - with no trailing /')
                .matches(CommonRegexUtils.notEndsWithSlash, 'Must not end with /'),
              targetPath: Yup.string().required('Required').matches(CommonRegexUtils.startsWithSlash, 'Must start with /')
            })}
          >
            {(formikProps: FormikProps<RouteFormModel>) => {
              return (
                <RouteForm
                  {...formikProps}
                  isNew={props.data == null}
                  targets={props.targets}
                  handleClose={props.handleClose}
                  loadError={props.loadError}
                  saving={props.saving}
                />
              );
            }}
          </Formik>
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default RouteDialog;
