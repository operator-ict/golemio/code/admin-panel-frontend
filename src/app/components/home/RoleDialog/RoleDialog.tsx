import { CommonError, EnumItem } from '@dataplatform/shared/common-ui';
import {
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle
} from '@material-ui/core';
import { Formik, FormikProps } from 'formik';
import React from 'react';
import * as Yup from 'yup';
import { RoleDetailModel, RoleFormModel, RoleSaveDataModel } from '../../../models';
import RoleForm from '../RoleForm/RoleForm';
import './RoleDialog.scss';

export interface RoleDialogProps {
  isOpened: boolean;
  parentItems: EnumItem[];

  loading: boolean;
  loadError: boolean;
  data: RoleDetailModel;
  handleSave: (data: RoleSaveDataModel) => void;
  saving: boolean;
  saveError: boolean;
  saveErrorData: string;
  handleClose: () => void;
}

const RoleDialog = (props: RoleDialogProps) => {
  const submit = (values: RoleFormModel) => {
    props.handleSave(getSaveData(values));
  };

  const getInitialData = (): RoleFormModel => {
    return {
      name: (props.data && props.data.name) || '',
      description: (props.data && props.data.description) || '',
      parentId:
        props.data && props.data.parentId != null ? props.data.parentId : '',
      rateLimit:
        props.data && props.data.rateLimit != null
          ? props.data.rateLimit
          : false,
      count: props.data && props.data.count != null ? props.data.count : '',
      timeWindow:
        props.data && props.data.timeWindow != null ? props.data.timeWindow : ''
    };
  };

  const getSaveData = (values: RoleFormModel): RoleSaveDataModel => {
    return {
      name: values.name,
      description: values.description,
      parentId: values.parentId !== '' ? values.parentId : null,
      rateLimit: values.rateLimit,
      count: values.count !== '' ? values.count : null,
      timeWindow: values.timeWindow !== '' ? values.timeWindow : null
    };
  };

  const getErrorMessage = (): string => {
    if (props.saveError) {
      if (props.saveErrorData === 'err_parent_role_not_allowed') {
        return 'Parent can be neither itself nor role with parent.';
      } else if (props.saveErrorData === 'err_role_can_not_have_parent') {
        return 'Parent can not be set to existing parent role.';
      }
    }
    return '';
  }

  return (
    <Dialog
      open={props.isOpened}
      onClose={props.handleClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle style={{ visibility: !props.loading && props.isOpened ? 'visible' : 'hidden' }}
        id="form-dialog-title">{props.data == null ? "Create" : "Edit"} Role</DialogTitle>
      <DialogContent>
        <div>
          <div className={props.loading || props.saving ? "overlay visible" : "overlay"}>
            <div className="center-vertical">
              <CircularProgress color="primary" />
            </div>
          </div>
        </div>
        <div style={{ visibility: props.loading && props.isOpened ? 'hidden' : 'visible' }}>
          {props.saveError ? (
            <CommonError message="Data was not saved." subMessage={getErrorMessage()} />
          ) : null}

          <Formik<RoleFormModel>
            enableReinitialize={true}
            initialValues={getInitialData()}
            onSubmit={values => {
              submit(values);
            }}
            validationSchema={Yup.object().shape({
              name: Yup.string().required('Required'),
              description: Yup.string().required('Required'),
              count: Yup.number().when('rateLimit', {
                is: true,
                then: Yup.number().required('Required').min(0, 'Min. value is 0'),
                otherwise: Yup.number().notRequired()
              }),
              timeWindow: Yup.number().when('rateLimit', {
                is: true,
                then: Yup.number().required('Required').min(0, 'Min. value is 0'),
                otherwise: Yup.number().notRequired()
              }),
            })}
          >
            {(formikProps: FormikProps<RoleFormModel>) => {
              return (
                <RoleForm
                  {...formikProps}
                  isNew={props.data == null}
                  parentItems={props.parentItems}
                  isParent={props.data && props.data.isParent}
                  handleClose={props.handleClose}
                  loadError={props.loadError}
                  saving={props.saving}
                />
              );
            }}
          </Formik>
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default RoleDialog;
