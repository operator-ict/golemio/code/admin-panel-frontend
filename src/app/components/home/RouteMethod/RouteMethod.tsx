import React from 'react';
import './RouteMethod.scss';

export interface RouteMethodProps {
  method: string;
}

const RouteMethod = (props: RouteMethodProps) => {

  return (
    <span className={'method method-' + props.method.toLocaleLowerCase()}>{props.method}</span>
  );
};

export default RouteMethod;
