import {
  CommonError,
  EnumItem,
  CommonUiUtils,
  AsyncData
} from '@dataplatform/shared/common-ui';
import {
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormHelperText,
  FormLabel,
  Grid,
  InputLabel,
  ListItemText,
  MenuItem,
  Select,
  Switch,
  TextField
} from '@material-ui/core';
import { FormikProps } from 'formik';
import React from 'react';
import { UserFormModel } from '../../../models';
import './UserForm.scss';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};

const UserForm = (
  props: FormikProps<UserFormModel> & {
    isNew: boolean;
    roleItems: EnumItem[];
    refererItems: EnumItem[];
    handleClose: () => void;
    loadError: boolean;
    saving: boolean;
  }
) => {
  const {
    values,
    touched,
    errors,
    isSubmitting,
    handleChange,
    handleBlur,
    handleSubmit,
    roleItems,
    refererItems,
    handleClose,
    loadError,
    saving
  } = props;

  return (
    <form onSubmit={handleSubmit} noValidate>
      {props.loadError ? (
        <CommonError message="Error while loading data" />
      ) : (
          <Grid container>
            <Grid item xs={12}>
              <TextField
                error={errors.firstName && touched.firstName}
                helperText={(errors.firstName && touched.firstName) && errors.firstName}
                type="text"
                id="firstName"
                label="Name"
                margin="normal"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.firstName}
                required
                className="text-item"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                error={errors.lastName && touched.lastName}
                helperText={(errors.lastName && touched.lastName) && errors.lastName}
                type="text"
                id="lastName"
                label="Surname"
                margin="normal"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.lastName}
                required
                className="text-item"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                error={errors.email && touched.email}
                helperText={(errors.email && touched.email) ? errors.email : 'Cannot edit after create!'}
                type="email"
                id="email"
                label="Email"
                margin="normal"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
                required={props.isNew}
                disabled={!props.isNew}
                className="text-item"
              />
            </Grid>
            <Grid item xs={12}>
              <FormControl
                fullWidth={true}
                margin="normal"
                required={props.isNew}
                disabled={!props.isNew}
                error={errors.referer && touched.referer ? true : false}
              >
                <InputLabel htmlFor="referer">
                  Referer
              </InputLabel>
                <Select
                  value={values.referer}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  displayEmpty
                  inputProps={{
                    name: 'referer',
                    id: 'referer'
                  }}
                  MenuProps={MenuProps}
                >
                  {refererItems.map(item => (
                    <MenuItem key={item.id} value={item.id}>
                      {item.name}
                    </MenuItem>
                  ))}
                </Select>
                {(errors.referer && touched.referer) ?
                  <FormHelperText>{errors.referer}</FormHelperText> :
                  <FormHelperText>Select main app for user</FormHelperText>
                }
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <FormControl
                fullWidth={true}
                margin="normal"
                // required
                error={errors.roles && touched.roles ? true : false}
              >
                <InputLabel htmlFor="roles">Role</InputLabel>
                <Select
                  multiple
                  id="roles"
                  value={values.roles}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  renderValue={selected =>
                    CommonUiUtils.getEnumNames(roleItems, selected as number[])
                  }
                  MenuProps={MenuProps}
                  inputProps={{
                    name: 'roles',
                    id: 'roles'
                  }}
                >
                  {roleItems.map(item => (
                    <MenuItem key={item.id} value={item.id}>
                      <Checkbox
                        checked={values.roles.indexOf(item.id) > -1}
                        value={item.id}
                      />
                      <ListItemText primary={item.name} />
                    </MenuItem>
                  ))}
                </Select>
                {(errors.roles && touched.roles) ?
                  <FormHelperText>{errors.roles}</FormHelperText> :
                  <FormHelperText>Select a role for user</FormHelperText>
                }
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <FormControl fullWidth={true} margin="normal" style={{ marginTop: '16px' }}>
                <FormLabel>Rate Limit</FormLabel>
                <FormControlLabel
                  control={
                    <Switch
                      checked={values.rateLimit}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      inputProps={{
                        name: 'rateLimit',
                        id: 'rateLimit',
                      }}
                    />
                  }
                  label=""
                />
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <TextField
                error={errors.count && touched.count}
                helperText={(errors.count && touched.count) && errors.count}
                type="number"
                id="count"
                disabled={!values.rateLimit}
                label="Count"
                margin="normal"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.count}
                className="text-item"
                inputProps={{ min: 0 }}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                error={errors.timeWindow && touched.timeWindow}
                helperText={(errors.timeWindow && touched.timeWindow) && errors.timeWindow}
                type="number"
                id="timeWindow"
                disabled={!values.rateLimit}
                label="Time Window (ms)"
                margin="normal"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.timeWindow}
                className="text-item"
                inputProps={{ min: 0 }}
              />
            </Grid>
          </Grid>
        )}

      <Grid item xs={12} className="buttons absolute-dialog-btns">
        <Button onClick={handleClose} color="primary">
          Cancel
        </Button>
        {!loadError ? (
          <Button
            type="submit"
            disabled={isSubmitting && saving}
            color="primary"
            autoFocus
          >
            {props.isNew ? 'Save' : 'Save'}
          </Button>
        ) : null}
      </Grid>
    </form>
  );
};

export default UserForm;
