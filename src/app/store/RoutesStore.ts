import { action, observable, runInAction } from 'mobx';
import { RouteListItemModel, RouteDetailModel, RouteSaveDataModel } from '../models';
import { EnhancedTableQuery, EnhancedTableState, AsyncData } from '@dataplatform/shared/common-ui';
import { routesService } from '../services';

export interface RoutesStore {
    list: EnhancedTableState<RouteListItemModel>;
    loadList: (query: EnhancedTableQuery<RouteListItemModel>) => Promise<void>;
    refreshList: () => Promise<void>;
    dialogState?: 'open' | 'closed';
    detailId: number;
    detail: AsyncData<RouteDetailModel>;
    openDetail: (id: number | null) => Promise<void>,
    closeDetail: () => void;
    loadDetail: (id: number) => Promise<void>;
    saveDetail: (data: RouteSaveDataModel, successCallback: (result: boolean) => void) => Promise<void>;
    targetsLoaded: boolean;
    targets: string[];
    loadTargets: (rethrow?: boolean) => Promise<void>;
    resetTargets: () => void;
    delete: (userId: number, successCallback: (result: boolean) => void) => Promise<void>;
}
export const routesStore: RoutesStore = observable({
    list: {
        query: {
            filters: [],
            page: 0,
            pageSize: 10,
            search: null,
            orderBy: 'endpoint',
            orderDirection: 'asc'
        }
    },

    /**
     * Loads the route data for route tab
     *
     * @param query
     * @return Promise
     */
    loadList: async (query: EnhancedTableQuery<RouteListItemModel>): Promise<void> => {
        routesStore.list.loading = true;

        try {
            const result = await routesService.getAll(query);

            runInAction(() => {
                routesStore.list.query = query;
                routesStore.list.loadError = false;
                routesStore.list.data = result;
                routesStore.list.loading = false;
            });
        } catch{
            runInAction(() => {
                routesStore.list.loading = false;
                routesStore.list.loadError = true;
            });
        }
    },

    /**
     * Refreshes the list of routes
     *
     * @return Promise
     */
    refreshList: async (): Promise<void> => {
        routesStore.resetTargets();
        await routesStore.loadList(routesStore.list.query);
    },
    // DETAIL
    dialogState: 'closed',
    detailId: null,
    detail: {},

    /**
     * Opens a window with detailed information about the route
     *
     * @param id
     */
    openDetail: (id: number | null): Promise<void> => {
        routesStore.dialogState = 'open';
        routesStore.detailId = id;
        return routesStore.loadDetail(id);
    },

    /**
     * Closes the window
     */
    closeDetail: (): void => {
        routesStore.dialogState = 'closed';
        routesStore.detailId = null;
        routesStore.detail = {};
    },

    /**
     * Loads the role detailed information
     *
     * @param id
     */
    loadDetail: async (id: number): Promise<void> => {
        routesStore.detail.loading = true;

        let targetsPromise = Promise.resolve();
        if (!routesStore.targetsLoaded) {
            targetsPromise = routesStore.loadTargets();
        }

        let detailPromise = Promise.resolve(null);
        if (id) {
            detailPromise = routesService.getById(id);
        }

        const promises: [Promise<RouteDetailModel>, Promise<void>] = [
            detailPromise,
            targetsPromise
        ];

        try {
            const result = await Promise.all(promises);

            runInAction(() => {
                routesStore.detail.data = result[0];
                routesStore.detail.loading = false;
            });
        } catch{
            runInAction(() => {
                routesStore.detail.loading = false;
                routesStore.detail.loadError = true;
            });
        }
    },

    /**
     * Saves changes made to the edited route
     * Accepts a callback function which it
     * runs if everything was successfully run
     *
     * @param data
     * @param successCallback
     */
    saveDetail: async (data: RouteSaveDataModel, successCallback: (result: boolean) => void): Promise<void> => {
        routesStore.detail.saving = true;
        routesStore.detail.saveResultData = null;
        routesStore.detail.saveError = false;

        try {
            const result: boolean = await routesService.upsert(routesStore.detailId, data);

            runInAction(() => {
                routesStore.closeDetail();
                if (successCallback) {
                    return successCallback(result);
                }
            });
        } catch (err) {
            runInAction(() => {
                routesStore.detail.saving = false;
                routesStore.detail.saveError = true;
            });
        }
    },

    // TARGETS

    targetsLoaded: false,
    targets: [],

    /**
     * Loads targets into the targets state and sets targetsLoaded to true
     *
     * @param rethrow
     * @throws err
     */
    loadTargets: async (rethrow: boolean = true): Promise<void> => {
        routesStore.targetsLoaded = false;

        try {
            const result = await routesService.getTargets();

            runInAction(() => {
                routesStore.targets = result;
                routesStore.targetsLoaded = true;
            });
        } catch (err) {
            if (rethrow) {
                throw err;
            }
        }
    },

    /**
     * Sets targetsLoaded to false
     */
    resetTargets: (): void => {
        routesStore.targetsLoaded = false;
    },

    /**
     * Deletes route by its id.
     * Accepts a callback function and executes it if provided
     *
     * @param routeId
     * @param successCallback
     */
    delete: async (routeId: number, successCallback: (result: boolean) => void): Promise<void> => {
        try {
            const result: boolean = await routesService.delete(routeId);

            runInAction(() => {
                if (successCallback) {
                    return successCallback(result);
                }
            });
        } catch{
        }
    },
});

export default routesStore;
