import { observable, runInAction } from 'mobx';
import { RoleListItemModel, RoleDetailModel, RoleSaveDataModel } from '../models';
import { EnhancedTableQuery, EnhancedTableState, EnumItem, AsyncData, EnhancedTableQueryResult } from '@dataplatform/shared/common-ui';
import { rolesService } from '../services';

export interface RolesStore {
    list: EnhancedTableState<RoleListItemModel>;
    loadList: (query: EnhancedTableQuery<RoleListItemModel>) => Promise<void>;
    refreshList: () => Promise<void>;
    dialogState?: 'open' | 'closed';
    detailId: number;
    detail: AsyncData<RoleDetailModel, boolean, string>;
    openDetail: (id: number | null) => Promise<void>;
    closeDetail: () => void;
    loadDetail: (id: number) => Promise<void>;
    saveDetail: (data: RoleSaveDataModel, successCallback: (result: boolean) => void) => Promise<void>;
    enumsLoaded: boolean;
    enums: EnumItem<number, { parentId: number }>[];
    loadEnums: (rethrow?: boolean) => Promise<void>;
    resetEnums: () => void;
    delete: (userId: number, successCallback: (result: boolean) => void) => Promise<void>;
}

export const rolesStore: RolesStore = observable({
    list: {
        query: {
            filters: [],
            page: 0,
            pageSize: 10,
            search: null,
            orderBy: 'name',
            orderDirection: 'asc'
        }
    },
    /**
     * Loads the data for role page
     *
     * @param query
     */
    loadList: async (query: EnhancedTableQuery<RoleListItemModel>): Promise<void> => {
        rolesStore.list.loading = true;

        let roleEnumsPromise = Promise.resolve();
        if (!rolesStore.enumsLoaded) {
            roleEnumsPromise = rolesStore.loadEnums();
        }

        const promises: [Promise<EnhancedTableQueryResult<RoleListItemModel>>, Promise<void>] = [
            rolesService.getAll(query),
            roleEnumsPromise
        ];

        try {
            const result = await Promise.all(promises);

            runInAction(() => {
                rolesStore.list.query = query;
                rolesStore.list.loadError = false;
                rolesStore.list.data = result[0];
                rolesStore.list.loading = false;
            });
        } catch {
            runInAction(() => {
                rolesStore.list.loading = false;
                rolesStore.list.loadError = true;
            });
        }
    },

    /**
     * Refreshes the list of roles
     */
    refreshList: async (): Promise<void> => {
        rolesStore.resetEnums();
        await rolesStore.loadList(rolesStore.list.query);
    },
    dialogState: 'closed',
    detailId: null,
    detail: {},

    /**
     * Opens a window with detailed information about the role
     *
     * @param id:number|null
     */
    openDetail: (id: number | null): Promise<void> => {
        rolesStore.dialogState = 'open';
        rolesStore.detailId = id;
        return rolesStore.loadDetail(id);
    },

    /**
     * Closes the window
     */
    closeDetail: (): void => {
        rolesStore.dialogState = 'closed';
        rolesStore.detailId = null;
        rolesStore.detail = {};
    },

    /**
     * Loads the role detailed information
     * @param id
     */
    loadDetail: async (id: number): Promise<void> => {
        rolesStore.detail.loading = true;

        let rolesPromise = Promise.resolve();
        if (!rolesStore.enumsLoaded) {
            rolesPromise = rolesStore.loadEnums();
        }

        let detailPromise = Promise.resolve(null);
        if (id) {
            detailPromise = rolesService.getById(id);
        }

        const promises: [Promise<RoleDetailModel>, Promise<void>] = [
            detailPromise,
            rolesPromise
        ];

        try {
            const result = await Promise.all(promises);

            runInAction(() => {
                rolesStore.detail.data = result[0];
                rolesStore.detail.loading = false;
            });
        } catch{
            runInAction(() => {
                rolesStore.detail.loading = false;
                rolesStore.detail.loadError = true;
            });
        }
    },

    /**
     * Saves changes made to the edited role
     *
     * @param data
     * @param successCallback
     */
    saveDetail: async (data: RoleSaveDataModel, successCallback: (result: boolean) => void): Promise<void> => {
        rolesStore.detail.saving = true;
        rolesStore.detail.saveResultData = null;
        rolesStore.detail.saveError = false;
        rolesStore.detail.saveErrorData = null;

        try {
            const result: boolean = await rolesService.upsert(rolesStore.detailId, data);

            runInAction(() => {
                rolesStore.closeDetail();
                if (successCallback) {
                    return successCallback(result);
                }
            });
        } catch (err) {
            runInAction(() => {
                let error: string;
                if (err.response) {
                    if (err.response.data) {
                        error = err.response.data.error_info;
                    }
                }

                rolesStore.detail.saving = false;
                rolesStore.detail.saveError = true;
                rolesStore.detail.saveErrorData = error;
            });
        }
    },
    enumsLoaded: false,
    enums: [],

    /**
     * Loads enums into the enums state
     * sets enumsLoaded to true
     *
     * @param rethrow
     * @throws err
     */
    loadEnums: async (rethrow: boolean = true): Promise<void> => {
        rolesStore.enumsLoaded = false;

        try {
            const result = await rolesService.getAsEnums();

            runInAction(() => {
                rolesStore.enums = result;
                rolesStore.enumsLoaded = true;
            });
        } catch (err) {
            if (rethrow) {
                throw err;
            }
        }
    },

    /**
     * Sets enumsLoaded state to false
     */
    resetEnums: (): void => {
        rolesStore.enumsLoaded = false;
    },

    /**
     * Deletes role by its id.
     * Accepts a callback function and executes it if provided
     *
     * @param roleId
     * @param successCallback
     * @return Promise
     */
    delete: async (roleId: number, successCallback: (result: boolean) => void): Promise<void> => {
        try {
            const result: boolean = await rolesService.delete(roleId);

            runInAction(() => {
                if (successCallback) {
                    return successCallback(result);
                }
            });
        } catch{
        }
    }
});

export default rolesStore;
