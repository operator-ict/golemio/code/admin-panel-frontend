import { AsyncData, EnhancedTableQuery, EnhancedTableState } from '@dataplatform/shared/common-ui';
import { action, observable, runInAction } from 'mobx';
import { RoleDetailModel, ScopeDetailModel, ScopeListItemModel, ScopeSaveDataModel } from '../models';
import { rolesService, scopesService } from '../services';
import { limitationsStore } from './LimitationsStore';

export interface ScopesStore {
    role: AsyncData<RoleDetailModel>;
    reset: () => void;
    loadRole: (roleId: number) => Promise<void>;

    // LIST OF ROLE SCOPES
    lastRoleId: number;
    list: EnhancedTableState<ScopeListItemModel>;
    loadList: (roleId: number, query: EnhancedTableQuery<ScopeListItemModel>) => Promise<void>;
    delete: (roleId: number, scopeId: number, successCallback: (result: boolean) => void) => Promise<void>;
    refreshList: (roleId: number) => Promise<void>;

    // LIST OF ROLE SCOPES
    lastParentRoleId: number;
    inheritedList: EnhancedTableState<ScopeListItemModel>;
    loadInheritedList: (query: EnhancedTableQuery<ScopeListItemModel>) => Promise<void>;
    refreshInheritedList: () => Promise<void>;
    dialogState: 'open' | 'closed';
    detailRoleId: number;
    detailScopeId: number;
    detail: AsyncData<ScopeDetailModel, boolean, number>;
    openDetail: (roleId: number, scopeId: number | null) => Promise<void>;
    closeDetail: () => void;
    loadDetail: (roleId: number, scopeId: number) => Promise<void>;
    saveDetail: (data: ScopeSaveDataModel, successCallback: (result: boolean) => void) => Promise<void>;
}

export const scopesStore: ScopesStore = observable({
    role: {},

    /**
     * Resets the role array, sets the list data to null and
     * sets the inheritedList data to null
     */
    reset: () => {
        scopesStore.role = {};
        scopesStore.list.data = null;
        scopesStore.inheritedList.data = null;
    },

    /**
     * Loads the role using its id
     *
     * @param roleId
     * @return Promise
     */
    loadRole: async (roleId: number): Promise<void> => {
        scopesStore.role.loading = true;
        scopesStore.role.data = null;

        let detailPromise = Promise.resolve(null);
        if (roleId) {
            detailPromise = rolesService.getById(roleId);
        }

        const promises: [Promise<RoleDetailModel>] = [
            detailPromise
        ];

        try {
            const result = await Promise.all(promises);

            runInAction(() => {
                scopesStore.role.data = result[0];
                scopesStore.role.loading = false;
                scopesStore.role.loadError = false;
                scopesStore.role.loadErrorMessage = null;
            });
        } catch (err) {
            runInAction(() => {
                scopesStore.role.loading = false;
                scopesStore.role.loadError = true;
                scopesStore.role.loadErrorMessage = err.response.status === 404 ? 'Role was not found.' : null;
            });
        }
    },

    // LIST OF ROLE SCOPES

    lastRoleId: null,
    list: {
        query: {
            filters: [],
            page: 0,
            pageSize: 5,
            search: null,
            orderBy: '',
            orderDirection: 'asc'
        }
    },

    /**
     * Loads the scopes data for specific role
     *
     * @param roleId
     * @param query
     */
    loadList: async (roleId: number, query: EnhancedTableQuery<ScopeListItemModel>): Promise<void> => {
        if (scopesStore.lastRoleId !== roleId) {
            scopesStore.list.data = null;
            scopesStore.lastRoleId = roleId;
        }
        scopesStore.list.loading = true;

        try {
            const result = await scopesService.getAll(roleId, query);

            runInAction(() => {
                scopesStore.list.query = query;
                scopesStore.list.loadError = false;
                scopesStore.list.loading = false;
                scopesStore.list.data = result;
            });
        } catch (err) {
            runInAction(() => {
                scopesStore.list.loading = false;
                scopesStore.list.loadError = true;
            });
        }
    },

    /**
     * Deletes a scope using its id
     *
     * @param roleId
     * @param scopeId
     * @param successCallback
     */
    delete: async (roleId: number, scopeId: number, successCallback: (result: boolean) => void): Promise<void> => {
        try {
            const result: boolean = await scopesService.delete(roleId, scopeId);

            runInAction(() => {
                if (successCallback) {
                    return successCallback(result);
                }
            });
        } catch{
        }
    },
    refreshList: async (roleId: number): Promise<void> => {
        await scopesStore.loadList(roleId, scopesStore.list.query);
    },

    // LIST OF ROLE SCOPES

    lastParentRoleId: null,
    inheritedList: {
        query: {
            filters: [],
            page: 0,
            pageSize: 5,
            search: null,
            orderBy: '',
            orderDirection: 'asc'
        }
    },

    /**
     * Loads the inherited scopes if the route has parentId
     *
     * @param query
     */
    loadInheritedList: async (query: EnhancedTableQuery<ScopeListItemModel>): Promise<void> => {
        if (scopesStore.role && scopesStore.role.data && scopesStore.role.data.parentId) {
            if (scopesStore.lastParentRoleId !== scopesStore.role.data.parentId) {
                scopesStore.inheritedList.data = null;
                scopesStore.lastParentRoleId = scopesStore.role.data.parentId;
            }
            scopesStore.inheritedList.loading = true;

            try {
                const result = await scopesService.getAll(scopesStore.role.data.parentId, query);

                runInAction(() => {
                    scopesStore.inheritedList.query = query;
                    scopesStore.inheritedList.loadError = false;
                    scopesStore.inheritedList.data = result;
                    scopesStore.inheritedList.loading = false;
                });
            } catch (err) {
                runInAction(() => {
                    scopesStore.inheritedList.loading = false;
                    scopesStore.inheritedList.loadError = true;
                });
            }
        } else {
            scopesStore.inheritedList.query = query;
            scopesStore.inheritedList.loadError = false;
            scopesStore.inheritedList.data = null;
        }
    },

    /**
     * Refreshes the inherited scopes of a route
     *
     * @return Promise
     */
    refreshInheritedList: async (): Promise<void> => {
        await scopesStore.loadInheritedList(scopesStore.inheritedList.query);
    },

    // DETAIL OF ROLE SCOPE

    dialogState: 'closed',
    detailRoleId: null,
    detailScopeId: null,
    detail: {},

    /**
     * Opens a window with details of chosen scope
     *
     * @param roleId
     * @param scopeId
     */
    openDetail: (roleId: number, scopeId: number | null): Promise<void> => {
        scopesStore.dialogState = 'open';
        scopesStore.detailRoleId = roleId;
        scopesStore.detailScopeId = scopeId;
        return scopesStore.loadDetail(roleId, scopeId);
    },

    /**
     * Closes the detail window
     * Cleans up the state
     */
    closeDetail: (): void => {
        scopesStore.dialogState = 'closed';
        scopesStore.detailRoleId = null;
        scopesStore.detailScopeId = null;
        scopesStore.detail = {};
    },

    /**
     * Loads the scope details into the state
     *
     * @param roleId
     * @param scopeId
     * @return Promise
     */
    loadDetail: async (roleId: number, scopeId: number): Promise<void> => {
        scopesStore.detail.loading = true;

        let limitationsPromise = Promise.resolve();
        if (!limitationsStore.enumsLoaded) {
            limitationsPromise = limitationsStore.loadEnums();
        }

        let detailPromise = Promise.resolve(null);
        if (roleId && scopeId) {
            detailPromise = scopesService.getById(roleId, scopeId);
        }

        const promises: [Promise<ScopeDetailModel>, Promise<void>] = [
            detailPromise,
            limitationsPromise
        ];

        try {
            const result = await Promise.all(promises);

            runInAction(() => {
                scopesStore.detail.data = result[0];
                scopesStore.detail.loading = false;
            });
        } catch{
            runInAction(() => {
                scopesStore.detail.loading = false;
                scopesStore.detail.loadError = true;
            });
        }
    },

    /**
     * Saves the detail of edited scope
     *
     * @param data
     * @param successCallback
     */
    saveDetail: async (data: ScopeSaveDataModel, successCallback: (result: boolean) => void): Promise<void> => {
        scopesStore.detail.saving = true;
        scopesStore.detail.saveResultData = null;
        scopesStore.detail.saveError = false;
        scopesStore.detail.saveErrorData = null;

        try {
            const result: boolean = await scopesService.upsert(scopesStore.detailRoleId, scopesStore.detailScopeId, data);

            runInAction(() => {
                scopesStore.closeDetail();
                if (successCallback) {
                    return successCallback(result);
                }
            });
        } catch (err) {
            runInAction(() => {
                let errorStatus: number;
                if (err.response) {
                    if (err.response.data) {
                        errorStatus = err.response.data.error_status;
                    }
                }

                scopesStore.detail.saving = false;
                scopesStore.detail.saveError = true;
                scopesStore.detail.saveErrorData = errorStatus;
            });
        }
    }
});

export default scopesStore;
