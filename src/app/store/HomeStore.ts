import { action, observable, runInAction } from 'mobx';
import { StatusType, RouteEnum, ChangePasswordModel } from '../models';
import { authService } from '../services';

export interface HomeStore {
    tab: RouteEnum;
    changePasswordDialogState?: 'open' | 'closed';
    changePasswordSaving: boolean;
    changePasswordError: boolean;
    changePasswordErrorType?: 'invalid_password' | 'other';
    setTab: (tab: RouteEnum) => void;
    toggleChangePasswordDialog: (open: boolean) => void;
    changePassword: (data: ChangePasswordModel) => Promise<void>;
}

export const homeStore: HomeStore = observable({
    tab: RouteEnum.USERS,
    changePasswordDialogState: null,
    changePasswordSaving: false,
    changePasswordError: false,
    changePasswordErrorType: null,

    /**
     * Sets the tab name into the state
     *
     * @param tab
     */
    setTab: (tab: RouteEnum) => {
        homeStore.tab = tab;
    },

    /**
     * Toggles the Change Password dialog window
     *
     * @param open
     */
    toggleChangePasswordDialog: (open: boolean) => {
        homeStore.changePasswordDialogState = open ? 'open' : 'closed';
    },

    /**
     * Changes the password of the currently logged-in user
     *
     * @param data  old password and new password
     */
    changePassword: async (data: ChangePasswordModel) => {
        homeStore.changePasswordSaving = true;

        try {
            const result = await authService.changePassword(data);

            runInAction(() => {
                homeStore.changePasswordSaving = false;
                homeStore.changePasswordErrorType = null;
                homeStore.changePasswordError = false;
                homeStore.toggleChangePasswordDialog(false);
            })
        } catch (ex) {
            runInAction(() => {
                homeStore.changePasswordSaving = false;
                const errorStatus = ex.response && ex.response.data ? ex.response.data.error_status : null;
                this.changePasswordErrorType = errorStatus === 403 ? 'invalid_password' : 'other';
                homeStore.changePasswordError = true;
            });
        }
    }
});

export default homeStore;
