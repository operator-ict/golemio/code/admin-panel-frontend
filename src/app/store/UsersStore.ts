import { EnhancedTableQuery, EnhancedTableQueryResult, EnhancedTableState, AsyncData } from '@dataplatform/shared/common-ui';
import { action, observable, runInAction } from 'mobx';
import { UserListItemModel, UserDetailModel, UserSaveDataModel } from '../models';
import { usersService } from '../services';
import { rolesStore } from './RolesStore';
import { referersStore } from './ReferersStore';

export interface UsersStore {
    list: EnhancedTableState<UserListItemModel>;
    loadList: (query: EnhancedTableQuery<UserListItemModel>) => Promise<void>;
    refreshList: () => Promise<void>;
    dialogState?: 'open' | 'closed';
    detailId: number;
    detail: AsyncData<UserDetailModel>;
    openDetail: (id: number | null) => Promise<void>;
    closeDetail: () => void;
    loadDetail: (id: number) => Promise<void>;
    saveDetail: (data: UserSaveDataModel, successCallback: (result: boolean) => void) => Promise<void>;
    delete: (userId: number, successCallback: (result: boolean) => void) => Promise<void>;
}

/**
 * Initialize the usersStore
 */
export const usersStore: UsersStore = observable({
    list: {
        query: {
            filters: [],
            page: 0,
            pageSize: 10,
            search: null,
            orderBy: 'email',
            orderDirection: 'asc'
        }
    },

    /**
     * Loads the users for users tab
     *
     * @param query
     * @return Promise
     */
    loadList: async (query: EnhancedTableQuery<UserListItemModel>): Promise<void> => {
        usersStore.list.loading = true;

        let rolesPromise = Promise.resolve();
        if (!rolesStore.enumsLoaded) {
            rolesPromise = rolesStore.loadEnums();
        }

        const promises: [Promise<EnhancedTableQueryResult<UserListItemModel>>, Promise<void>] = [
            usersService.getAll(query),
            rolesPromise
        ];

        try {
            const result = await Promise.all(promises);

            runInAction(() => {
                usersStore.list.query = query;
                usersStore.list.loadError = false;
                usersStore.list.data = result[0];
                usersStore.list.loading = false;
            });
        } catch{
            runInAction(() => {
                usersStore.list.loading = false;
                usersStore.list.loadError = true;
            });
        }
    },
    refreshList: (): Promise<void> => {
        rolesStore.resetEnums();
        return usersStore.loadList(usersStore.list.query);
    },

    // DETAIL

    dialogState: 'closed',
    detailId: null,
    detail: {},

    /**
     * Opens a window with detailed information about the user
     *
     * @param id
     * @return Promise
     */
    openDetail: async (id: number | null): Promise<void> => {
        usersStore.dialogState = 'open';
        usersStore.detailId = id;
        return usersStore.loadDetail(id);
    },

    /**
     * Sets the state dialogState to closed
     */
    closeDetail: (): void => {
        usersStore.dialogState = 'closed';
        usersStore.detailId = null;
        usersStore.detail = {};
    },

    /**
     *
     * @param id
     */
    loadDetail: async (id: number): Promise<void> => {
        usersStore.detail.loading = true;

        let rolesPromise = Promise.resolve();
        if (!rolesStore.enumsLoaded) {
            rolesPromise = rolesStore.loadEnums();
        }

        let referersPromise = Promise.resolve();
        if (!referersStore.enumsLoaded) {
            referersPromise = referersStore.loadEnums();
        }

        let detailPromise = Promise.resolve(null);
        if (id) {
            detailPromise = usersService.getById(id);
        }

        const promises: [Promise<UserDetailModel>, Promise<void>, Promise<void>] = [
            detailPromise,
            rolesPromise,
            referersPromise
        ];

        try {
            const result = await Promise.all(promises);

            runInAction(() => {
                usersStore.detail.data = result[0];
                usersStore.detail.loading = false;
            });
        } catch{
            runInAction(() => {
                usersStore.detail.loading = false;
                usersStore.detail.loadError = true;
            });
        }
    },
    saveDetail: async (data: UserSaveDataModel, successCallback: (result: boolean) => void): Promise<void> => {
        usersStore.detail.saving = true;
        usersStore.detail.saveResultData = null;
        usersStore.detail.saveError = false;

        try {
            const result: boolean = await usersService.upsert(usersStore.detailId, data);

            runInAction(() => {
                usersStore.closeDetail();
                if (successCallback) {
                    return successCallback(result);
                }
            });
        } catch{
            runInAction(() => {
                usersStore.detail.saving = false;
                usersStore.detail.saveError = true;
            });
        }
    },
    delete: async (userId: number, successCallback: (result: boolean) => void): Promise<void> => {
        try {
            const result: boolean = await usersService.delete(userId);

            runInAction(() => {
                if (successCallback) {
                    return successCallback(result);
                }
            });
        } catch{
        }
    }
});

export default usersStore;
