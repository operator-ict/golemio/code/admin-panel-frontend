import { EnumItem } from '@dataplatform/shared/common-ui';
import { observable, runInAction } from 'mobx';
import { referersService } from '../services';

export interface ReferersStore {
    enumsLoaded: boolean;
    enums: EnumItem[];
    loadEnums(rethrow?: boolean): Promise<void>;
    resetEnums: (rethrow?: boolean) => void;

}
export const referersStore: ReferersStore = observable({
    enumsLoaded: false,
    enums: [],

    /**
     * Loads enums into the enums state
     *
     * @param rethrow
     */
    loadEnums: async (rethrow: boolean = true) => {
        referersStore.enumsLoaded = false;

        try {
            const result = await referersService.getAsEnums();

            runInAction(() => {
                referersStore.enums = result;
                referersStore.enumsLoaded = true;
            });
        } catch (err) {
            if (rethrow) {
                throw err;
            }
        }
    },

    /**
     * Sets enumsLoaded to false
     */
    resetEnums: () => {
        referersStore.enumsLoaded = false;
    }
});
export default referersStore;

