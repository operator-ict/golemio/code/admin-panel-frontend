import { EnumItem } from '@dataplatform/shared/common-ui';
import { observable, runInAction } from 'mobx';
import { limitationsService } from '../services/LimitationsService';
import { LimitationTypeEnum } from '../models';

export interface LimitationsStore {
    enumsLoaded: boolean;
    enums: EnumItem<LimitationTypeEnum>[];
    loadEnums: (rethrow?: boolean) => Promise<void>;
    resetEnums: () => void;
}
export const limitationsStore: LimitationsStore = observable({
    enumsLoaded: false,
    enums: [],

    /**
     * Loads the limitations into the state
     *
     * @param rethrow
     */
    loadEnums: async (rethrow: boolean = true) => {
        limitationsStore.enumsLoaded = false;

        try {
            const result = await limitationsService.getAll();

            runInAction(() => {
                limitationsStore.enums = result.map(x => {
                    return {
                        id: x,
                        name: LimitationTypeEnum[x]
                    }
                });
                limitationsStore.enumsLoaded = true;
            });
        } catch (err) {
            if (rethrow) {
                throw err;
            }
        }
    },

    /**
     * Sets enumsLoaded state to false
     */
    resetEnums: () => {
        limitationsStore.enumsLoaded = false;
    }
});

export default limitationsStore;
