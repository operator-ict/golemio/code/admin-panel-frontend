import { observable, runInAction } from 'mobx';
import {
  AuthModel,
  LoginModel,
  StatusType,
  ForgottenPasswordModel,
  PasswordResetModel,
  PasswordResetErrorType
} from '../models';
import { authService } from '../services';
import { getToken } from '@dataplatform/shared/common-token';

export interface AuthStore {
  apiVersion: string;
  status?: StatusType;
  authUser: AuthModel;
  authenticated: boolean;
  tokenChecked: boolean;
  signIn: (data: LoginModel) => Promise<void>;
  fetchApiVersion: () => Promise<void>;
  signOut: () => void;
  checkToken: () => void;
  forgottenPasswordSaved: boolean;
  forgottenPasswordSaving: boolean;
  forgottenPasswordError: boolean;
  forgottenPasswordErrorType?: 'not_found' | 'other';
  forgottenPasswordInit: () => void;
  forgottenPasswordRequest: (email: string) => Promise<void>;
  passwordResetCodeCheckLoading: boolean;
  passwordResetCodeCheckError: boolean;
  passwordResetCodeCheckErrorType?: PasswordResetErrorType;
  passwordResetInit: () => void;
  passwordResetCodeCheck: (code: string) => Promise<void>;
  passwordResetSaved: boolean;
  passwordResetSaving: boolean;
  passwordResetError: boolean;
  passwordResetErrorType?: PasswordResetErrorType;
  passwordReset: (code: string, data: PasswordResetModel) => Promise<void>;
}

export const authStore: AuthStore = observable({
  apiVersion: '',
  status: null,
  errorType: null,
  authUser: {
    email: null,
    token: null
  },
  authenticated: false,
  tokenChecked: false,

  /**
   * Authenticates the user and remembers him in the state
   * Sets the state of authUser
   * Sets the state authenticated to true
   *
   * @param data  Email, password
   * @return void
   */
  signIn: async (data: LoginModel) => {
    try {
      const result = await authService.signIn(data);

      runInAction(() => {
        authStore.authUser = {
          email: data.email,
          token: result.token
        };
        authStore.authenticated = true;
        authStore.status = 'success';
      });
    } catch (ex) {
      runInAction(() => {
        authStore.authenticated = false;
        authStore.status = 'error';
      });
    }
  },

  /**
   * Fetches the api version from authService and sets the state apiVersion
   */
  fetchApiVersion: async () => {
    try {
      const result = await authService.getApiVersion();
      runInAction(() => {
        authStore.apiVersion = result;
      });
    } catch (ex) {
      authStore.apiVersion = '';
    }
  },

  /**
   * Deauthenticates the currently logged-in user
   * sets the state authUser to null, sets the state tokenChecked amd authenticated to false
   * Calls the signOut function from authService
   */
  signOut: () => {
    authStore.authUser = {
      email: null,
      token: null
    };
    authStore.authenticated = false;
    authStore.tokenChecked = false;

    authService.signOut();
  },

  /**
   * Fetches the token using function getToken() and using the value from token decides
   * if the user was authenticated or not
   */
  checkToken: () => {
    // TODO check if token is not expired
    try {
      // const result = await authService.verifyToken(data);

      runInAction(() => {
        // if (result.ok) {
        authStore.authUser = {
          email: null,
          token: getToken()
        };
        authStore.authenticated = authStore.authUser.token ? true : false;
        authStore.tokenChecked = true;
        // }
      });
    } catch (ex) {
      authStore.authUser = {
        email: null,
        token: null
      };
      authStore.authenticated = false;
      authStore.tokenChecked = true;
    }
  },
  forgottenPasswordSaved: false,
  forgottenPasswordSaving: false,
  forgottenPasswordError: false,
  forgottenPasswordErrorType: null,
  forgottenPasswordInit: () => {
    authStore.forgottenPasswordSaved = false;
    authStore.forgottenPasswordSaving = false;
    authStore.forgottenPasswordError = false;
    authStore.forgottenPasswordErrorType = null;
  },

  /**
   * Sends a forgottenPasswordRequest from authService
   * Sets the state of forgottenPasswordSaving to false
   * Sets the state of forgottenPasswordError to false
   * Sets the state of forgottenPasswordSaved to true
   * Sets the state of forgottenPasswordErrorType to null
   *
   * @param data  Email of the user
   */
  forgottenPasswordRequest: async (email: string) => {
    authStore.forgottenPasswordSaving = true;
    authStore.forgottenPasswordSaved = false;

    try {
      const result = await authService.forgottenPasswordRequest({ email });

      runInAction(() => {
        authStore.forgottenPasswordSaving = false;
        authStore.forgottenPasswordError = false;
        authStore.forgottenPasswordSaved = true;
        authStore.forgottenPasswordErrorType = null;
      });
    } catch (ex) {
      runInAction(() => {
        authStore.forgottenPasswordSaving = false;
        authStore.forgottenPasswordError = true;
        authStore.forgottenPasswordSaved = false;
        const errorStatus =
          ex.response && ex.response.data
            ? ex.response.data.error_status
            : null;
        this.forgottenPasswordErrorType =
          errorStatus === 404 ? 'not_found' : 'other';
      });
    }
  },
  passwordResetCodeCheckLoading: false,
  passwordResetCodeCheckError: false,
  passwordResetCodeCheckErrorType: null,
  passwordResetInit: () => {
    authStore.passwordResetCodeCheckLoading = false;
    authStore.passwordResetCodeCheckError = false;
    authStore.passwordResetCodeCheckErrorType = null;
    authStore.passwordResetSaved = false;
    authStore.passwordResetSaving = false;
    authStore.passwordResetError = false;
    authStore.passwordResetErrorType = null;
  },
  passwordResetCodeCheck: async (code: string) => {
    authStore.passwordResetCodeCheckLoading = true;
    authStore.passwordResetCodeCheckError = false;
    authStore.passwordResetCodeCheckErrorType = null;

    try {
      const result = await authService.forgottenPasswordCodeCheck(code);

      runInAction(() => {
        authStore.passwordResetCodeCheckLoading = false;
        authStore.passwordResetCodeCheckError = result != null;
        authStore.passwordResetCodeCheckErrorType = result;
      });
    } catch (ex) {
      runInAction(() => {
        authStore.passwordResetCodeCheckLoading = false;
        authStore.passwordResetCodeCheckError = true;
        authStore.passwordResetErrorType = 'other';
      });
    }
  },
  passwordResetSaved: false,
  passwordResetSaving: false,
  passwordResetError: false,
  passwordResetErrorType: null,
  passwordReset: async (code: string, data: PasswordResetModel) => {
    authStore.passwordResetSaving = true;
    authStore.passwordResetSaved = false;

    try {
      const result = await authService.forgottenPasswordReset(code, data);

      runInAction(() => {
        authStore.passwordResetSaving = false;
        authStore.passwordResetError = false;
        authStore.passwordResetSaved = true;
        authStore.passwordResetErrorType = null;
      });
    } catch (ex) {
      runInAction(() => {
        authStore.passwordResetSaving = false;
        authStore.passwordResetError = true;
        authStore.passwordResetSaved = false;
        const error =
          ex.response && ex.response.data ? ex.response.data.error : null;
        // TODO - predelat podle novych error responsu
        if (error === 'err_not_found') {
          // status 404
          authStore.passwordResetErrorType = 'not_found';
        } else if (error === 'err_already_expired') {
          // status 400
          authStore.passwordResetErrorType = 'expired';
        } else {
          authStore.passwordResetErrorType = 'other';
        }
      });
    }
  }
});
export default authStore;
