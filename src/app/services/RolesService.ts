import { httpService } from '@dataplatform/shared/common-http';
import { EnhancedTableQuery, EnhancedTableQueryResult, EnumItem, CommonUiUtils } from '@dataplatform/shared/common-ui';
import { environment } from '../../environments/environment';
import { RoleDataHttpModel, RoleDetailHttpModel, RoleDetailModel, RoleListItemModel, RoleUpsertHttpModel, RoleSaveDataModel } from '../models';

/**
 * Initializes the RolesService class
 * This class is used to make requests to the back-end server
 */
class RolesService {

  private mapEnumFn = (x: RoleDataHttpModel): EnumItem<number, { parentId: number }> => {
    if (x) {
      const item: EnumItem<number, { parentId: number }> = {
        id: x.id,
        name: x.label,
        parentId: x.parent_id,
      };

      return item;
    }
    return null;
  }

  private mapListItemFn = (x: RoleDataHttpModel): RoleListItemModel => {
    if (x) {
      const item: RoleListItemModel = {
        id: x.id,
        name: x.label,
        description: x.description,
        parentId: x.parent_id,
        parentName: x.parent_name,
        updated: x.updated_at ? new Date(x.updated_at) : null,
      };

      return item;
    }
    return null;
  }

  private mapDetailFn = (x: RoleDetailHttpModel): RoleDetailModel => {
    if (x) {
      const item: RoleDetailModel = {
        name: x.label,
        description: x.description,
        parentId: x.parent_id,
        isParent: x.isParent,
        parentName: x.parent_name,
        rateLimit: !!x.rateLimit,
        count: x.rateLimit ? x.rateLimit.limit : null,
        timeWindow: x.rateLimit ? x.rateLimit.timewindow : null,
      };

      return item;
    }
    return null;
  }

  getAsEnums = (): Promise<EnumItem<number, { parentId: number }>[]> => {
    let url = environment.apiUrl + '/roles?orderBy=label&orderDir=asc';

    return httpService.get<RoleDataHttpModel[], EnumItem<number, { parentId: number }>[]>(
      url,
      response => {
        let data: EnumItem<number, { parentId: number }>[] = [];
        if (response) {
          data = response.map(x => this.mapEnumFn(x));
        }
        return data;
      }
    );
  }

  private mapFieldFn = (x: string): string => {
    switch (x) {
      case 'name': return 'label';
      case 'parentName': return 'parent_name';
      case 'updated': return 'updated_at';
    }
    return x;
  }

  getAll = (query: EnhancedTableQuery<RoleListItemModel>): Promise<EnhancedTableQueryResult<RoleListItemModel>> => {
    let qs = CommonUiUtils.enhancedTableQueryToQueryString(query, this.mapFieldFn);
    const url = environment.apiUrl + '/roles' + qs;

    return httpService.getRaw<RoleDataHttpModel[], EnhancedTableQueryResult<RoleListItemModel>>(url, response => {
      let data: RoleListItemModel[] = [];
      if (response.data) {
        data = response.data.map(this.mapListItemFn);
      }
      return {
        total: parseInt(response.headers['x-count'] || response.data.length, 10),
        items: data
      };
    });
  }

  delete = (id: number): Promise<boolean> => {
    const url = environment.apiUrl + '/roles/' + id;
    const mapFn = () => { return true; }
    return httpService.delete<any, boolean>(url, mapFn);
  }

  getById = (id: number): Promise<RoleDetailModel> => {
    const url = environment.apiUrl + '/roles/' + id;

    return httpService.get<RoleDetailHttpModel, RoleDetailModel>(url, response => this.mapDetailFn(response));
  }

  upsert = (id: number, data: RoleSaveDataModel): Promise<boolean> => {
    const url = environment.apiUrl + '/roles' + (id != null ? '/' + id : '');

    const body: RoleUpsertHttpModel = {
      label: data.name,
      description: data.description,
      parent_id: data.parentId,
      rateLimit: data.rateLimit ? {
        limit: data.count,
        timewindow: data.timeWindow
      } : null
    };

    const mapFn = () => { return true; }

    if (id != null) {
      return httpService.patch<any, boolean>(url, body, mapFn);
    } else {
      return httpService.post<any, boolean>(url, body, mapFn);
    }
  }
}

export const rolesService = new RolesService();
