import { httpService } from '@dataplatform/shared/common-http';
import { environment } from '../../environments/environment';
import {
  LoginModel,
  LoginReponseModel,
  ChangePasswordModel,
  ForgottenPasswordModel,
  PasswordResetModel,
  PasswordResetErrorType
} from '../models';
import {
  LoginHttpModel,
  LoginResponseHttpModel,
  ChangePasswordHttpModel,
  ForgottenPasswordHttpModel,
  ForgottenPasswordResponseHttpModel,
  PasswordResetHttpModel,
  PasswordResetCodeCheckResponseHttpModel
} from '../models/http';
import { setToken, removeToken } from '@dataplatform/shared/common-token';

/**
 * Initializes the AuthService class to be used.
 * AuthService takes care of executing requests to the back-end server
 * using the HttpService class.
 */
class AuthService {
  /**
   * Checks if the route is not login
   * and if the status type is 401, signs the user out
   */
  constructor() {
    httpService.handle401(() => {
      if (location.href.indexOf('login') === -1) {
        this.signOut();
      }
    });
  }

  /**
   * Fetches the api version using the httpService class
   */
  getApiVersion(): Promise<string> {
    const url = environment.apiUrl + '/status';
    return httpService.get<{ version: string }, string>(
      url,
      (x: { version: string }) => {
        return 'v' + x.version;
      }
    );
  }

  /**
   * Sends request to /login route and returns a promise
   * sets the token
   *
   * @param model Email, password
   * @return Promise
   */
  signIn = (model: LoginModel): Promise<LoginReponseModel> => {
    const url = environment.apiUrl + '/login?isAdmin=true';
    const body: LoginHttpModel = {
      email: model.email,
      password: model.password
    };
    return httpService.post<LoginResponseHttpModel, LoginReponseModel>(
      url,
      body,
      (x: LoginResponseHttpModel) => {
        setToken(x.token);
        return {
          token: x.token
        };
      }
    );
  };

  /**
   * Calls the removeToken function and
   * redirects the user to the login page
   */
  signOut = () => {
    removeToken();
    window.location.href =
      window.location.origin + environment.prefix + '/login';
  };

  /**
   * Changes the password of a user
   *
   * @param model Old password and the new password
   * @return Promise
   */
  changePassword = (model: ChangePasswordModel): Promise<boolean> => {
    const url = environment.apiUrl + '/change-password';
    const body: ChangePasswordHttpModel = {
      old_password: model.oldPassword,
      new_password: model.newPassword
    };
    return httpService.post<any, boolean>(url, body, () => {
      return true;
    });
  };

  /**
   * Sends a request to the back-end server
   * when user requests a forgotten password
   *
   * @param model Email
   * @return Promise
   */
  forgottenPasswordRequest = (
    model: ForgottenPasswordModel
  ): Promise<boolean> => {
    const url = environment.apiUrl + '/forgotten-password';
    const body: ForgottenPasswordHttpModel = {
      email: model.email,
      route_link: environment.prefix + '/password-reset/'
    };
    return httpService.post<ForgottenPasswordResponseHttpModel, boolean>(
      url,
      body,
      () => {
        return true;
      }
    );
  };

  mapCodeCheckError = (error: string): PasswordResetErrorType => {
    if (!error) {
      return null;
    }
    if (error === 'err_not_found') {
      return 'not_found';
    }
    if (error === 'err_already_expired') {
      return 'expired';
    }
    return 'other';
  };

  forgottenPasswordCodeCheck = (
    code: string
  ): Promise<PasswordResetErrorType> => {
    const url = environment.apiUrl + '/forgotten-password/' + code;

    return httpService.get<
      PasswordResetCodeCheckResponseHttpModel,
      PasswordResetErrorType
    >(url, x => {
      return this.mapCodeCheckError(x.error);
    });
  };

  /**
   * Resets the forgotten password
   *
   * @param code
   */
  forgottenPasswordReset = (
    code: string,
    model: PasswordResetModel
  ): Promise<boolean> => {
    const url = environment.apiUrl + '/forgotten-password/' + code;
    const body: PasswordResetHttpModel = {
      password: model.newPassword
    };

    return httpService.put<any, boolean>(url, body, () => {
      return true;
    });
  };
}

export const authService = new AuthService();
