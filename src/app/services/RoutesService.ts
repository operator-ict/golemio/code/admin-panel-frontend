import { httpService } from '@dataplatform/shared/common-http';
import { CommonUiUtils, EnhancedTableQuery, EnhancedTableQueryResult } from '@dataplatform/shared/common-ui';
import { environment } from '../../environments/environment';
import { RouteDataHttpModel, RouteDetailHttpModel, RouteDetailModel, RouteListItemModel, RouteMethodEnum, RouteMethodHttpType, RouteSaveDataModel, RouteUpsertHttpModel } from '../models';

class RoutesService {
  public mapMethod = (x: RouteMethodHttpType): RouteMethodEnum => {
    switch (x) {
      case 'GET': return RouteMethodEnum.Get;
      case 'POST': return RouteMethodEnum.Post;
      case 'PATCH': return RouteMethodEnum.Patch;
      case 'PUT': return RouteMethodEnum.Put;
      case 'DELETE': return RouteMethodEnum.Delete;
    }
    return null;
  }

  public mapMethodToHttp = (x: RouteMethodEnum): RouteMethodHttpType => {
    switch (x) {
      case RouteMethodEnum.Get: return 'GET';
      case RouteMethodEnum.Post: return 'POST';
      case RouteMethodEnum.Patch: return 'PATCH';
      case RouteMethodEnum.Put: return 'PUT';
      case RouteMethodEnum.Delete: return 'DELETE';
    }
    return null;
  }

  private mapListItemFn = (x: RouteDataHttpModel): RouteListItemModel => {
    if (x) {
      const item: RouteListItemModel = {
        id: x.id,
        method: this.mapMethod(x.method),
        endpoint: x.endpoint,
        targetUrl: x.target_url,
        targetPath: x.target_path,
        isPublic: x.public || false,
        isRedirect: x.is_redirect || false,
      };

      return item;
    }
    return null;
  }

  private mapDetailFn = (x: RouteDetailHttpModel): RouteDetailModel => {
    if (x) {
      const item: RouteDetailModel = {
        method: this.mapMethod(x.method),
        endpoint: x.endpoint,
        targetUrl: x.target_url,
        targetPath: x.target_path,
        isPublic: x.public || false,
        isRedirect: x.is_redirect || false,
      };

      return item;
    }
    return null;
  }

  private mapFieldFn = (x: string): string => {
    switch (x) {
      case 'targetUrl': return 'target_url';
      case 'targetPath': return 'target_path';
      case 'isPublic': return 'public';
      case 'isRedirect': return 'is_redirect';
    }
    return x;
  }

  getAll = (query: EnhancedTableQuery<RouteListItemModel>): Promise<EnhancedTableQueryResult<RouteListItemModel>> => {
    let qs = CommonUiUtils.enhancedTableQueryToQueryString(query, this.mapFieldFn);
    const url = environment.apiUrl + '/routes' + qs;

    return httpService.getRaw<RouteDataHttpModel[], EnhancedTableQueryResult<RouteListItemModel>>(url, response => {
      let data: RouteListItemModel[] = [];
      if (response.data) {
        data = response.data.map(this.mapListItemFn);
      }
      return {
        total: parseInt(response.headers['x-count'] || response.data.length, 10),
        items: data
      };
    });
  }

  getTargets = (): Promise<string[]> => {
    const url = environment.apiUrl + '/routes/targets';

    return httpService.get<string[], string[]>(url);
  }

  delete = (id: number): Promise<boolean> => {
    const url = environment.apiUrl + '/routes/' + id;
    const mapFn = () => { return true; }
    return httpService.delete<any, boolean>(url, mapFn);
  }

  getById = (id: number): Promise<RouteDetailModel> => {
    const url = environment.apiUrl + '/routes/' + id;

    return httpService.get<RouteDetailHttpModel, RouteDetailModel>(url, response => this.mapDetailFn(response));
  }

  upsert = (id: number, data: RouteSaveDataModel): Promise<boolean> => {
    let url = environment.apiUrl + '/routes' + (id != null ? '/' + id : '');

    const body: RouteUpsertHttpModel = {
      endpoint: data.endpoint,
      method: this.mapMethodToHttp(data.method),
      public: data.isPublic,
      target_path: data.targetPath,
      target_url: data.targetUrl,
      is_redirect: data.isRedirect,
    };

    const mapFn = () => { return true; }

    if (id != null) {
      return httpService.patch<any, boolean>(url, body, mapFn);
    } else {
      return httpService.post<any, boolean>(url, body, mapFn);
    }
  }
}

export const routesService = new RoutesService();
