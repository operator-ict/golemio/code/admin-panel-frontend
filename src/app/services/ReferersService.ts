import { httpService } from '@dataplatform/shared/common-http';
import { EnumItem } from '@dataplatform/shared/common-ui';
import { ListHttpModel, RefererDataHttpModel } from '../models';
import { environment } from '../../environments/environment';

/**
 * Initializes the ReferersService class
 */
class ReferersService {
  /**
   * Helper function that creates enum item and returns
   * the item if it was passed as x, otherwise returns null
   *
   * @param x
   * @returns item|null
   */
  private mapEnumFn = (x: RefererDataHttpModel): EnumItem => {
    if (x) {
      const item: EnumItem = {
        id: x.id,
        name: x.referer,
      };

      return item;
    }
    return null;
  }

  /**
   * Fetches the white-listed referrers
   *
   * @return Promise
   */
  getAsEnums = (): Promise<EnumItem[]> => {
    let url = environment.apiUrl + '/referer-whitelist';

    return httpService.get<RefererDataHttpModel[], EnumItem[]>(
      url,
      response => {
        let data: EnumItem[] = [];
        if (response) {
          data = response.map(x => this.mapEnumFn(x));
        }
        return data;
      }
    );
  }
}

export const referersService = new ReferersService();
