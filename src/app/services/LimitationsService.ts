import { httpService } from '@dataplatform/shared/common-http';
import { environment } from '../../environments/environment';
import { LimitationHttpMinMaxType, LimitationHttpType } from '../models/http/limitations';
import { LimitationTypeEnum, MinMaxTypeEnum } from '../models';

/**
 * Initializes the LimitationsService class
 */
class LimitationsService {
  /**
   * Maps the limitation type from http type using switch case
   *
   * @param x
   */
  public mapLimitationType = (x: LimitationHttpType): LimitationTypeEnum => {
    switch (x) {
      case 'in-array':
        return LimitationTypeEnum.InArray;
      case 'min-max':
        return LimitationTypeEnum.MinMax;
      case 'is-empty':
        return LimitationTypeEnum.IsEmpty;
    }
    return null;
  };

  public mapLimitationMinMaxType = (x: LimitationHttpMinMaxType): MinMaxTypeEnum => {
    switch (x) {
      case 'number':
        return MinMaxTypeEnum.Number;
      case 'date':
        return MinMaxTypeEnum.Date;
      case 'date-time':
        return MinMaxTypeEnum.DateTime;
      default:
        return null;
    }
  };

  /**
   * Maps the limitation type to http using switch case
   *
   * @param x
   */
  public mapLimitationTypeToHttp = (
    x: LimitationTypeEnum
  ): LimitationHttpType => {
    switch (x) {
      case LimitationTypeEnum.InArray:
        return 'in-array';
      case LimitationTypeEnum.MinMax:
        return 'min-max';
      case LimitationTypeEnum.IsEmpty:
        return 'is-empty';
    }
    return null;
  };

  public mapLimitationMinMaxTypeToHttp = (
    type: LimitationTypeEnum,
    x: MinMaxTypeEnum
  ): LimitationHttpMinMaxType => {
    switch (x) {
      case MinMaxTypeEnum.Number:
        return 'number';
      case MinMaxTypeEnum.Date:
        return 'date';
      case MinMaxTypeEnum.DateTime:
        return 'date-time';
      default:
        return type === LimitationTypeEnum.MinMax ? 'number' : null;
    }
  };

  /**
   * Gets all the limitations and maps them into an array
   *
   * @return Promise
   */
  getAll = (): Promise<LimitationTypeEnum[]> => {
    let url = environment.apiUrl + '/limitations';

    return httpService.get<LimitationHttpType[], LimitationTypeEnum[]>(
      url,
      response => {
        return response.map(x => this.mapLimitationType(x));
      }
    );
  };
}

export const limitationsService = new LimitationsService();
