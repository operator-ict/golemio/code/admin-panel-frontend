import { httpService } from "@dataplatform/shared/common-http";
import {
    ScopeListItemModel,
    ScopeDataHttpModel,
    LimitationModel,
    LimitationTypeEnum,
    ScopeDetailModel,
    ScopeDetailHttpModel,
    ScopeSaveDataModel,
    ScopeUpsertHttpModel,
} from "../models";
import { environment } from "../../environments/environment";
import { EnhancedTableQuery, EnhancedTableQueryResult, CommonUiUtils } from "@dataplatform/shared/common-ui";
import { LimitationDetailHttpModel, LimitationHttpType } from "../models/http/limitations";
import { limitationsService } from "./LimitationsService";
import { routesService } from "./RoutesService";

class ScopesService {
    private mapLimitation = (x: LimitationDetailHttpModel): LimitationModel => {
        return {
            block: x.block,
            max: x.type === "min-max" ? x.max : null,
            min: x.type === "min-max" ? x.min : null,
            default: x.type === "min-max" ? x.default : null,
            parameter: x.name,
            type: limitationsService.mapLimitationType(x.type),
            minMaxType: limitationsService.mapLimitationMinMaxType(x.minMaxType),
            valuesList: x.type === "in-array" ? x.values : [],
        };
    };

    private mapLimitationToHttp = (x: LimitationModel): LimitationDetailHttpModel => {
        return {
            block: x.block,
            max: x.type === LimitationTypeEnum.MinMax ? x.max : null,
            min: x.type === LimitationTypeEnum.MinMax ? x.min : null,
            default: x.type === LimitationTypeEnum.MinMax ? x.default : null,
            name: x.parameter,
            type: limitationsService.mapLimitationTypeToHttp(x.type),
            minMaxType: limitationsService.mapLimitationMinMaxTypeToHttp(x.type, x.minMaxType),
            values: x.type === LimitationTypeEnum.InArray ? x.valuesList : [],
        };
    };

    private mapListItemFn = (x: ScopeDataHttpModel): ScopeListItemModel => {
        return {
            id: x.id,
            roleId: x.role_id,
            routeId: x.route_id,
            method: x.route.method,
            routeName: x.route ? x.route.endpoint : null,
            limitations: x.limitations.map(this.mapLimitation),
            updated: x.updated_at ? new Date(x.updated_at) : null,
            isNew: false,
            wasChanged: false,
        };
    };

    private mapDetailFn = (x: ScopeDetailHttpModel): ScopeDetailModel => {
        if (x) {
            const item: ScopeDetailModel = {
                routeId: x.route_id,
                endpoint: x.route ? x.route.endpoint : null,
                method: x.route ? routesService.mapMethod(x.route.method) : null,
                limitations: x.limitations.map(this.mapLimitation),
            };

            return item;
        }
        return null;
    };

    private mapFieldFn = (x: string): string => {
        switch (x) {
            case "routeName":
                return "route.endpoint";
            case "updated":
                return "updated_at";
        }
        return x;
    };

    getAll = (
        roleId: number,
        query: EnhancedTableQuery<ScopeListItemModel>
    ): Promise<EnhancedTableQueryResult<ScopeListItemModel>> => {
        let qs = CommonUiUtils.enhancedTableQueryToQueryString(query, this.mapFieldFn);
        let url = environment.apiUrl + "/roles/" + roleId + "/scopes" + qs;

        return httpService.getRaw<ScopeDataHttpModel[], EnhancedTableQueryResult<ScopeListItemModel>>(url, response => {
            let data: ScopeListItemModel[] = response.data ? response.data.map(this.mapListItemFn) : [];
            const result: EnhancedTableQueryResult<ScopeListItemModel> = {
                total: parseInt(response.headers["x-count"] || response.data.length, 10),
                items: data,
            };
            return result;
        });
    };

    getById = (roleId: number, scopeId: number): Promise<ScopeDetailModel> => {
        let url = environment.apiUrl + "/roles/" + roleId + "/scopes/" + scopeId;

        return httpService.get<ScopeDetailHttpModel, ScopeDetailModel>(url, response => this.mapDetailFn(response));
    };

    upsert = (roleId: number, scopeId: number, data: ScopeSaveDataModel): Promise<boolean> => {
        let url = environment.apiUrl + "/roles/" + roleId + "/scopes" + (scopeId != null ? "/" + scopeId : "");

        const body: ScopeUpsertHttpModel = {
            route_id: data.routeId,
            limitations: data.limitations ? data.limitations.map(x => this.mapLimitationToHttp(x)) : [],
        };

        const mapFn = () => {
            return true;
        };

        if (scopeId != null) {
            return httpService.patch<any, boolean>(url, body, mapFn);
        } else {
            return httpService.post<any, boolean>(url, body, mapFn);
        }
    };

    delete = (roleId: number, scopeId: number): Promise<boolean> => {
        let url = environment.apiUrl + "/roles/" + roleId + "/scopes/" + scopeId;

        const mapFn = () => {
            return true;
        };

        return httpService.delete<any, boolean>(url, mapFn);
    };
}

export const scopesService = new ScopesService();
