import { httpService } from '@dataplatform/shared/common-http';
import {
  EnhancedTableQuery,
  EnhancedTableQueryResult,
  CommonUiUtils
} from '@dataplatform/shared/common-ui';
import {
  UserDataHttpModel,
  UserDetailHttpModel,
  UserDetailModel,
  UserListItemModel,
  UserSaveDataModel,
  UserUpdateHttpModel,
  UserCreateHttpModel
} from '../models';
import { environment } from '../../environments/environment';

class UsersService {
  private mapListItemFn = (x: UserDataHttpModel): UserListItemModel => {
    if (x) {
      const item: UserListItemModel = {
        id: x.id,
        email: x.email,
        firstName: x.name,
        lastName: x.surname,
        roles: x.roleIds,
        created: x.created_at ? new Date(x.created_at) : null,
        activated: x.invitation_accepted_at
          ? new Date(x.invitation_accepted_at)
          : null,
        last_activity: x.last_activity ? new Date(x.last_activity) : null
      };

      return item;
    }
    return null;
  };

  private mapDetailFn = (x: UserDetailHttpModel): UserDetailModel => {
    if (x) {
      const item: UserDetailModel = {
        firstName: x.name,
        lastName: x.surname,
        email: x.email,
        referer: x.initial_referer,
        roles: x.roleIds,
        rateLimit: !!x.rateLimit,
        count: x.rateLimit ? x.rateLimit.limit : null,
        timeWindow: x.rateLimit ? x.rateLimit.timewindow : null
      };

      return item;
    }
    return null;
  };

  private mapFieldFn = (x: string): string => {
    switch (x) {
      case 'firstName':
        return 'name';
      case 'lastName':
        return 'surname';
      case 'created':
        return 'created_at';
      case 'activated':
        return 'invitation_accepted_at';
      case 'last_activity':
        return 'last_activity';
    }
    return x;
  };

  getAll = (
    query: EnhancedTableQuery<UserListItemModel>
  ): Promise<EnhancedTableQueryResult<UserListItemModel>> => {
    let qs = CommonUiUtils.enhancedTableQueryToQueryString(
      query,
      this.mapFieldFn
    );
    let url = environment.apiUrl + '/users' + qs;

    return httpService.getRaw<
      UserDataHttpModel[],
      EnhancedTableQueryResult<UserListItemModel>
    >(url, response => {
      let data: UserListItemModel[] = [];
      if (response.data) {
        data = response.data.map(this.mapListItemFn);
      }
      return {
        total: parseInt(
          response.headers['x-count'] || response.data.length,
          10
        ),
        items: data
      };
    });
  };

  delete = (id: number): Promise<boolean> => {
    const url = environment.apiUrl + '/users/' + id;
    const mapFn = () => {
      return true;
    };
    return httpService.delete<any, boolean>(url, mapFn);
  };

  getById = (id: number): Promise<UserDetailModel> => {
    let url = environment.apiUrl + '/users/' + id + '?isAdmin=true';

    return httpService.get<UserDetailHttpModel, UserDetailModel>(
      url,
      response => this.mapDetailFn(response)
    );
  };

  upsert = (id: number, data: UserSaveDataModel): Promise<boolean> => {
    let url =
      environment.apiUrl +
      '/users' +
      (id != null ? '/' + id : '') +
      '?isAdmin=true';
    const updateBody: UserUpdateHttpModel = {
      name: data.firstName,
      surname: data.lastName,
      roleIds: data.roles,
      rateLimit: data.rateLimit
        ? {
            limit: data.count,
            timewindow: data.timeWindow
          }
        : null
    };

    const mapFn = () => {
      return true;
    };

    if (id != null) {
      return httpService.patch<any, boolean>(url, updateBody, mapFn);
    } else {
      const createBody: UserCreateHttpModel = {
        email: data.email,
        referer: data.referer,
        ...updateBody
      };

      return httpService.post<any, boolean>(url, createBody, mapFn);
    }
  };
}

export const usersService = new UsersService();
