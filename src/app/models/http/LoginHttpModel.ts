export interface LoginHttpModel {
  email: string;
  password: string;
}
