import { RouteMethodHttpType } from './RouteMethodHttpType';

export interface RouteDataHttpModel {
  id: number;
  endpoint: string;
  method: RouteMethodHttpType;
  public: boolean;
  target_path: string;
  target_url: string;
  is_redirect: boolean;
  created_at: Date;
  updated_at: Date;
}
