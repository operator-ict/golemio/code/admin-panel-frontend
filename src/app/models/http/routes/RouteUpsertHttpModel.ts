import { RouteMethodHttpType } from './RouteMethodHttpType';

export interface RouteUpsertHttpModel {
    endpoint: string;
    method: RouteMethodHttpType;
    public: boolean;
    target_path: string;
    target_url: string;
    is_redirect: boolean;
}