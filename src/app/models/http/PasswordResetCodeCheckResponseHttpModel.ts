export interface PasswordResetCodeCheckResponseHttpModel {
    valid: boolean;
    error: string;
}