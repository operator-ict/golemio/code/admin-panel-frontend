export interface RoleUpsertHttpModel {
  label: string;
  description: string;
  parent_id: number;
  rateLimit: {
    limit: number;
    timewindow: number;
  };
}
