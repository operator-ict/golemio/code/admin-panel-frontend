import { ScopeDataHttpModel } from '../scopes';

export interface RoleDataHttpModel {
  id: number;
  label: string;
  description: string;
  parent_id: number;
  parent_name: string;
  isParent: boolean;
  default: boolean;
  created_at: string;
  updated_at: string;
  scopes: ScopeDataHttpModel[];
  rateLimit: {
    limit: number;
    timewindow: number;
  };
}
