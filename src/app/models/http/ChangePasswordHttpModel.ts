export interface ChangePasswordHttpModel {
    old_password: string;
    new_password: string;
}