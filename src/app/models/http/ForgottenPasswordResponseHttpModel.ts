export interface ForgottenPasswordResponseHttpModel {
    resetLink: string;
    token: string;
}