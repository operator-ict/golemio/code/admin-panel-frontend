export interface RefererDataHttpModel {
  id: number;
  referer: string;
}
