export type LimitationHttpType = 'in-array' | 'min-max' | 'is-empty';
export type LimitationHttpMinMaxType = 'number' | 'date' | 'date-time';
