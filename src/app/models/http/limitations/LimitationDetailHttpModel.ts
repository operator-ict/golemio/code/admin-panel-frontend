import { LimitationHttpMinMaxType, LimitationHttpType } from './LimitationHttpType';

export interface LimitationDetailHttpModel {
    name: string;
    type: LimitationHttpType;
    minMaxType?: LimitationHttpMinMaxType;
    block: boolean;
    values?: string[];
    min?: number;
    max?: number;
    default?: number;
}
