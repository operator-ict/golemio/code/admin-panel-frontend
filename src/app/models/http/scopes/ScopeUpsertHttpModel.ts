import { LimitationDetailHttpModel } from '../limitations';

export interface ScopeUpsertHttpModel {
    route_id: number;
    limitations: LimitationDetailHttpModel[];
}