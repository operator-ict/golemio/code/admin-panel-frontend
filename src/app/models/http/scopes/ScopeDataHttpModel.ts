import { LimitationDetailHttpModel } from '../limitations';
import { RouteMethodHttpType } from '../routes';

export interface ScopeDataHttpModel {
    id: number;
    role_id: number;
    route_id: number;
    limitations: LimitationDetailHttpModel[];
    created_at: string;
    updated_at: string;
    route: {
        endpoint: string;
        method: RouteMethodHttpType;
    };
}