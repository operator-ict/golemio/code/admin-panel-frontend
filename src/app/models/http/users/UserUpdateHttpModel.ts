export interface UserUpdateHttpModel {
    name: string;
    surname: string;
    roleIds: number[];
    rateLimit: {
        limit: number;
        timewindow: number;
    };
}