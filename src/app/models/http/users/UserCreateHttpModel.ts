import { UserUpdateHttpModel } from './UserUpdateHttpModel';

export interface UserCreateHttpModel extends UserUpdateHttpModel {
    email: string;
    referer: number;
}