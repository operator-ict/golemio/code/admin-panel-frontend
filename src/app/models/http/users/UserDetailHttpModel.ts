import { UserDataHttpModel } from './UserDataHttpModel';

export interface UserDetailHttpModel extends UserDataHttpModel {
  rateLimit: {
    limit: number;
    timewindow: number;
  };
  initial_referer: number;
}
