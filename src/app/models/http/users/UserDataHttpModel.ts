export interface UserDataHttpModel {
  id: number;
  name: string;
  surname: string;
  email: string;
  created_at: string;
  updated_at: string;
  invitation_accepted_at: Date;
  roleIds: number[];
  last_activity: Date;
}
