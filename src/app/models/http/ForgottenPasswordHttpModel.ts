export interface ForgottenPasswordHttpModel {
  email: string;
  route_link: string;
}
