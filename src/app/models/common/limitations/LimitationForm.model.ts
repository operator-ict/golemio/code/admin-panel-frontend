import { LimitationTypeEnum } from './LimitationTypeEnum';

export interface LimitationFormModel {
  type: LimitationTypeEnum | '';
  parameter: string;
  block?: boolean;
  min?: number | '';
  max?: number | '';
  default?: number | '';
  whitelist?: string[];
  valuesList?: string[];
}
