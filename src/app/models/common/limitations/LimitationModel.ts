import { LimitationTypeEnum, MinMaxTypeEnum } from './LimitationTypeEnum';

export interface LimitationModel {
    type: LimitationTypeEnum;
    parameter: string;
    block?: boolean;
    minMaxType?: MinMaxTypeEnum;
    min?: number;
    max?: number;
    default?: number;
    whitelist?: string[];
    valuesList?: string[];
}
