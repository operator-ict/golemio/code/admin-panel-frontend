export enum LimitationTypeEnum {
  InArray = 'in-array',
  MinMax = 'min-max',
  IsEmpty = 'is-empty'
}

export enum MinMaxTypeEnum {
  Number = 'number',
  Date = 'date',
  DateTime = 'date-time'
}
