import { RouteMethodEnum } from './RouteMethodEnum';

export interface RouteSaveDataModel {
  method: RouteMethodEnum;
  endpoint: string;
  targetUrl: string;
  targetPath: string;
  isPublic: boolean;
  isRedirect: boolean;
}
