import { RouteMethodEnum } from './RouteMethodEnum';

export interface RouteDetailModel {
  method: RouteMethodEnum;
  endpoint: string;
  targetUrl: string;
  targetPath: string;
  isPublic: boolean;
  isRedirect: boolean;
}
