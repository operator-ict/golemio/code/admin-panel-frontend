import { RouteMethodEnum } from './RouteMethodEnum';

export interface RouteListItemModel {
  id: number;
  method: RouteMethodEnum;
  endpoint: string;
  targetUrl: string;
  targetPath: string;
  isPublic: boolean;
  isRedirect: boolean;
}
