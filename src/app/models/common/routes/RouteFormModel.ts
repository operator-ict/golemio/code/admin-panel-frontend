import { RouteMethodEnum } from './RouteMethodEnum';

export interface RouteFormModel {
  method: RouteMethodEnum | "";
  endpoint: string;
  target: string;
  targetPath: string;
  isPublic: boolean;
  isRedirect: boolean;
}
