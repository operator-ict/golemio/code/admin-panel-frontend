export interface RoleListItemModel {
  id: number;
  name: string;
  description: string;
  parentId: number;
  parentName: string;
  updated: Date;
  settings?: string;
}
