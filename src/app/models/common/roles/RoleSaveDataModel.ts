export interface RoleSaveDataModel {
  name: string;
  description: string;
  parentId: number;
  rateLimit: boolean;
  count: number;
  timeWindow: number;
}
