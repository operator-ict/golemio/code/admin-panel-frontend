export interface RoleDetailModel {
  name: string;
  description: string;
  parentId: number;
  isParent: boolean;
  parentName: string;
  rateLimit: boolean;
  count: number;
  timeWindow: number;
}
