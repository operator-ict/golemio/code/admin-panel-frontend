import { LimitationModel } from '../limitations';
import { RouteMethodEnum } from '../routes';

export interface ScopeDetailModel {
  routeId: number;
  method: RouteMethodEnum;
  endpoint: string;
  limitations: LimitationModel[];
}
