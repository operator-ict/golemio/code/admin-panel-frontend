import { LimitationModel } from '../limitations';

export interface ScopeListItemModel {
    id: number;
    roleId: number;
    routeId: number;
    method: string;
    routeName: string;
    updated: Date;
    limitations: LimitationModel[];
    originalLimitations?: LimitationModel[];
    isNew: boolean;
    wasChanged: boolean;
}