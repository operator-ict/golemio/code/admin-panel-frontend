import { ComboBoxOptionModel } from '@dataplatform/shared/common-ui';

export interface ScopeFormModel {
  routeId: number | '';
  route: ComboBoxOptionModel;
}
