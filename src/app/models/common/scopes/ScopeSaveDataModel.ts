import { LimitationModel } from '../limitations';

export interface ScopeSaveDataModel {
  routeId: number;
  limitations: LimitationModel[];
}
