export interface AuthModel {
    email: string;
    token: string;
}