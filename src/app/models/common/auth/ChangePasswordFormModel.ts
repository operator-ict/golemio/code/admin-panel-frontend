export interface ChangePasswordFormModel {
    oldPassword: string;
    newPassword: string;
    newPasswordConfirmation: string;
}