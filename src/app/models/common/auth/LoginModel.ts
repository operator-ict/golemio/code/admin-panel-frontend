export interface LoginModel {
    email: string;
    password: string;
    confirmation_password?: string;
}
