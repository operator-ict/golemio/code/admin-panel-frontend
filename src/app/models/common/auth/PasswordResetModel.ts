export interface PasswordResetModel {
    newPassword: string;
}

export type PasswordResetErrorType = 'not_found' | 'expired' | 'other' | null;