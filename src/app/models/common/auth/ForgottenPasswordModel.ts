export interface ForgottenPasswordModel {
    email: string;
    confirmation_password?: string;
}
