export interface PasswordResetFormModel {
    newPassword: string;
    newPasswordConfirmation: string;
}