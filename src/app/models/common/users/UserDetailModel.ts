export interface UserDetailModel {
  firstName: string;
  lastName: string;
  email: string;
  referer: number; // id
  roles: number[]; // array of ids
  rateLimit: boolean;
  count: number;
  timeWindow: number;
}
