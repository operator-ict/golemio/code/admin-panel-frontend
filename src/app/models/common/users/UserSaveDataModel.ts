export interface UserSaveDataModel {
  email: string;
  referer: number;
  firstName: string;
  lastName: string;
  roles: number[]; // array of ids
  rateLimit: boolean;
  count: number;
  timeWindow: number;
}
