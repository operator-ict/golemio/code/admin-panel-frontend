export interface UserListItemModel {
  id: number;
  email: string;
  firstName: string;
  lastName: string;
  roles: number[];
  created: Date;
  activated: Date;
  last_activity: Date;
}
