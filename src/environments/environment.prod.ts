export const environment = {
  production: true,
  apiUrl: 'https://permissions.golemio.cz/gateway',
  prefix: '/admin',
  version: 'v0.1'
};
