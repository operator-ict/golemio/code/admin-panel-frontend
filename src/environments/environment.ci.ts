export const environment = {
  production: false,
  apiUrl: 'http://permission-proxy:3007',
  prefix: '/admin',
  version: 'v0.1'
};
