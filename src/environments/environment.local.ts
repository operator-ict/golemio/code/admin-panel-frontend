export const environment = {
  production: false,
  apiUrl: 'http://localhost:3010/gateway',
  prefix: '/admin',
  version: 'v0.1'
};
