export const environment = {
    production: false,
    apiUrl: "https://permissions.rabin.golemio.cz/gateway",
    prefix: "/admin",
    version: "v0.1",
};
